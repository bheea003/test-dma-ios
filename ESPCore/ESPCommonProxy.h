//
//  ESPCommonProxy.h
//  DMA
//
//  Created by JP on 5/23/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESPConvenience.h"

#import "ESPClient.h"
#import "ESPClientUpgrade.h"
#import "ESPCatalog.h"
#import "ESPMedia.h"
#import "ESPConsumer.h"
#import "ESPAuthorization.h"

@interface ESPCommonProxy : NSObject

// You should set these values, otherwise things might get funky!
@property (strong, nonatomic) NSString *applicationName; // defaults to "DMA"
@property (strong, nonatomic) NSString *applicationLanguage; // defaults to "EN"
@property (strong, nonatomic) NSString *applicationCountry; // defaults to "US"

// You should KVO on these objects...
@property (strong, nonatomic) ESPAuthorization *authorization; // set upon login
@property (strong, nonatomic) NSMutableOrderedSet *ownedMedia; // set upon login; the client's Entitlements
@property (assign, nonatomic) BOOL hasValidAccess; // set upon validateAccess
@property (strong, nonatomic) ESPClientUpgrade *clientUpgrade; // set upon validateUpgradeStatus
@property (assign, nonatomic) BOOL isLoading; // changes based on network activity

+ (id)sharedProxy;
- (void)restore;

// Client (aka Device, Application)
- (ESPClient *)client;
- (void)validateAccess;
- (void)validateUpgradeStatus;

// Catalog (aka Categories, Categories)
- (ESPCatalog *)catalog;
- (void)obtainCategoriesMinus:(NSArray *)categoryNames success:(void (^)(NSArray *categories))success failure:(void (^)(NSError *error))failure;
- (void)obtainTypes:(void (^)(NSArray *types))success failure:(void (^)(NSError *error))failure;

// Consumer (aka user/guest)
- (ESPConsumer *)consumer;
- (void)displayLoginForm:(NSString *)message; // will call login:password:
- (void)login:(NSString *)login password:(NSString *)password;
- (void)archiveLogin:(NSString *)login password:(NSString *)password;
- (void)obtainOwnedMovies;
- (void)logout;

// Entitlement/Owned media handling
- (BOOL)hasEntitlementToMedia:(ESPMedia *)media;
- (ESPMedia *)entitledMediaForRawMedia:(ESPMedia *)media; // convenience to access Entitled/Owned Movies

// App-wide setting of network activity indicator
- (void)shouldEnableNetworkActivityIndicator:(BOOL)flag;

// Developer metadata
- (NSDictionary *)developerInfo;
- (NSDictionary *)licenses;
- (NSArray *)licenseArray;

@end
