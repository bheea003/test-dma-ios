//
//  ESPClient.m
//  ESPClient
//
//  Created by JP on 4/4/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPClient.h"
#import "ESPClientUpgrade.h"
#import "ESPConvenience.h"

@implementation ESPClient

+ (ESPClient *)clientForApplication:(NSString *)applicationName language:(NSString *)iso639dash1 country:(NSString *)iso3166dash1alpha2
{
    static ESPClient *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURL *url = [NSURL URLWithString:ESPAPIBaseURL];
        __sharedInstance = [[ESPClient alloc] initWithBaseURL:url];
        [__sharedInstance setDefaultHeader:@"Client" value:applicationName];
        [__sharedInstance setDefaultHeader:@"Language" value:iso639dash1];
        [__sharedInstance setDefaultHeader:@"Country" value:iso3166dash1alpha2];
        
        [__sharedInstance registerHTTPOperationClass:[AFJSONRequestOperation class]];
    });
    
    
    if (! [__sharedInstance hasCorrectHeaders]) {
        NSLog(@"ESPClient ALERT: Please check your client settings:\n\tapplicationName is:%@\n\tlanguage (should be ISO 639-1) is:%@\n\tcountry (should be ISO 3166-1 alpha-2) is:%@", __sharedInstance.applicationName, __sharedInstance.language, __sharedInstance.country);
    }
    return __sharedInstance;
}

#pragma mark - Convenience
- (NSString *)domain
{
    return @"com.disney.esp.client";
}

#pragma mark - Validation Methods
- (void)validateAccess:(NSString *)ipAddress success:(void (^)(NSString *country))success failure:(void (^)(NSError *error))failure
{
    if (! [self hasCorrectHeaders]) {
        failure([self errorForType:InvalidHeadersError userInfo:nil]);
        return;
    }

    NSMutableDictionary *dictionary = nil;
    if (! IsEmpty(ipAddress)) {
        dictionary = [NSMutableDictionary dictionaryWithObject:ipAddress forKey:@"ip"];
    }
    
    NSString *restPath = [NSString stringWithFormat:@"/%@/allowed", [self restDomain]];
    [self getPath:restPath parameters:dictionary success:^(AFHTTPRequestOperation *operation, id response) {
        if (IsEmpty(response)) {
            failure([self errorForType:APIServiceError userInfo:nil]);
            return;
        }
        
        NSError *error = nil;
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        if (! error) {
            BOOL isAllowed = [[dictionary valueForKey:@"supported"] boolValue];
            NSString *country = [dictionary valueForKey:@"countryCode"];
            if (isAllowed) {
                success(country);
            } else {
                failure([self errorForType:AccessDenied userInfo:nil]);
            }
        } else {
            failure([self errorForType:AccesssValidationError userInfo:error.userInfo]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([self errorForType:AccesssValidationError userInfo:error.userInfo]);
    }];
}

- (void)validateApplication:(NSString *)device systemVersion:(NSString *)systemVersion appversion:(NSString *)appVersion success:(void (^)(ESPClientUpgrade *upgrade))success failure:(void (^)(NSError *error))failure
{
    if (! [self hasCorrectHeaders]) {
        failure([self errorForType:InvalidHeadersError userInfo:nil]);
    }
    
    NSString *restPath = [NSString stringWithFormat:@"/%@/upgrade/%@/%@/%@", [self restDomain], device, systemVersion, appVersion];
    [self getPath:restPath parameters:nil success:^(AFHTTPRequestOperation *operation, id response) {
        if (IsEmpty(response)) {
            failure([self errorForType:APIServiceError userInfo:nil]);
            return;
        }
        
        NSError *error = nil;
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        if (! error) {
            ESPClientUpgrade *upgrade = [ESPClientUpgrade instanceFromDictionary:dictionary];
            success(upgrade);
        } else {
            failure([self errorForType:AccesssValidationError userInfo:error.userInfo]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([self errorForType:AccesssValidationError userInfo:error.userInfo]);
    }];
}

@end