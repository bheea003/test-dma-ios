//
//  ESPClientUpgrade.h
//  
//
//  Created by Josh Paul on 5/17/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    UpgradeError = -1,
    UpgradeNotApplicable,
    UpgradeAvailable,
    UpgradeRequired,
    UpgradeUnsupported,
} ESPClientUpgradeStatus;

@interface ESPClientUpgrade : NSObject <NSCoding> {
    NSString *appVersion;
    NSString *device;
    NSNumber *upgradeID;
    NSString *message;
    NSString *osVersion;
    NSString *upgradeOption;
    NSString *upgradeURL;
}

@property (nonatomic, copy) NSString *appVersion;
@property (nonatomic, copy) NSString *device;
@property (nonatomic, copy) NSNumber *upgradeID;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSString *osVersion;
@property (nonatomic, copy) NSString *upgradeOption;
@property (nonatomic, copy) NSString *upgradeURL;

+ (ESPClientUpgrade *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (ESPClientUpgradeStatus)upgradeStatus;

- (NSDictionary *)dictionaryRepresentation;

@end
