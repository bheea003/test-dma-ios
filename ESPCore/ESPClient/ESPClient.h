//
//  ESPClient.h
//  ESPClient
//
//  Created by JP on 4/4/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESPAPIClient.h"

@class ESPClientUpgrade;
@interface ESPClient : ESPAPIClient

+ (ESPClient *)clientForApplication:(NSString *)applicationName language:(NSString *)language country:(NSString *)country;

- (void)validateAccess:(NSString *)ipAddress success:(void (^)(NSString *country))success failure:(void (^)(NSError *error))failure;
- (void)validateApplication:(NSString *)device systemVersion:(NSString *)systemVersion appversion:(NSString *)appVersion success:(void (^)(ESPClientUpgrade *upgrade))success failure:(void (^)(NSError *error))failure;

@end
