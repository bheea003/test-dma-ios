//
//  ESPClientUpgrade.m
//  
//
//  Created by Josh Paul on 5/17/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import "ESPClientUpgrade.h"

@implementation ESPClientUpgrade

@synthesize appVersion;
@synthesize device;
@synthesize upgradeID;
@synthesize message;
@synthesize osVersion;
@synthesize upgradeOption;
@synthesize upgradeURL;

+ (ESPClientUpgrade *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    ESPClientUpgrade *instance = [[ESPClientUpgrade alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (ESPClientUpgradeStatus)upgradeStatus
{
    // none|optional|required|end-of-life
    if ([self.upgradeOption caseInsensitiveCompare:@"none"] == NSOrderedSame) {
        return UpgradeNotApplicable;
    } else if ([self.upgradeOption caseInsensitiveCompare:@"optional"] == NSOrderedSame) {
        return UpgradeAvailable;
    } else if ([self.upgradeOption caseInsensitiveCompare:@"required"] == NSOrderedSame) {
        return UpgradeRequired;
    } else if ([self.upgradeOption caseInsensitiveCompare:@"end-of-life"] == NSOrderedSame) {
        return UpgradeUnsupported;
    }
    return UpgradeError;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    [self setValuesForKeysWithDictionary:aDictionary];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        [self setValue:value forKey:@"upgradeID"];
    } else if ([key isEqualToString:@"upgradeUrl"]) {
        [self setValue:value forKey:@"upgradeURL"];
    } else {
        NSLog(@"ESPClientUpgrade undefined key from API! %@ [%@]", key, value);
        // [super setValue:value forUndefinedKey:key];
    }
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    if (self.appVersion) {
        [dictionary setObject:self.appVersion forKey:@"appVersion"];
    }
    if (self.device) {
        [dictionary setObject:self.device forKey:@"device"];
    }
    if (self.upgradeID) {
        [dictionary setObject:self.upgradeID forKey:@"upgradeID"];
    }
    if (self.message) {
        [dictionary setObject:self.message forKey:@"message"];
    }
    if (self.osVersion) {
        [dictionary setObject:self.osVersion forKey:@"osVersion"];
    }
    if (self.upgradeOption) {
        [dictionary setObject:self.upgradeOption forKey:@"upgradeOption"];
    }
    if (self.upgradeURL) {
        [dictionary setObject:self.upgradeURL forKey:@"upgradeURL"];
    }
    return dictionary;
}


//===========================================================
//  Keyed Archiving
//===========================================================
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.appVersion forKey:@"appVersion"];
    [encoder encodeObject:self.device forKey:@"device"];
    [encoder encodeObject:self.upgradeID forKey:@"upgradeID"];
    [encoder encodeObject:self.message forKey:@"message"];
    [encoder encodeObject:self.osVersion forKey:@"osVersion"];
    [encoder encodeObject:self.upgradeOption forKey:@"upgradeOption"];
    [encoder encodeObject:self.upgradeURL forKey:@"upgradeURL"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.appVersion = [decoder decodeObjectForKey:@"appVersion"];
        self.device = [decoder decodeObjectForKey:@"device"];
        self.upgradeID = [decoder decodeObjectForKey:@"upgradeID"];
        self.message = [decoder decodeObjectForKey:@"message"];
        self.osVersion = [decoder decodeObjectForKey:@"osVersion"];
        self.upgradeOption = [decoder decodeObjectForKey:@"upgradeOption"];
        self.upgradeURL = [decoder decodeObjectForKey:@"upgradeURL"];
    }
    return self;
}

@end
