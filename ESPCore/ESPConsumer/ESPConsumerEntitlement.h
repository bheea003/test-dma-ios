//
//  ESPConsumerEntitlement.h
//  
//
//  Created by Josh Paul on 5/14/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ESPAuthorization;
@interface ESPConsumerEntitlement : NSObject {
    NSString *entitlementLevel;
    NSString *mpmp;
    NSString *umid;
    ESPAuthorization *authorization;
}

@property (nonatomic, copy) NSString *entitlementLevel;
@property (nonatomic, copy) NSString *mpmp;
@property (nonatomic, copy) NSString *umid;
@property (nonatomic, strong) ESPAuthorization *authorization;

+ (ESPConsumerEntitlement *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
