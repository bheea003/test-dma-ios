//
//  ESPConsumerEntitlement.m
//  
//
//  Created by Josh Paul on 5/14/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import "ESPConsumerEntitlement.h"
#import "ESPAuthorization.h"

@implementation ESPConsumerEntitlement

@synthesize entitlementLevel;
@synthesize mpmp;
@synthesize umid;
@synthesize authorization;

+ (ESPConsumerEntitlement *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    ESPConsumerEntitlement *instance = [[ESPConsumerEntitlement alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    [self setValuesForKeysWithDictionary:aDictionary];
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    if (self.entitlementLevel) {
        [dictionary setObject:self.entitlementLevel forKey:@"entitlementLevel"];
    }
    if (self.mpmp) {
        [dictionary setObject:self.mpmp forKey:@"mpmp"];
    }
    if (self.umid) {
        [dictionary setObject:self.umid forKey:@"umid"];
    }
    return dictionary;
}

//===========================================================
//  Keyed Archiving
//===========================================================
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.entitlementLevel forKey:@"entitlementLevel"];
    [encoder encodeObject:self.mpmp forKey:@"mpmp"];
    [encoder encodeObject:self.umid forKey:@"umid"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.entitlementLevel = [decoder decodeObjectForKey:@"entitlementLevel"];
        self.mpmp = [decoder decodeObjectForKey:@"mpmp"];
        self.umid = [decoder decodeObjectForKey:@"umid"];
    }
    return self;
}

@end
