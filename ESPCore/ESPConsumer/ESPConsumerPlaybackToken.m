//
//  ESPConsumerPlaybackToken.m
//  
//
//  Created by Josh Paul on 5/17/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import "ESPConsumerPlaybackToken.h"

@implementation ESPConsumerPlaybackToken

@synthesize duration;
@synthesize idleTimeout;
@synthesize token;
@synthesize userId;
@synthesize userName;

+ (ESPConsumerPlaybackToken *)instanceFromDictionary:(NSDictionary *)aDictionary
{

    ESPConsumerPlaybackToken *instance = [[ESPConsumerPlaybackToken alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    [self setValuesForKeysWithDictionary:aDictionary];
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    if (self.duration) {
        [dictionary setObject:self.duration forKey:@"duration"];
    }
    if (self.idleTimeout) {
        [dictionary setObject:self.idleTimeout forKey:@"idleTimeout"];
    }
    if (self.token) {
        [dictionary setObject:self.token forKey:@"token"];
    }
    if (self.userId) {
        [dictionary setObject:self.userId forKey:@"userId"];
    }
    if (self.userName) {
        [dictionary setObject:self.userName forKey:@"userName"];
    }
    return dictionary;
}

//===========================================================
//  Keyed Archiving
//===========================================================
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.duration forKey:@"duration"];
    [encoder encodeObject:self.idleTimeout forKey:@"idleTimeout"];
    [encoder encodeObject:self.token forKey:@"token"];
    [encoder encodeObject:self.userId forKey:@"userId"];
    [encoder encodeObject:self.userName forKey:@"userName"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.duration = [decoder decodeObjectForKey:@"duration"];
        self.idleTimeout = [decoder decodeObjectForKey:@"idleTimeout"];
        self.token = [decoder decodeObjectForKey:@"token"];
        self.userId = [decoder decodeObjectForKey:@"userId"];
        self.userName = [decoder decodeObjectForKey:@"userName"];
    }
    return self;
}

@end
