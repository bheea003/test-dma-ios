//
//  ESPConsumer.h
//  ESPCore
//
//  Created by JP on 5/14/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPAPIClient.h"

@class ESPConsumer;
@class ESPAuthorization;
@class ESPAuthorizationValidation;
@class ESPConsumerPlaybackToken;
@interface ESPConsumer : ESPAPIClient

+ (ESPConsumer *)consumerForApplication:(NSString *)applicationName language:(NSString *)iso639dash1 country:(NSString *)iso3166dash1alpha2;

// Login
- (void)login:(NSString *)username password:(NSString *)password success:(void (^)(ESPAuthorization *authorization))success failure:(void (^)(NSError *error))failure;

// Session
- (void)beginPlaybackSession:(ESPAuthorization *)authorization success:(void (^)(ESPConsumerPlaybackToken *token))success failure:(void (^)(NSError *error))failure;
- (void)logout:(ESPAuthorization *)authorization success:(void (^)(NSString *result))success failure:(void (^)(NSError *error))failure;

// Owned
- (void)obtainOwnedEntitlements:(ESPAuthorization *)authorization success:(void (^)(NSArray *entitlements))success failure:(void (^)(NSError *error))failure;
- (void)obtainOwnedEntitlements:(ESPAuthorization *)authorization ratingsFilter:(NSArray *)ratingsFilter success:(void (^)(NSArray *entitlements))success failure:(void (^)(NSError *error))failure;
- (void)obtainOwnedMovies:(ESPAuthorization *)authorization ratingsFilter:(NSArray *)ratingsFilter success:(void (^)(NSArray *entitlements))success failure:(void (^)(NSError *error))failure;

// Entitlement
- (void)refreshEntitlements:(ESPAuthorization *)authorization success:(void (^)(id noData))success failure:(void (^)(NSError *))failure;

// Authentication
- (void)refresh:(ESPAuthorization *)authorization success:(void (^)(ESPAuthorization *refreshedAuthorization))success failure:(void (^)(NSError *error))failure;
- (void)validate:(ESPAuthorization *)authorization success:(void (^)(ESPAuthorizationValidation *validation))success failure:(void (^)(NSError *error))failure;

// DMR
- (void)obtainDMRProfile:(ESPAuthorization *)authorization  success:(void (^)(NSDictionary *accountDictionary))success failure:(void (^)(NSError *error))failure;

// Account (DMA)
- (void)obtainMetadata:(ESPAuthorization *)authorization success:(void (^)(NSDictionary *accountDictionary))success failure:(void (^)(NSError *error))failure;

@end
