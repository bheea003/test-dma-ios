//
//  ESPConsumer.m
//  ESPCore
//
//  Created by JP on 5/14/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPConsumer.h"
#import "ESPAuthZ.h"
#import "ESPAuthorization.h"
#import "ESPAuthorizationValidation.h"
#import "ESPConsumerEntitlement.h"
#import "ESPConsumerPlaybackToken.h"

#import "ESPMedia.h"

#import "ESPConvenience.h"
#import "NSString+ESP.h"

@interface ESPConsumer()
- (void)parseEntitlementsResult:(id)result authorization:(ESPAuthorization *)authorization success:(void (^)(NSArray *entitlements))success;
- (void)parseMoviesResult:(id)result success:(void (^)(NSArray *movies))success;
@end

@implementation ESPConsumer

+ (ESPConsumer *)consumerForApplication:(NSString *)applicationName language:(NSString *)iso639dash1 country:(NSString *)iso3166dash1alpha2
{
    static ESPConsumer *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURL *url = [NSURL URLWithString:ESPAPIBaseURL];
        __sharedInstance = [[ESPConsumer alloc] initWithBaseURL:url];
        [__sharedInstance setDefaultHeader:@"Client" value:applicationName];
        [__sharedInstance setDefaultHeader:@"Language" value:iso639dash1];
        [__sharedInstance setDefaultHeader:@"Country" value:iso3166dash1alpha2];
        
        [__sharedInstance registerHTTPOperationClass:[AFJSONRequestOperation class]];
    });
    
    if (! [__sharedInstance hasCorrectHeaders]) {
        ESPLog("ESPConsumer ALERT: Please check your client settings:\n\tapplicationName is:%@\n\tlanguage (should be ISO 639-1) is:%@\n\tcountry (should be ISO 3166-1 alpha-2) is:%@", [__sharedInstance defaultValueForHeader:@"Client"], [__sharedInstance defaultValueForHeader:@"Language"], [__sharedInstance defaultValueForHeader:@"Country"]);
    }
    return __sharedInstance;
}

#pragma mark - Convenience
- (NSString *)domain
{
    return @"com.disney.esp.consumer";
}

#pragma mark Login/Logout
- (void)login:(NSString *)username password:(NSString *)password success:(void (^)(ESPAuthorization *playbackAuthorization))success failure:(void (^)(NSError *error))failure
{
    ESPAuthZ *authZ = [ESPAuthZ authZForApplication:@"DSAAiOSPlayer" language:[self language] country:[self country]];
    [authZ authorizeUser:username password:password success:^(ESPAuthorization *result) {
        success(result);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)refresh:(ESPAuthorization *)authorization success:(void (^)(ESPAuthorization *result))success failure:(void (^)(NSError *error))failure
{
    ESPAuthZ *authZ = [ESPAuthZ authZForApplication:@"DSAAiOSPlayer" language:[self language] country:[self country]];
    [authZ refresh:authorization success:^(ESPAuthorization *result) {
        success(result);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)validate:(ESPAuthorization *)authorization success:(void (^)(ESPAuthorizationValidation *validation))success failure:(void (^)(NSError *))failure
{
    
    ESPAuthZ *authZ = [ESPAuthZ authZForApplication:@"DSAAiOSPlayer" language:[self language] country:[self country]];
    [authZ validate:authorization success:^(ESPAuthorizationValidation *result) {
        success(result);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)logout:(ESPAuthorization *)authorization success:(void (^)(NSString *result))success failure:(void (^)(NSError *error))failure
{
    if (! [self hasCorrectHeaders]) {
        failure([self errorForType:InvalidHeadersError userInfo:nil]);
        return;
    }
    
    [self setParameterEncoding:AFFormURLParameterEncoding];
    
    NSString *token = [[authorization playbackToken] token];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:token forKey:@"token"];
    NSString *path = [NSString stringWithFormat:@"/%@/session/%@", [self restDomain], token];
    [self deletePath:path parameters:dictionary success:^(AFHTTPRequestOperation *operation, id response) {
        if (IsEmpty(response)) {
            failure([self errorForType:APIServiceError userInfo:nil]);
            return;
        }
        
        NSError *error = nil;
        id json = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        if (! error) {
            NSString *result = nil;
            if ([json isKindOfClass:[NSDictionary class]]) {
                result = [json valueForKey:@"signOutResponse"];
            } else {
                result = [json stringValue];
            }
            success(result);
        } else {
            failure(error);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([self errorForType:APIServiceError userInfo:error.userInfo]);
    }];
}

#pragma mark Entitlements/Movies
- (void)obtainOwnedEntitlements:(ESPAuthorization *)authorization ratingsFilter:(NSArray *)ratingsFilter shouldExpand:(BOOL)shouldExpandMetadata  success:(void (^)(NSArray *entitlements))success failure:(void (^)(NSError *error))failure
{
    if (! [self hasCorrectHeaders]) {
        failure([self errorForType:InvalidHeadersError userInfo:nil]);
        return;
    }
    
    NSString *authorizationString = [NSString stringWithFormat:@"%@ %@", [authorization tokenType], [authorization accessToken]];
    [self setDefaultHeader:@"Authorization" value:authorizationString];
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    if (! IsEmpty(ratingsFilter)) {
        [dictionary setObject:[ratingsFilter componentsJoinedByString:@","] forKey:@"rating"];
    }
    if (shouldExpandMetadata) {
        [dictionary setObject:@"true" forKey:@"expanded"];
    }
    [dictionary setObject:@"DSAAiOSPlayer" forKey:@"app_id"];

    NSString *restPath = [NSString stringWithFormat:@"/%@/entitlement/", [self restDomain]];
    [self getPath:restPath parameters:dictionary success:^(AFHTTPRequestOperation *operation, id response) {
        if (IsEmpty(response)) {
            failure([self errorForType:APIServiceError userInfo:nil]);
            return;
        }
        
        NSError *error = nil;
        id result = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        // ESPLog(@"entitlement response: %@", [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding]);
        if (! error) {
            if (shouldExpandMetadata) {
                [self parseMoviesResult:result success:success];
            } else {
                [self parseEntitlementsResult:result authorization:authorization success:success];
            }
        } else {
            failure([self errorForType:AccesssValidationError userInfo:error.userInfo]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([self errorForType:APIServiceError userInfo:error.userInfo]);
    }];
}

- (void)obtainOwnedEntitlements:(ESPAuthorization *)authorization success:(void (^)(NSArray *entitlements))success failure:(void (^)(NSError *error))failure
{
    [self obtainOwnedEntitlements:authorization ratingsFilter:nil shouldExpand:NO success:success failure:failure];
}

- (void)obtainOwnedEntitlements:(ESPAuthorization *)authorization ratingsFilter:(NSArray *)ratingsFilter success:(void (^)(NSArray *entitlements))success failure:(void (^)(NSError *error))failure
{
    [self obtainOwnedEntitlements:authorization ratingsFilter:ratingsFilter shouldExpand:NO success:success failure:failure];
}

- (void)refreshEntitlements:(ESPAuthorization *)authorization success:(void (^)(id noData))success failure:(void (^)(NSError *))failure
{
    NSString *restPath = [NSString stringWithFormat:@"/%@/entitlement/refresh", [self restDomain]];
    [self postPath:restPath parameters:nil success:^(AFHTTPRequestOperation *operation, id response) {
        if (IsEmpty(response)) {
            failure([self errorForType:APIServiceError userInfo:nil]);
            return;
        }
        
        NSError *error = nil;
        id result = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        if (! error) {
            success(result);
        } else {
            failure([self errorForType:AccesssValidationError userInfo:error.userInfo]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([self errorForType:APIServiceError userInfo:error.userInfo]);
    }];
}

#pragma mark Movies
- (void)obtainOwnedMovies:(ESPAuthorization *)authorization ratingsFilter:(NSArray *)ratingsFilter success:(void (^)(NSArray *entitlements))success failure:(void (^)(NSError *error))failure
{
    [self obtainOwnedEntitlements:authorization ratingsFilter:ratingsFilter shouldExpand:YES success:success failure:failure];
}

#pragma mark Session/Playback Tokens
- (void)beginPlaybackSession:(ESPAuthorization *)authorization success:(void (^)(ESPConsumerPlaybackToken *token))success failure:(void (^)(NSError *error))failure
{
    if (! [self hasCorrectHeaders]) {
        failure([self errorForType:InvalidHeadersError userInfo:nil]);
        return;
    }
    
    NSString *authorizationString = [NSString stringWithFormat:@"%@ %@", [authorization tokenType], [authorization accessToken]];
    [self setDefaultHeader:@"Authorization" value:authorizationString];
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    [dictionary setObject:[authorization refreshToken] forKey:@"refresh_token"];
    [dictionary setObject:@"DSAAiOSPlayer" forKey:@"app_id"];
    
    NSString *restPath = [NSString stringWithFormat:@"/%@/session/", [self restDomain]];
    [self postPath:restPath parameters:dictionary success:^(AFHTTPRequestOperation *operation, id response) {
        if (IsEmpty(response)) {
            failure([self errorForType:APIServiceError userInfo:nil]);
            return;
        }
        NSError *error = nil;
        id result = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        if (! error) {
            ESPConsumerPlaybackToken *token = [ESPConsumerPlaybackToken instanceFromDictionary:result];
            success(token);
        } else {
            failure([self errorForType:AccesssValidationError userInfo:error.userInfo]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([self errorForType:APIServiceError userInfo:error.userInfo]);
    }];
}

#pragma mark - Entitlement/Movie parsing
- (void)parseEntitlementsResult:(id)result authorization:(ESPAuthorization *)authorization success:(void (^)(NSArray *entitlements))success;
{
    NSMutableArray *entitlements = [NSMutableArray array];
    if ([result isKindOfClass:[NSArray class]]) {
        for (NSDictionary *entitlementDictionary in result) {
            ESPConsumerEntitlement *entitlement = [ESPConsumerEntitlement instanceFromDictionary:entitlementDictionary];
            entitlement.authorization = authorization;
            [entitlements addObject:entitlement];
        }
    } else if ([result isKindOfClass:[NSDictionary class]]) {
        ESPConsumerEntitlement *entitlement = [ESPConsumerEntitlement instanceFromDictionary:result];
        entitlement.authorization = authorization;
        [entitlements addObject:entitlement];
    }
    success(entitlements);
}

- (void)parseMoviesResult:(id)result success:(void (^)(NSArray *movies))success
{
    NSMutableArray *movies = [NSMutableArray array];
    if ([result isKindOfClass:[NSArray class]]) {
        for (NSDictionary *entitlementDictionary in result) {
            ESPMedia *movie = [ESPMedia instanceFromDictionary:entitlementDictionary];
            [movies addObject:movie];
        }
    } else if ([result isKindOfClass:[NSDictionary class]]) {
        ESPMedia *movie = [ESPMedia instanceFromDictionary:result];
        [movies addObject:movie];
    }
    success(movies);
}

#pragma mark - Metadata (DMA)
- (void)obtainMetadata:(ESPAuthorization *)authorization success:(void (^)(NSDictionary *accountDictionary))success failure:(void (^)(NSError *error))failure
{
    if (! [self hasCorrectHeaders]) {
        failure([self errorForType:InvalidHeadersError userInfo:nil]);
        return;
    }
    
    NSString *authorizationString = [NSString stringWithFormat:@"%@ %@", [authorization tokenType], [authorization accessToken]];
    [self setDefaultHeader:@"Authorization" value:authorizationString];
    
    NSString *restPath = [NSString stringWithFormat:@"/%@/account/dma", [self restDomain]];
    [self getPath:restPath parameters:nil success:^(AFHTTPRequestOperation *operation, id response) {
        if (IsEmpty(response)) {
            failure([self errorForType:APIServiceError userInfo:nil]);
            return;
        }
        
        NSError *error = nil;
        id result = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        // ESPLog(@"entitlement response: %@", [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding]);
        if (! error) {
            if ([result isKindOfClass:[NSDictionary class]]) {
                success(result);
            } else if ([result isKindOfClass:[NSArray class]]) {
                success([result objectAtIndex:0]);
            } else {
                failure([self errorForType:APIServiceError userInfo:error.userInfo]);
            }
        } else {
            failure([self errorForType:AccesssValidationError userInfo:error.userInfo]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([self errorForType:APIServiceError userInfo:error.userInfo]);
    }];
}

#pragma mark - DMR Profile
- (void)obtainDMRProfile:(ESPAuthorization *)authorization  success:(void (^)(NSDictionary *accountDictionary))success failure:(void (^)(NSError *error))failure
{
    if (! [self hasCorrectHeaders]) {
        failure([self errorForType:InvalidHeadersError userInfo:nil]);
        return;
    }
    
    NSString *authorizationString = [NSString stringWithFormat:@"%@ %@", [authorization tokenType], [authorization accessToken]];
    [self setDefaultHeader:@"Authorization" value:authorizationString];
    
    NSString *restPath = [NSString stringWithFormat:@"/%@/dmr/profile", [self restDomain]];
    [self getPath:restPath parameters:nil success:^(AFHTTPRequestOperation *operation, id response) {
        if (IsEmpty(response)) {
            failure([self errorForType:APIServiceError userInfo:nil]);
            return;
        }
        
        NSError *error = nil;
        id result = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        // ESPLog(@"entitlement response: %@", [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding]);
        if (! error) {
            if ([result isKindOfClass:[NSDictionary class]]) {
                success(result);
            } else if ([result isKindOfClass:[NSArray class]]) {
                success([result objectAtIndex:0]);
            } else {
                failure([self errorForType:APIServiceError userInfo:error.userInfo]);
            }
        } else {
            failure([self errorForType:AccesssValidationError userInfo:error.userInfo]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([self errorForType:APIServiceError userInfo:error.userInfo]);
    }];
}

@end
