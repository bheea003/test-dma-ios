//
//  ESPConsumerPlaybackToken.h
//  
//
//  Created by Josh Paul on 5/17/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESPConsumerPlaybackToken : NSObject {
    NSNumber *duration;
    NSNumber *idleTimeout;
    NSString *token;
    NSString *userId;
    NSString *userName;
}

@property (nonatomic, copy) NSNumber *duration;
@property (nonatomic, copy) NSNumber *idleTimeout;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *userName;

+ (ESPConsumerPlaybackToken *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
