
//
//  ESPAPIClient.m
//  ESPFoundation
//
//  Created by JP on 4/15/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPAPIClient.h"
#import "ESPConvenience.h"
#import "AFNetworking.h"
#import "NSString+ESP.h"

NSString *const ESPAPIBaseURL =
                                #if DEBUG
                                    @"http://dev.api.studios.disney.go.com";
                                #else
                                    @"http://api.studios.disney.go.com";
                                #endif

@implementation ESPAPIClient

+ (ESPAPIClient *)clientForApplication:(NSString *)applicationName language:(NSString *)iso639dash1 country:(NSString *)iso3166dash1alpha2
{
    static ESPAPIClient *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURL *url = [NSURL URLWithString:ESPAPIBaseURL];
        __sharedInstance = [[ESPAPIClient alloc] initWithBaseURL:url];
        [__sharedInstance setDefaultHeader:@"Client" value:applicationName];
        [__sharedInstance setDefaultHeader:@"Language" value:iso639dash1];
        [__sharedInstance setDefaultHeader:@"Country" value:iso3166dash1alpha2];
        
        [__sharedInstance registerHTTPOperationClass:[AFJSONRequestOperation class]];
    });
    
    if (! [__sharedInstance hasCorrectHeaders]) {
        ESPLog("ESPClient ALERT: Please check your client settings:\n\tapplicationName is:%@\n\tlanguage (should be ISO 639-1) is:%@\n\tcountry (should be ISO 3166-1 alpha-2) is:%@", [__sharedInstance defaultValueForHeader:@"Client"], [__sharedInstance defaultValueForHeader:@"Language"], [__sharedInstance defaultValueForHeader:@"Country"]);
    }
    return __sharedInstance;
}

- (NSString *)restDomain
{
    NSArray *baseREST = [[self domain] componentsSeparatedByString:@"."];
    return [baseREST lastObject];
}

#pragma mark - Status
- (void)obtainStatus:(void (^)(NSDictionary *result))success failure:(void (^)(NSError *error))failure
{
    if (! [self hasCorrectHeaders]) {
        failure([self errorForType:InvalidHeadersError userInfo:nil]);
        return;
    }
    
    NSString *restPath = [NSString stringWithFormat:@"/%@/status", [self restDomain]];
    [self getPath:restPath parameters:nil success:^(AFHTTPRequestOperation *operation, id response) {
        if (IsEmpty(response)) {
            failure([self errorForType:APIServiceError userInfo:nil]);
            return;
        }
        
        NSError *error = nil;
        id result = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        if (! error) {
            success(result);
        } else {
            failure([self errorForType:AccesssValidationError userInfo:error.userInfo]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([self errorForType:APIServiceError userInfo:error.userInfo]);
    }];
}

#pragma mark - Convenience
- (NSString *)domain
{
    return @"com.disney.esp.core";
}

- (NSString *)applicationName
{
    NSString *applicationName = [self defaultValueForHeader:@"Client"];
    return applicationName;
}

- (NSString *)language
{
    NSString *language = [self defaultValueForHeader:@"Language"];
    return language;
}

- (NSString *)country
{
    NSString *country = [self defaultValueForHeader:@"Country"];
    return country;    
}

- (BOOL)hasCorrectHeaders
{
    BOOL isValidApplication = [self.applicationName length] > 0;
    
    NSPredicate *languagePredicate = [NSPredicate predicateWithFormat:@"SELF IN %@", [NSLocale ISOLanguageCodes]];
    BOOL isValidLanguage = [languagePredicate evaluateWithObject:[self.language lowercaseString]];
    
    NSPredicate *countryPredicate = [NSPredicate predicateWithFormat:@"SELF IN %@", [NSLocale ISOCountryCodes]];
    BOOL isValidCountry = [countryPredicate evaluateWithObject:[self.country uppercaseString]];
    
    BOOL isValid = isValidApplication && isValidLanguage && isValidCountry;
    return isValid;    
}

- (NSError *)errorForType:(ESPAPIBridgeError)errorType userInfo:(NSDictionary *)userInfo
{
    return [NSError errorWithDomain:[self domain] code:errorType userInfo:userInfo];
}

#pragma mark - Request Cancellation
- (void)cancelAllHTTPOperations
{
    [self cancelAllHTTPOperationsWithMethod:nil basePath:[self restDomain]];
}

- (void)cancelAllHTTPOperationsWithMethod:(NSString *)method basePath:(NSString *)path
{
    NSString *pathToBeMatched = [[[self requestWithMethod:(method ?: @"GET") path:path parameters:nil] URL] path];
    
    for (NSOperation *operation in [self.operationQueue operations]) {
        if (![operation isKindOfClass:[AFHTTPRequestOperation class]]) {
            continue;
        }
        
        BOOL hasMatchingMethod = !method || [method isEqualToString:[[(AFHTTPRequestOperation *)operation request] HTTPMethod]];
        BOOL hasMatchingPath = [[[[(AFHTTPRequestOperation *)operation request] URL] path] contains_esp:pathToBeMatched];
        
        if (hasMatchingMethod && hasMatchingPath) {
            [operation cancel];
        }
    }
}

@end
