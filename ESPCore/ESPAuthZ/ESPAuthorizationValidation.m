//
//  ESPAuthorizationValidation.m
//  
//
//  Created by Josh Paul on 7/8/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import "ESPAuthorizationValidation.h"

@implementation ESPAuthorizationValidation

@synthesize authenticator;
@synthesize blue;
@synthesize clientID;
@synthesize clientTaxonomyID;
@synthesize red;
@synthesize refreshTTL;
@synthesize scope;
@synthesize swids;
@synthesize thirdParty;
@synthesize tokenTTL;

+ (ESPAuthorizationValidation *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    ESPAuthorizationValidation *instance = [[ESPAuthorizationValidation alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    [self setValuesForKeysWithDictionary:aDictionary];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"BLUE"]) {
        [self setValue:value forKey:@"blue"];
    } else if ([key isEqualToString:@"client_id"]) {
        [self setValue:value forKey:@"clientID"];
    } else if ([key isEqualToString:@"client_taxonomy_id"]) {
        [self setValue:value forKey:@"clientTaxonomyID"];
    } else if ([key isEqualToString:@"RED"]) {
        [self setValue:value forKey:@"red"];
    } else if ([key isEqualToString:@"refresh_ttl"]) {
        [self setValue:value forKey:@"refreshTTL"];
    } else if ([key isEqualToString:@"third_party"]) {
        [self setValue:value forKey:@"thirdParty"];
    } else if ([key isEqualToString:@"token_ttl"]) {
        [self setValue:value forKey:@"tokenTTL"];
    } else {
        NSLog(@"ESPAuthorizationValidation undefined key from API! %@ [%@]", key, value);
        // [super setValue:value forUndefinedKey:key];
    }
}


- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    if (self.authenticator) {
        [dictionary setObject:self.authenticator forKey:@"authenticator"];
    }
    if (self.blue) {
        [dictionary setObject:self.blue forKey:@"blue"];
    }
    if (self.clientID) {
        [dictionary setObject:self.clientID forKey:@"clientID"];
    }
    if (self.clientTaxonomyID) {
        [dictionary setObject:self.clientTaxonomyID forKey:@"clientTaxonomyID"];
    }
    if (self.red) {
        [dictionary setObject:self.red forKey:@"red"];
    }
    if (self.refreshTTL) {
        [dictionary setObject:self.refreshTTL forKey:@"refreshTTL"];
    }
    if (self.scope) {
        [dictionary setObject:self.scope forKey:@"scope"];
    }
    if (self.swids) {
        [dictionary setObject:self.swids forKey:@"swids"];
    }
    if (self.thirdParty) {
        [dictionary setObject:self.thirdParty forKey:@"thirdParty"];
    }
    if (self.tokenTTL) {
        [dictionary setObject:self.tokenTTL forKey:@"tokenTTL"];
    }
    return dictionary;
}

//===========================================================
//  Keyed Archiving
//===========================================================
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.authenticator forKey:@"authenticator"];
    [encoder encodeObject:self.blue forKey:@"blue"];
    [encoder encodeObject:self.clientID forKey:@"clientID"];
    [encoder encodeObject:self.clientTaxonomyID forKey:@"clientTaxonomyID"];
    [encoder encodeObject:self.red forKey:@"red"];
    [encoder encodeObject:self.refreshTTL forKey:@"refreshTTL"];
    [encoder encodeObject:self.scope forKey:@"scope"];
    [encoder encodeObject:self.swids forKey:@"swids"];
    [encoder encodeObject:self.thirdParty forKey:@"thirdParty"];
    [encoder encodeObject:self.tokenTTL forKey:@"tokenTTL"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.authenticator = [decoder decodeObjectForKey:@"authenticator"];
        self.blue = [decoder decodeObjectForKey:@"blue"];
        self.clientID = [decoder decodeObjectForKey:@"clientID"];
        self.clientTaxonomyID = [decoder decodeObjectForKey:@"clientTaxonomyID"];
        self.red = [decoder decodeObjectForKey:@"red"];
        self.refreshTTL = [decoder decodeObjectForKey:@"refreshTTL"];
        self.scope = [decoder decodeObjectForKey:@"scope"];
        self.swids = [decoder decodeObjectForKey:@"swids"];
        self.thirdParty = [decoder decodeObjectForKey:@"thirdParty"];
        self.tokenTTL = [decoder decodeObjectForKey:@"tokenTTL"];
    }
    return self;
}

@end
