//
//  ESPAuthZ.h
//  DMA
//
//  Created by JP on 7/8/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPAPIClient.h"

@class ESPAuthorization;
@class ESPAuthorizationValidation;
@interface ESPAuthZ : ESPAPIClient

+ (ESPAuthZ *)authZForApplication:(NSString *)applicationName language:(NSString *)iso639dash1 country:(NSString *)iso3166dash1alpha2;

- (void)authorizeUser:(NSString *)username password:(NSString *)password success:(void (^)(ESPAuthorization *result))success failure:(void (^)(NSError *error))failure;
- (void)validate:(ESPAuthorization *)authorization success:(void (^)(ESPAuthorizationValidation *result))success failure:(void (^)(NSError *error))failure;
- (void)refresh:(ESPAuthorization *)authorization success:(void (^)(ESPAuthorization *result))success failure:(void (^)(NSError *error))failure;

@end
