//
//  ESPAuthZ.m
//  DMA
//
//  Created by JP on 7/8/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPAuthZ.h"
#import "ESPAuthorization.h"
#import "ESPAuthorizationValidation.h"
#import "ESPConvenience.h"

NSString *const ESPAuthZBaseURL =
                            #if DEBUG
                                @"https://stg.authorization.go.com";
                            #else
                                @"https://authorization.go.com";
                            #endif

@implementation ESPAuthZ

+ (ESPAuthZ *)authZForApplication:(NSString *)applicationName language:(NSString *)iso639dash1 country:(NSString *)iso3166dash1alpha2
{
    static ESPAuthZ *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURL *url = [NSURL URLWithString:ESPAuthZBaseURL];
        __sharedInstance = [[ESPAuthZ alloc] initWithBaseURL:url];
        [__sharedInstance setDefaultHeader:@"Client" value:applicationName];
        [__sharedInstance setDefaultHeader:@"Language" value:iso639dash1];
        [__sharedInstance setDefaultHeader:@"Country" value:iso3166dash1alpha2];
        
        [__sharedInstance registerHTTPOperationClass:[AFJSONRequestOperation class]];
    });
    
    
    if (! [__sharedInstance hasCorrectHeaders]) {
        NSLog(@"ESPAuthZ ALERT: Please check your client settings:\n\tapplicationName is:%@\n\tlanguage (should be ISO 639-1) is:%@\n\tcountry (should be ISO 3166-1 alpha-2) is:%@", __sharedInstance.applicationName, __sharedInstance.language, __sharedInstance.country);
    }
    return __sharedInstance;
}

- (void)authorizeUser:(NSString *)username password:(NSString *)password success:(void (^)(ESPAuthorization *result))success failure:(void (^)(NSError *error))failure
{
    if (! [self hasCorrectHeaders]) {
        failure([self errorForType:InvalidHeadersError userInfo:nil]);
        return;
    }
    
    NSDictionary *dictionary =  @{@"username":username,
                                  @"password":password,
                                  @"client_id":[self applicationName],
                                  @"grant_type":@"password"};
    
    [self postPath:@"/token" parameters:dictionary success:^(AFHTTPRequestOperation *operation, id response) {
        if (IsEmpty(response)) {
            failure([self errorForType:APIServiceError userInfo:nil]);
            return;
        }
        
        NSError *error = nil;
        id result = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        if (! error) {
            ESPAuthorization *authorization = nil;
            if ([result isKindOfClass:[NSArray class]]) {
                authorization = [ESPAuthorization instanceFromDictionary:[result lastObject]];
            } else if ([result isKindOfClass:[NSDictionary class]]) {
                authorization = [ESPAuthorization instanceFromDictionary:result];
            }
            success(authorization);
        } else {
            failure([self errorForType:AccesssValidationError userInfo:error.userInfo]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([self errorForType:APIServiceError userInfo:error.userInfo]);
    }];
}

- (void)validate:(ESPAuthorization *)authorization success:(void (^)(ESPAuthorizationValidation *result))success failure:(void (^)(NSError *error))failure
{
    if (! [self hasCorrectHeaders]) {
        failure([self errorForType:InvalidHeadersError userInfo:nil]);
        return;
    }
    
    NSString *restPath = [NSString stringWithFormat:@"/validate/%@", [authorization accessToken]];
    [self getPath:restPath parameters:nil success:^(AFHTTPRequestOperation *operation, id response) {
        if (IsEmpty(response)) {
            failure([self errorForType:APIServiceError userInfo:nil]);
            return;
        }
        
        NSError *error = nil;
        id result = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        if (! error) {
            ESPAuthorizationValidation *authorizationValidation = nil;
            if ([result isKindOfClass:[NSArray class]]) {
                authorizationValidation = [ESPAuthorizationValidation instanceFromDictionary:[result lastObject]];
            } else if ([result isKindOfClass:[NSDictionary class]]) {
                authorizationValidation = [ESPAuthorizationValidation instanceFromDictionary:result];
            }
            success(authorizationValidation);
        } else {
            failure([self errorForType:AccesssValidationError userInfo:error.userInfo]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([self errorForType:APIServiceError userInfo:error.userInfo]);
    }];
}

- (void)refresh:(ESPAuthorization *)authorization success:(void (^)(ESPAuthorization *result))success failure:(void (^)(NSError *error))failure
{
    if (! [self hasCorrectHeaders]) {
        failure([self errorForType:InvalidHeadersError userInfo:nil]);
        return;
    }
    
    __block ESPAuthorization *updatedToken = [authorization copy];
    NSDictionary *dictionary =  @{@"client_id":[self applicationName],
                                  @"refresh_token":[authorization refreshToken],
                                  @"grant_type":@"refresh_token"};
    
    [self postPath:@"/token" parameters:dictionary success:^(AFHTTPRequestOperation *operation, id response) {
        if (IsEmpty(response)) {
            failure([self errorForType:APIServiceError userInfo:nil]);
            return;
        }
        ESPAuthorization *refreshed = nil;
        NSError *error = nil;
        id result = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        if (! error) {
            if ([result isKindOfClass:[NSArray class]]) {
                refreshed = [ESPAuthorization instanceFromDictionary:[result lastObject]];
            } else if ([result isKindOfClass:[NSDictionary class]]) {
                refreshed = [ESPAuthorization instanceFromDictionary:result];
            }
            
            updatedToken.accessToken = refreshed.accessToken;
            updatedToken.refreshToken = refreshed.refreshToken;
            updatedToken.expiresIn = refreshed.expiresIn;
            updatedToken.lastUpdated = [NSDate date];
            success(updatedToken);
        } else {
            failure([self errorForType:AccesssValidationError userInfo:error.userInfo]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([self errorForType:APIServiceError userInfo:error.userInfo]);
    }];
}

@end
