//
//  ESPAuthorization
//
//  Created by Josh Paul on 5/14/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import "ESPAuthorization.h"
#import "ESPConsumerPlaybackToken.h"

@implementation ESPAuthorization

@synthesize accessToken;
@synthesize expiresIn;
@synthesize redCookie;
@synthesize refreshToken;
@synthesize swid;
@synthesize tokenType;
@synthesize scope;
@synthesize playbackToken;
@synthesize lastUpdated;

+ (ESPAuthorization *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    ESPAuthorization *instance = [[ESPAuthorization alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    [instance setLastUpdated:[NSDate date]];
    return instance;
}

- (id)copyWithZone:(NSZone *)zone
{
    ESPAuthorization *newAuthorization = [[ESPAuthorization class] allocWithZone:zone];
    [newAuthorization setAttributesFromDictionary:[self dictionaryRepresentation]];
    return newAuthorization;
}

- (NSString *)unbracketedSWID
{
    NSString *unbracketedSWID = [[self swid] stringByReplacingOccurrencesOfString:@"{" withString:@""];
    unbracketedSWID = [unbracketedSWID stringByReplacingOccurrencesOfString:@"}" withString:@""];
    return unbracketedSWID;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    [self setValuesForKeysWithDictionary:aDictionary];

}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"access_token"]) {
        [self setValue:value forKey:@"accessToken"];
    } else if ([key isEqualToString:@"expires_in"]) {
        [self setValue:value forKey:@"expiresIn"];
    } else if ([key isEqualToString:@"red_cookie"]) {
        [self setValue:value forKey:@"redCookie"];
    } else if ([key isEqualToString:@"refresh_token"]) {
        [self setValue:value forKey:@"refreshToken"];
    } else if ([key isEqualToString:@"token_type"]) {
        [self setValue:value forKey:@"tokenType"];
    } else if ([key isEqualToString:@"scope"]) {
        [self setValue:value forKey:@"scope"];
    } else if ([key isEqualToString:@"playbackToken"]) {
        [self setValue:value forKey:@"playbackToken"];
    } else if ([key isEqualToString:@"lastUpdated"]) {
        [self setValue:value forKey:@"lastUpdated"];
    } else {
        NSLog(@"ESPAuthorization undefined key from API! %@ [%@]", key, value);
        // [super setValue:value forUndefinedKey:key];
    }
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    if (self.accessToken) {
        [dictionary setObject:self.accessToken forKey:@"accessToken"];
    }
    if (self.expiresIn) {
        [dictionary setObject:self.expiresIn forKey:@"expiresIn"];
    }
    if (self.redCookie) {
        [dictionary setObject:self.redCookie forKey:@"redCookie"];
    }
    if (self.refreshToken) {
        [dictionary setObject:self.refreshToken forKey:@"refreshToken"];
    }
    if (self.swid) {
        [dictionary setObject:self.swid forKey:@"swid"];
    }
    if (self.tokenType) {
        [dictionary setObject:self.tokenType forKey:@"tokenType"];
    }
    if (self.scope) {
        [dictionary setObject:self.scope forKey:@"scope"];
    }
    if (self.playbackToken) {
        [dictionary setObject:self.playbackToken forKey:@"playbackToken"];
    }
    if (self.lastUpdated) {
        [dictionary setObject:self.lastUpdated forKey:@"lastUpdated"];
    }
    return dictionary;
}

//===========================================================
//  Keyed Archiving
//===========================================================
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.accessToken forKey:@"accessToken"];
    [encoder encodeObject:self.expiresIn forKey:@"expiresIn"];
    [encoder encodeObject:self.redCookie forKey:@"redCookie"];
    [encoder encodeObject:self.refreshToken forKey:@"refreshToken"];
    [encoder encodeObject:self.swid forKey:@"swid"];
    [encoder encodeObject:self.tokenType forKey:@"tokenType"];
    [encoder encodeObject:self.scope forKey:@"scope"];
    [encoder encodeObject:self.playbackToken forKey:@"playbackToken"];
    [encoder encodeObject:self.lastUpdated forKey:@"lastUpdated"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.accessToken = [decoder decodeObjectForKey:@"accessToken"];
        self.expiresIn = [decoder decodeObjectForKey:@"expiresIn"];
        self.redCookie = [decoder decodeObjectForKey:@"redCookie"];
        self.refreshToken = [decoder decodeObjectForKey:@"refreshToken"];
        self.swid = [decoder decodeObjectForKey:@"swid"];
        self.tokenType = [decoder decodeObjectForKey:@"tokenType"];
        self.scope = [decoder decodeObjectForKey:@"scope"];
        self.playbackToken = [decoder decodeObjectForKey:@"playbackToken"];
        self.lastUpdated = [decoder decodeObjectForKey:@"lastUpdated"];
    }
    return self;
}

@end
