//
//  ESPAuthorization
//  
//
//  Created by Josh Paul on 5/14/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ESPConsumerPlaybackToken;
@interface ESPAuthorization : NSObject {
    NSString *accessToken;
    NSNumber *expiresIn;
    NSString *redCookie;
    NSString *refreshToken;
    NSString *swid;
    NSString *tokenType;
    NSString *scope;
    ESPConsumerPlaybackToken *playbackToken;
    NSDate *lastUpdated;
}

@property (nonatomic, copy) NSString *accessToken;
@property (nonatomic, copy) NSNumber *expiresIn;
@property (nonatomic, copy) NSString *redCookie;
@property (nonatomic, copy) NSString *refreshToken;
@property (nonatomic, copy) NSString *swid;
@property (nonatomic, copy) NSString *tokenType;
@property (nonatomic, copy) NSString *scope;
@property (nonatomic, strong) ESPConsumerPlaybackToken *playbackToken;
@property (nonatomic, strong) NSDate *lastUpdated;

+ (ESPAuthorization *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;
- (NSString *)unbracketedSWID;

@end
