//
//  ESPAuthorizationValidation.h
//  
//
//  Created by Josh Paul on 7/8/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESPAuthorizationValidation : NSObject {
    NSString *authenticator;
    NSString *blue;
    NSString *clientID;
    NSString *clientTaxonomyID;
    NSString *red;
    NSNumber *refreshTTL;
    NSString *scope;
    NSString *swids;
    NSString *thirdParty;
    NSNumber *tokenTTL;
}

@property (nonatomic, copy) NSString *authenticator;
@property (nonatomic, copy) NSString *blue;
@property (nonatomic, copy) NSString *clientID;
@property (nonatomic, copy) NSString *clientTaxonomyID;
@property (nonatomic, copy) NSString *red;
@property (nonatomic, copy) NSNumber *refreshTTL;
@property (nonatomic, copy) NSString *scope;
@property (nonatomic, copy) NSString *swids;
@property (nonatomic, copy) NSString *thirdParty;
@property (nonatomic, copy) NSNumber *tokenTTL;

+ (ESPAuthorizationValidation *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
