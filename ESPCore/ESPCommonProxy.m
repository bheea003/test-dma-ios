//
//  ESPCommonProxy.m
//  DMA
//
//  Created by JP on 5/23/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPCommonProxy.h"
#import "ESPUIAppDelegate.h"
#import "ESPKeychainItemWrapper.h"
#import "ESPCatalogCategory.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "NSString+ESP.h"

@implementation ESPCommonProxy

+ (id)sharedProxy
{
    static dispatch_once_t onceQueue;
    static ESPCommonProxy *proxy = nil;
    
    dispatch_once(&onceQueue, ^{
        proxy = [[self alloc] init];
        [proxy restore];
        NSLog(@"%@", [proxy licenseArray]);
        
    });
    return proxy;
}

#pragma mark - Accessors
- (NSString *)applicationName
{
    if (! _applicationName) {
        _applicationName = @"coolMovies";
    }
    return _applicationName;
}

- (NSString *)applicationLanguage
{
    if (! _applicationLanguage) {
        _applicationLanguage = @"EN";
    }
    return _applicationLanguage;
}

- (NSString *)applicationCountry
{
    if (! _applicationCountry) {
        _applicationCountry = @"US";
    }
    return _applicationCountry;
}

#pragma mark - ESPMedia convenience
- (BOOL)hasEntitlementToMedia:(ESPMedia *)media
{
    return [self.ownedMedia containsObject:media];
}

- (ESPMedia *)entitledMediaForRawMedia:(ESPMedia *)media
{
    if ([self hasEntitlementToMedia:media]) {
        int indexOfOwnedMovie = [self.ownedMedia indexOfObject:media];
        ESPMedia *ownedMovie = [self.ownedMedia objectAtIndex:indexOfOwnedMovie];
        return ownedMovie;
    }
    return nil;
}

#pragma mark - ESPClient
- (ESPClient *)client
{
    return [ESPClient clientForApplication:[self applicationName] language:[self applicationLanguage] country:[self applicationCountry]];
}

- (void)validateAccess
{
    [[self client] validateAccess:nil success:^(NSString *country) {
        self.applicationCountry = country;
        self.hasValidAccess = YES;
    } failure:^(NSError *error) {
        self.hasValidAccess = NO;
    }];
}

- (void)validateUpgradeStatus
{
    [[self client] validateApplication:[self applicationName] systemVersion:[[UIDevice currentDevice] systemVersion] appversion:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] success:^(ESPClientUpgrade *upgrade) {
        self.clientUpgrade = upgrade;
    } failure:^(NSError *error) {
        self.clientUpgrade = nil;
    }];
}

- (ESPClientUpgradeStatus)upgradeStatus
{
    if (self.clientUpgrade) {
        return [self.clientUpgrade upgradeStatus];
    }
    return UpgradeError;
}

#pragma mark - ESPCatalog
- (ESPCatalog *)catalog
{
    return [ESPCatalog catalogForApplication:[self applicationName] language:[self applicationLanguage] country:[self applicationCountry]];
}

- (void)obtainCategoriesMinus:(NSArray *)categoryNames success:(void (^)(NSArray *categories))success failure:(void (^)(NSError *error))failure
{
    NSMutableArray *remove = [NSMutableArray array];
    for (NSString *categoryName in categoryNames) {
        ESPCatalogCategory *category = [[ESPCatalogCategory alloc] init];
        category.urlTitle = categoryName;
        category.userTitle = categoryName;
        [remove addObject:category];
    }
    self.isLoading = YES;
    [[self catalog] obtainAllCategoriesFilteringOut:remove success:^(NSArray *filteredCategories) {
        success(filteredCategories);
        self.isLoading = NO;
    } failure:^(NSError *error) {
        failure(error);
        self.isLoading = NO;
    }];
}

- (void)obtainTypes:(void (^)(NSArray *types))success failure:(void (^)(NSError *error))failure
{
    self.isLoading = YES;
    [[self catalog] obtainAllTypes:^(NSArray *result) {
        success(result);
    } failure:^(NSError *error) {
        failure(error);
    }];
}


#pragma mark - ESPConsumer
- (ESPConsumer *)consumer
{
    return [ESPConsumer consumerForApplication:[self applicationName] language:[self applicationLanguage] country:[self applicationCountry]];
}

- (void)login:(NSString *)login password:(NSString *)password
{
    self.isLoading = YES;
    [[self consumer] login:login password:password success:^(ESPAuthorization *authorization) {
        [self archiveLogin:login password:password];
        self.authorization = authorization;
        [self obtainOwnedMovies];
    } failure:^(NSError *error) {
        [self setAuthorization:nil];
        [self displayLoginForm:NSLocalizedString(@"Invalid Credentials. Please try again.", @"Invalid Credentials. Please try again.")];
        self.isLoading = NO;
    }];
}

- (void)logout
{
    if ([[self authorization] playbackToken]) {
        [[self consumer] logout:[self authorization] success:^(NSString *result) {
            [self removeLogin];
        } failure:^(NSError *error) {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                        message:[error description]
                                       delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                              otherButtonTitles:nil] show];
            [self removeLogin];
        }];
    } else {
        self.authorization = nil;
        self.ownedMedia = nil;
    }
}

- (void)obtainOwnedMovies
{
    if (! self.authorization) {
        self.isLoading = NO;
        return;
    }
    
    [[self consumer] obtainOwnedMovies:self.authorization ratingsFilter:nil success:^(NSArray *movies) {
        NSMutableOrderedSet *mediaSet = [NSMutableOrderedSet orderedSetWithArray:movies];
        [self setOwnedMedia:mediaSet];
        self.isLoading = NO;
    } failure:^(NSError *error) {
        [self setOwnedMedia:nil];
        self.isLoading = NO;
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                    message:[error localizedDescription]
                                   delegate:self
                          cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                          otherButtonTitles:nil] show];
    }];
}

#pragma mark Login
- (void)displayLoginForm:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Login", @"Login")
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                          otherButtonTitles:NSLocalizedString(@"Submit", @"Submit"), nil];
    alert.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    alert.backgroundColor = [UIColor clearColor];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != [alertView cancelButtonIndex]) {
        // The login field is at index 0. The password field is at index 1
        NSString *login = [[alertView textFieldAtIndex:0] text];
        NSString *password = [[alertView textFieldAtIndex:1] text];
        [self login:login password:password];
    } else {
        self.authorization = nil;
        self.ownedMedia = nil;
    }
}

#pragma mark - Authorization storage
- (ESPKeychainItemWrapper *)wrapper
{
    NSString *appIdentifier = [ESPUIAppDelegate appIdentifier]; // @"G58WL7FR8E.com.disney.GenericKeychainSuite";
    ESPKeychainItemWrapper *wrapper = [[ESPKeychainItemWrapper alloc] initWithIdentifier:@"Authorization" accessGroup:appIdentifier];
    return wrapper;
}

- (NSString *)secureLoginKey
{
    return b_kSecAttrAccount;
}

- (NSString *)securePasswordKey
{
    return b_kSecAttrDescription;
}

- (void)restore
{
    NSString *login = [[self wrapper] objectForKey:[self secureLoginKey]];
    NSString *password = [[self wrapper] objectForKey:[self securePasswordKey]];
    if (! IsEmpty(login) && ! IsEmpty(password)) {
        [self login:login password:password];
    }
}

- (void)archiveLogin:(NSString *)login password:(NSString *)password
{
    [[self wrapper] setObject:login forKey:[self secureLoginKey]];
    [[self wrapper] setObject:password forKey:[self securePasswordKey]];
}

- (void)removeLogin
{
    self.authorization = nil;
    self.ownedMedia = nil;
    [[self wrapper] resetKeychainItem];
}

#pragma mark - Convenience
- (void)shouldEnableNetworkActivityIndicator:(BOOL)flag
{
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:flag];
}

- (NSDictionary *)developerInfo
{
    return [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"Domain", @"Version", nil] forKeys:[NSArray arrayWithObjects:@"com.disney.esp", @"1.0", nil]];
}

- (NSDictionary *)licenses
{
    NSString *licenses = [[NSBundle mainBundle] pathForResource:@"_ESPLicenses" ofType:@"plist"];
    NSDictionary *licenseDictionary = [NSDictionary dictionaryWithContentsOfFile:licenses];
    return licenseDictionary;
}

- (NSArray *)licenseArray
{
    NSMutableArray *licenseArray = [NSMutableArray array];
    for (NSString *key in [[self licenses] allKeys]) {
        NSString *license = [[self licenses] valueForKey:key];
        license = [[license componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
        [licenseArray addObject:[NSString stringWithFormat:@"%@: %@", key, license]];
    }
    return licenseArray;
}

@end
