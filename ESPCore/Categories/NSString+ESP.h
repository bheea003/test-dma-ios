//
//  NSString+ESP.h
//

#import <Foundation/Foundation.h>

@interface NSString (ESP)
 
- (BOOL)contains_esp:(NSString *)string;
- (NSString *)stringByReplacingPhoneNumberMarkdown_esp;
- (NSString *)stringByReplacingCMSLink_esp;
- (NSString *)stringByStrippingUnicodeCharacters_esp;
- (NSString *)urlEncodedString_esp;

@end
