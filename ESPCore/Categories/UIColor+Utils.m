//
//  UIColor+Utils.m
//

#import "UIColor+Utils.h"

@implementation UIColor (Utils)

+ (UIColor *)colorWithString:(NSString *)string 
{
    unsigned int c;
    if ([string characterAtIndex:0] == '#') 
    {
        [[NSScanner scannerWithString:[string substringFromIndex:1]] scanHexInt:&c];
    }
    else
    {
        [[NSScanner scannerWithString:string] scanHexInt:&c];
    }
    return [UIColor colorWithRed:((c & 0xff0000) >> 16)/255.0 green:((c & 0xff00) >> 8)/255.0 blue:(c & 0xff)/255.0 alpha:1.0];
}

+ (UIColor *)colorWithHexString:(NSString *)stringToConvert 
{
    // based on code from https://github.com/ars/uicolor-utilities/

    if ([stringToConvert characterAtIndex:0] == '#')
    {
        stringToConvert = [stringToConvert substringFromIndex:1];
    }
    
	NSScanner *scanner = [NSScanner scannerWithString:stringToConvert];
    
	unsigned hexNum;
	if (![scanner scanHexInt:&hexNum]) return nil;
    
    int red = (hexNum >> 16) & 0xFF;
	int green = (hexNum >> 8) & 0xFF;
	int blue = (hexNum) & 0xFF;
    
    float r = (red/255.0f);
    float g = (green/255.0f);
    float b = (blue/255.0f);
    
	return [UIColor colorWithRed:r green:g blue:b alpha:1.0f];
}

@end
