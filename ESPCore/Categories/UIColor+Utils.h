//
//  UIColor+Utils.h
//

#import <Foundation/Foundation.h>

@interface UIColor (Utils)

+ (UIColor *)colorWithString:(NSString *)string;
// Returns a UIColor by scanning the string for a hex number
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert;

@end
