//
//  UIButton+Badge.m
//  buttonBadge
//
//  Created by Stephen Kochan on 3/13/12.
//  Copyright (c) 2012 Disney. All rights reserved.
//

#import "UIButton+Badge.h"

#define BADGETAG    1234
#define LABELTAG    5678

@implementation UIButton (Badge)

-(void) enableBadge: (BOOL) showIt 
{
    UIImageView *badgeView =  (UIImageView *) [self viewWithTag: BADGETAG];
    if (badgeView) 
        badgeView.alpha = showIt;
}

-(BOOL) hasBadge
{
    return [self viewWithTag: BADGETAG] != nil;
}


// Helvetica Neue Bold 11pt #064266. 

-(BOOL) addBadgeWithValue: (int) badgeValue
{
    UIImage *badge = [UIImage imageNamed: @"indicator_yellow~iphone.png"];

    if (!badge)
        return NO;
    
    // We allow updating an existing badge here
    
    UIImageView *badgeView = (UIImageView *) [self viewWithTag: BADGETAG];

    if (badgeView) {
        [self updateBadgeValue: badgeValue];
        return YES;
    }
        
    badgeView = [[UIImageView alloc] initWithImage: badge];
    
    if (!badgeView)
        return NO;
    
    // Add the badge image
                         
    CGPoint badgeCenter = CGPointMake (self.bounds.size.width - 2, 4);          
    badgeView.center = badgeCenter;
    badgeView.tag = BADGETAG;
    badgeView.clipsToBounds = YES;
    
    // Add the label and set to the badgeValue
    
    UILabel *badgeLabel = [[UILabel alloc] initWithFrame: badgeView.bounds];
    badgeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size: 11];

    CGPoint labelCenter = badgeLabel.center;
    labelCenter.x -= 1;
    labelCenter.y -= 1;
    badgeLabel.center = labelCenter;

    badgeLabel.textAlignment = NSTextAlignmentCenter;
    badgeLabel.backgroundColor = [UIColor clearColor];    
    badgeLabel.textColor = [UIColor blackColor];
    badgeLabel.adjustsFontSizeToFitWidth = YES;
    badgeLabel.text = [NSString stringWithFormat: @"%i", badgeValue];
    badgeLabel.tag = LABELTAG;
    
    [badgeView addSubview: badgeLabel];
    [self addSubview: badgeView]; 
                                  
    return YES;
}

-(void) removeBadge
{
    UIImageView *badgeView = (UIImageView *) [self viewWithTag: BADGETAG];
    [badgeView removeFromSuperview];
}

-(int) badgeValue
{
    UILabel *badgeLabel = (UILabel *) [self viewWithTag: LABELTAG];
    
    if (badgeLabel)
        return [badgeLabel.text intValue];
    else 
        return 0;
}

-(void) updateBadgeValue: (int) value
{
    UILabel *badgeLabel = (UILabel *) [self viewWithTag: LABELTAG];
    
    if (badgeLabel)
        badgeLabel.text = [NSString stringWithFormat: @"%i", value];
}
@end