//
//  UIButton+Badge.h
//  buttonBadge
//
//  Created by Stephen Kochan on 3/13/12.
//  Copyright (c) 2012 Disney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Badge)

- (BOOL)addBadgeWithValue:(int)badgeValue;
- (void)removeBadge;
- (void)enableBadge:(BOOL)showIt;

- (int)badgeValue;
- (void)updateBadgeValue:(int)value;
- (BOOL)hasBadge;

@end
