//
//  NSString+ESP.m
//

#import "ESPConvenience.h"
#import "NSString+ESP.h"

@implementation NSString (ESP)

- (BOOL)contains_esp:(NSString *)string
{
    NSRange range = [self rangeOfString:string];
    return (range.location != NSNotFound);
}

- (NSString *)stringByReplacingPhoneNumberMarkdown_esp
{
    if (IsEmpty(self))
    {
        return self;
    }
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\[\\bTEL:(\\b[^|]+)\\|([^]]+)\\]" options:0 error:nil];
    return [regex stringByReplacingMatchesInString:self options:0 range:NSMakeRange(0, [self length]) withTemplate:@"<a href=\"tel:$1\">$2</a>"];
}

- (NSString *)stringByReplacingCMSLink_esp
{
    if (IsEmpty(self))
    {
        return self;
    }
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\[\\bLINK:(\\b[^|]+)\\|([^]]+)\\]" options:0 error:nil];
    return [regex stringByReplacingMatchesInString:self options:0 range:NSMakeRange(0, [self length]) withTemplate:@"<a href=\"dma://$1\">$2</a>"];
}

- (NSString *)stringByStrippingUnicodeCharacters_esp
{
    NSString *updatedString = [self copy];
    
    updatedString = [updatedString stringByReplacingOccurrencesOfString:@"&#145;" withString:@"\\u2018"];// ' (left)
    updatedString = [updatedString stringByReplacingOccurrencesOfString:@"&#146;" withString:@"\\u2019"];// ' (right)
    updatedString = [updatedString stringByReplacingOccurrencesOfString:@"&#147;" withString:@"\\u201C"];// " (left)
    updatedString = [updatedString stringByReplacingOccurrencesOfString:@"&#148;" withString:@"\\u201D"];// " (right)
    updatedString = [updatedString stringByReplacingOccurrencesOfString:@"&#149;" withString:@"\\u2022"];// bullet
    updatedString = [updatedString stringByReplacingOccurrencesOfString:@"&#150;" withString:@"\\u2013"];// endash
    updatedString = [updatedString stringByReplacingOccurrencesOfString:@"&#151;" withString:@"\\u2014"];// emdash
    updatedString = [updatedString stringByReplacingOccurrencesOfString:@"&#153;" withString:@"\\u2122"];// tm
    updatedString = [updatedString stringByReplacingOccurrencesOfString:@"&#169;" withString:@"\\u00A9"];// c
    updatedString = [updatedString stringByReplacingOccurrencesOfString:@"&#174;" withString:@"\\u00AE"];// r
    
    return (__bridge_transfer NSString *) CFStringCreateFromExternalRepresentation(0, (CFDataRef)[updatedString dataUsingEncoding:NSUTF8StringEncoding], kCFStringEncodingNonLossyASCII);
}

- (NSString *)urlEncodedString_esp
{
    // via Dave DeLong: http://stackoverflow.com/questions/3423545/objective-c-iphone-percent-encode-a-string/3426140#3426140
    NSMutableString * output = [NSMutableString string];
    const unsigned char * source = (const unsigned char *)[self UTF8String];
    int sourceLen = strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

@end
