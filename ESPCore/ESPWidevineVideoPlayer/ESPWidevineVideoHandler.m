//
//  ESPWidevineVideoPlayer.m
//  ESPCore
//
//  Created by JP on 5/16/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPWidevineVideoHandler.h"
#import "ESPConvenience.h"
#import "ESPConsumer.h"
#import "ESPAuthorization.h"
#import "ESPConsumerPlaybackToken.h"
#import "ESPMediaContent.h"
#import "ESPWidevineSettings.h"
#import "ESPWidevineLicenseProxy.h"

#import "AFXMLRequestOperation.h"

@interface ESPWidevineVideoHandler()
@property (assign, nonatomic, readwrite) WViOsApiStatus status;
- (void)showSimulatorMessage;
@end

@implementation ESPWidevineVideoHandler

/**
	@returns the domain (used in NSError)
 */
- (NSString *)domain
{
    return @"com.disney.esp.widevineplayer";
}

#pragma mark - Playback/Shutdown
/**
	Queries Widevine license server and returns the URL appropriate for playback.
	@param media an ESPMedia (aka Movie) object
	@param authorization an ESPAuthorization with a valid playbackToken
 */
- (void)play:(ESPMedia *)media authorization:(ESPAuthorization *)authorization success:(void (^)(NSURL *playbackURL))success failure:(void (^)(NSError *error))failure
{
    if (IsEmpty(authorization.playbackToken)) {
        NSError *wvError = [NSError errorWithDomain:[self domain] code:0 userInfo:[NSDictionary dictionaryWithObject:NSLocalizedString(@"Playback Token Missing", @"Playback Token Missing") forKey:@"reason"]];
        failure(wvError);
        return;
    }
    
    ESPRETURNIFSIMULATOR(); // If running in simulator, return
    if (IsEmpty([[media widevineContent] pid])) {
        NSDictionary *errorDictionary = @{@"reason":NSLocalizedString(@"Missing PID", @"Missing PID")};
        NSError *error = [NSError errorWithDomain:[self domain] code:7500 userInfo:errorDictionary];
        failure(error);
        return;
    }
    
    NSString *fileURL = [[media widevineContent] fileUrl];
    if (! IsEmpty(fileURL)) {
        NSString *widevineURL = [NSString stringWithFormat:@"%@&auth=%@&Affiliate=%@&UserName=%@&format=smil", fileURL, [authorization.playbackToken token], [media guid], [authorization unbracketedSWID]];
        [self licenseWidevineURL:widevineURL success:^(NSString *playbackURL) {
            NSString *pid = [[media widevineContent] pid];
            NSString *token = [[authorization playbackToken] token];
            ESPLog(@"\nPID: %@\nTOKEN: %@", pid, token);
            [self setupUsingPID:pid token:token];
            
            NSMutableString *moviePlayURL = [[NSMutableString alloc] init];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused"
            self.status = WV_Play(playbackURL, moviePlayURL, 0);
#pragma clang diagnostic pop
            
            if (self.status != WViOsApiStatus_OK) {
                NSString *errorString = (self.status < ESPWVErrorCount) ? ESPWidevineErrors[self.status] : NSLocalizedString(@"Unknown Error", @"Unknown Error");
                NSError *wvError = [NSError errorWithDomain:[self domain] code:0 userInfo:[NSDictionary dictionaryWithObject:errorString forKey:@"reason"]];
                failure(wvError);
            } else {
                NSURL *url = [NSURL URLWithString:moviePlayURL];
                success(url);
            }
        } failure:^(NSError *error) {
            failure(error);
        }];
        
        // non-SMIL
        // NSString *widevineURL = [NSString stringWithFormat:@"%@&auth=%@&Affiliate=%@&UserName=%@", fileURL, [token token], [media guid], [authorization swid]];
        // [self playWidevineURL:widevineURL success:^(NSURL *playbackURL) { success(playbackURL); } failure:^(NSError *error) { failure(error); }];
    } else {
        NSError *error = [NSError errorWithDomain:[self domain] code:0 userInfo:[NSDictionary dictionaryWithObject:NSLocalizedString(@"Content is not DRM protected.", @"Content is not DRM protected") forKey:@"reason"]];
        failure(error);
    }
}

- (void)playWidevineURL:(NSString *)widevineURL success:(void (^)(NSURL *playbackURL))success failure:(void (^)(NSError *error))failure
{
    NSError *error = nil;
    if (self.status != WViOsApiStatus_OK) {
        NSString *errorString =  (self.status < ESPWVErrorCount) ? ESPWidevineErrors[self.status] : NSLocalizedString(@"Unknown Error", @"Unknown Error");
        error = [NSError errorWithDomain:[self domain] code:0 userInfo:[NSDictionary dictionaryWithObject:errorString forKey:@"reason"]];
        failure(error);
    } else {
        NSMutableString *moviePlayURL = [[NSMutableString alloc] init];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused"
        self.status = WV_Play(widevineURL, moviePlayURL, 0);
#pragma clang diagnostic pop
        if (self.status != WViOsApiStatus_OK) {
            NSString *errorString = (self.status < ESPWVErrorCount) ? ESPWidevineErrors[self.status] : NSLocalizedString(@"Unknown Error", @"Unknown Error");
            NSError *wvError = [NSError errorWithDomain:[self domain] code:WIDEVINE_PLAYBACK_ERROR userInfo:[NSDictionary dictionaryWithObject:errorString forKey:@"reason"]];
            failure(wvError);
        } else {
            NSURL *url = [NSURL URLWithString:moviePlayURL];
            success(url);
        }
    }
}

- (void)licenseWidevineURL:(NSString *)widevineURL success:(void (^)(NSString *playbackURL))success failure:(void (^)(NSError *error))failure
{
    NSError *error = nil;
    if (self.status != WViOsApiStatus_OK) {
        NSString *errorString =  (self.status < ESPWVErrorCount) ? ESPWidevineErrors[self.status] : NSLocalizedString(@"Unknown Error", @"Unknown Error");
        error = [NSError errorWithDomain:[self domain] code:0 userInfo:[NSDictionary dictionaryWithObject:errorString forKey:@"reason"]];
        failure(error);
    } else {
        ESPWidevineLicenseProxy *license = [[ESPWidevineLicenseProxy alloc] init];
        [license processURL:widevineURL success:^(NSString *movieURL) {
            success(movieURL);
        } failure:^(NSError *error) {
            NSString *errorString = NSLocalizedString(@"Unknown Error", @"Unknown Error");
            int code = [error code];
            switch (code) {
                case InvalidSMIL:
                    code = WIDEVINE_INVALID_ENTITLEMENT_ERROR;
                    break;
                case GeolocationViolation:
                    code = WIDEVINE_GEOLOCATION_BLOCKED_ERROR;
                    break;
                case ConcurrencyViolation:
                    code = WIDEVINE_PLAYBACK_CONCURRENCY_LIMIT_REACHED_ERROR;
                    break;
                default:
                    code = WIDEVINE_PLAYBACK_ERROR;
                    break;
            }
            error = [NSError errorWithDomain:[self domain] code:code userInfo:[NSDictionary dictionaryWithObject:errorString forKey:@"reason"]];
            failure(error);
        }];
    }
}

#pragma mark - Widevine Convenience
- (void)setupUsingPID:(NSString *)pid token:(NSString *)token
{
    if (! self.settings) {
        self.settings = [[ESPWidevineSettings alloc] init];
    }
    
    [self endPlayback];
    [self.settings setupUsingPID:pid token:token];
    
    ESPLog(@"calling WV_Initialize with %@", [self.settings settingsDictionary]);
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused"
    self.status = WV_Initialize(ESP_WVStatusCallback, [self.settings settingsDictionary]);
#pragma clang diagnostic pop
}

- (void)showSimulatorMessage
{
    // this is a dev-only feature, so no need to localize
    [[[UIAlertView alloc] initWithTitle:@"Simulator"
                                message:@"Widevine playback is unavailable in the simulator."
                               delegate:nil
                      cancelButtonTitle:@"Fine"
                      otherButtonTitles:nil] show];
}

- (NSString *)widevineStatusAsString
{
    NSString *status = nil;
    WViOsApiStatus wvStatus = WV_QueryAssetsStatus();
    switch (wvStatus) {
        case WViOsApiStatus_OK: {
            status =  @"OK";
            break;
        }
        case WViOsApiStatus_NotInitialized: {
            status = @"Not Initialized";
            break;
        }
        case WViOsApiStatus_AlreadyInitialized: {
            status = @"Already Initialized";
            break;
        }
        case WViOsApiStatus_CantConnectToMediaServer: {
            status = @"Can't Connect To Media Server";
            break;
        }
        case WViOsApiStatus_BadMedia: {
            status = @"Bad Media";
            break;
        }
        case WViOsApiStatus_CantConnectToDrmServer: {
            status = @"Can't Connect To DRM Server";
            break;
        }
        case WViOsApiStatus_NotEntitled: {
            status = @"Not Entitled";
            break;
        }
        case WViOsApiStatus_EntitlementDenied: {
            status = @"Entitlement Denied";
            break;
        }
        case WViOsApiStatus_LostConnection: {
            status = @"Lost Connection";
            break;
        }
        case WViOsApiStatus_EntitlementExpired: {
            status = @"Entitlement Expired";
            break;
        }
        case WViOsApiStatus_NotEntitledByRegion: {
            status = @"Not Entitled By Region";
            break;
        }
        case WViOsApiStatus_BadUrl: {
            status = @"Bad URL";
            break;
        }
        case WViOsApiStatus_FileNotPresent: {
            status = @"File Not Present";
            break;
        }
        case WViOsApiStatus_NotRegistered: {
            status = @"Not Registered";
            break;
        }
        case WViOsApiStatus_AlreadyRegistered: {
            status = @"Already Registered";
            break;
        }
        case WViOsApiStatus_NotPlaying: {
            status = @"Not Playing";
            break;
        }
        case WViOsApiStatus_AlreadyPlaying: {
            status = @"Already Playing";
            break;
        }
        case WViOsApiStatus_FileSystemError: {
            status = @"File System Error";
            break;
        }
        case WViOsApiStatus_AssetDBWasCorrupted: {
            status = @"AssetDB Was Corrupted";
            break;
        }
        case WViOsApiStatus_JailBreakDetected: {
            status = @"Jail Break Detected";
            break;
        }
        case WViOsApiStatus_UnknownError: {
            status = @"Unknown Error";
            break;
        }
    }
    return status;
}

#pragma mark - Movie playback
- (NSURL *)widevineCompatibleURLForFile:(NSString *)path
{
    [self setupUsingPID:nil token:nil];
    
    NSString *asset = [NSString stringWithFormat:@"/%@",path];
    NSMutableString *urlString = [NSMutableString string];
    NSURL *url;
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused"
    self.status = WV_QueryAssetStatus(asset);
#pragma clang diagnostic pop
    [self setupOnlineStatusForAsset:asset];
    if (self.status == WViOsApiStatus_OK) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused"
        self.status = WV_Play(asset, urlString, 0);
#pragma clang diagnostic pop
    }
    
    if (self.status == WViOsApiStatus_OK) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, path];
        
        url = [NSURL URLWithString:(([self isEncrypted:filePath] == YES) ? urlString : filePath)] ;
        return url;
    }
    return nil;
}

#pragma mark - Reachability convenience
- (BOOL)isReachable:(NSString *)hostName // DO NOT USE http://; just use host name
{
    BOOL result = NO;
    @autoreleasepool {
        SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL, [hostName UTF8String]);
        SCNetworkReachabilityFlags flags;
        if (reachability != NULL) {
            if (SCNetworkReachabilityGetFlags(reachability, &flags)) {
                if (flags & kSCNetworkReachabilityFlagsReachable) {
                    result = YES;
                } else {
                    result = NO;
                }
            } else {
                result = NO;
            }
            CFRelease(reachability);
        } else {
            result = NO;
        }
    }
    return result;
}

#pragma mark - Encryption convenience
- (BOOL)isEncrypted:(NSString*)filePath
{
    const char kEncrypedMp4Magic[] = { 0x49, 0x44, 0x4d, 0x20 };
    const char kEncryptedWvmMagic[] = { 0x00, 0x00, 0x01, 0xba };
    BOOL encrypted = NO;
    
    // look at clip
    const char* filepath = [filePath cStringUsingEncoding: NSASCIIStringEncoding];
    FILE *file = fopen(filepath, "r");
    char buffer[4];
    if (file) {
        if (fread(&buffer, sizeof(buffer), 1, file) == 1) {
            encrypted = !memcmp(buffer, kEncrypedMp4Magic, sizeof(kEncrypedMp4Magic)) || !memcmp(buffer, kEncryptedWvmMagic, sizeof(kEncryptedWvmMagic));
        }
        fclose(file);
    }
    return encrypted;
}

#pragma mark - WV control
- (void)setupOnlineStatusForAsset:(NSString *)asset
{
    NSString *hostName = [self.settings hostName];
    BOOL isReachable = [self isReachable:hostName];
    if (isReachable) {
        if (self.status == WViOsApiStatus_NotRegistered) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused"
            self.status = WV_RegisterAsset(asset);
#pragma clang diagnostic pop
        }
        if (self.status == WViOsApiStatus_OK) {
            self.status = WV_NowOnline();
        }
    } else {
       self.status = WV_NowOnline();
    }
}

- (void)nowOnline
{
    self.status = WV_NowOnline();
}

- (void)registerAsset:(NSString *)asset
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused"
    self.status = WV_RegisterAsset(asset);
#pragma clang diagnostic pop
}

- (void)unregisterAsset:(NSString *)asset
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused"
    self.status = WV_UnregisterAsset(asset);
#pragma clang diagnostic pop
}

- (void)stop
{
    self.status = WV_Stop();
}

- (void)initialize
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused"
    self.status = WV_Initialize(ESP_WVStatusCallback, [self.settings settingsDictionary]);
#pragma clang diagnotic pop
}

- (void)terminate
{
    self.status = WV_Terminate();
}

- (void)endPlayback
{
    [self stop];
    [self terminate];
}

#pragma mark - WV callbacks
- (void)handleCurrentBitrate:(NSDictionary *)attributes {}
- (void)handleBitrates:(NSDictionary *)attributes {}
- (void)handleChapterTitle:(NSDictionary *)attributes {}
- (void)handleChapterImage:(NSDictionary *)attributes {}
- (void)handleChapterSetup:(NSDictionary *)attributes {}

@end

#pragma mark -
#pragma mark Widevine Status Callback
WViOsApiStatus ESP_WVStatusCallback(WViOsApiEvent event, NSDictionary *attributes) {
    ESPLog(@"callback %d %@ %@\n", event, NSStringFromWViOsApiEvent(event), attributes);
    @autoreleasepool {
        SEL selector = 0;
        switch (event) {
            case WViOsApiEvent_SetCurrentBitrate:
                // selector = NSSelectorFromString(@"handleCurrentBitrate:");
                break;
            case WViOsApiEvent_Bitrates:
                // selector = NSSelectorFromString(@"handleBitrates:");
                break;
            case WViOsApiEvent_ChapterTitle:
                // selector = NSSelectorFromString(@"handleChapterTitle:");
                break;
            case WViOsApiEvent_ChapterImage:
                // selector = NSSelectorFromString(@"handleChapterImage:");
                break;
            case WViOsApiEvent_ChapterSetup:
                // selector = NSSelectorFromString(@"handleChapterSetup:");
                break;
            case WViOsApiEvent_StoppingOnError:
                return WViOsApiStatus_EntitlementDenied;
                break;
            case WViOsApiEvent_InitializeFailed:
                return WViOsApiStatus_NotInitialized;
                break;
            default:
                break;
        }
        if (selector) {
            // [widevineVideoPlayer performSelectorOnMainThread:selector withObject:attributes waitUntilDone:NO];
        }
    }
    return WViOsApiStatus_OK;
}
