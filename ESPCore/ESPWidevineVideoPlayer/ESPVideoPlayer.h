//
//  ESPVideoPlayer.h
//  DMA
//
//  Created by JP on 6/4/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@class ESPVideoPlayer;
@protocol ESPVideoPlayerDelegate
- (void)videoPlayer:(ESPVideoPlayer *)videoPlayer didChangeState:(NSNumber *)mpMoviePlayerStateAsNumber;
- (void)videoPlayer:(ESPVideoPlayer *)videoPlayer didEncounterError:(NSError *)error;
@end


@class ESPMedia;
@class ESPCatalog;
@class ESPConsumer;
@class ESPAuthorization;
@interface ESPVideoPlayer : NSObject

@property (weak, nonatomic) UIViewController<ESPVideoPlayerDelegate> *delegate;

- (MPMoviePlayerController *)moviePlayer;
- (UIView *)moviePlayerView;

- (void)playTrailer:(ESPMedia *)media catalog:(ESPCatalog *)catalog;
- (void)playFeature:(ESPMedia *)media consumer:(ESPConsumer *)consumer authorization:(ESPAuthorization *)authorization;

@end
