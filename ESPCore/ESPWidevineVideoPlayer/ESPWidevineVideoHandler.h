//
//  ESPWidevineVideoHandler.h
//  ESPCore
//
//  Created by JP on 5/16/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "WViPhoneAPI.h"
#import "ESPMedia.h"


#define WIDEVINE_PLAYBACK_ERROR 7500 // general catch-all error for video playback issues
#define WIDEVINE_PLAYBACK_CONCURRENCY_LIMIT_REACHED_ERROR 7501
#define WIDEVINE_INVALID_ENTITLEMENT_ERROR 7502
#define WIDEVINE_GEOLOCATION_BLOCKED_ERROR 7503

#if TARGET_IPHONE_SIMULATOR
    // Comment out unavailable methods. They are only available on a real device.
    // These defines allow the code to compile/link without error, but will give a runtime error
    #define WV_Initialize(a,b) 0;a;b;
    #define WV_Terminate(a) 0;a;
    #define WV_Play(a,b,c) 0;a;b;c;
    #define WV_GetDeviceId(a) (@"")
    #define WV_Stop(a) 0;a;
    #define WV_SetCredentials(a) 0;a;
    #define WV_SetUserData(a) 0;a;
    #define NSStringFromWViOsApiEvent(a) @""
    #define WV_QueryAssetsStatus() 0
    #define WV_NowOnline() 0
    #define WV_QueryAssetStatus(a) 0;a;
    #define WV_RegisterAsset(a) 0;a;
    #define WV_UnregisterAsset(a) 0;a;
    #define ESPRETURNIFSIMULATOR(x) {[self showSimulatorMessage];return;}
#else
    // Real device
    #define ESPRETURNIFSIMULATOR(x)
#endif

static NSString *ESPWidevineErrors[] = {
    @"OK",
	@"NotInitialized",
	@"AlreadyInitialized",
	@"CantConnectToMediaServer",
	@"BadMedia",
	@"CantConnectToDrmServer",
	@"NotEntitled",
	@"EntitlementDenied",
	@"LostConnection",
	@"EntitlementExpired",
	@"NotEntitledByRegion",
	@"BadUrl",
	@"FileNotPresent",
	@"NotRegistered",
	@"AlreadyRegistered",
	@"NotPlaying",
	@"AlreadyPlaying",
	@"FileSystemError",
	@"AssetDBWasCorrupted",
    @"JailBreakDetected"
};

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused"
static int ESPWVErrorCount = (sizeof(ESPWidevineErrors) / sizeof(NSString*));
#pragma clang diagnostic pop

@class ESPWidevineVideoHandler;
@class ESPAuthorization;
@class ESPConsumerPlaybackToken;
@class ESPWidevineSettings;

@interface ESPWidevineVideoHandler : NSObject

@property (assign, nonatomic, readonly) WViOsApiStatus status;
@property (strong, nonatomic) ESPWidevineSettings *settings;

- (void)play:(ESPMedia *)media authorization:(ESPAuthorization *)authorization success:(void (^)(NSURL *playbackURL))success failure:(void (^)(NSError *error))failure;
- (NSURL *)widevineCompatibleURLForFile:(NSString *)path;
- (void)endPlayback;

@end

// Widevine status callback - C function
WViOsApiStatus ESP_WVStatusCallback(WViOsApiEvent event, NSDictionary *attributes);