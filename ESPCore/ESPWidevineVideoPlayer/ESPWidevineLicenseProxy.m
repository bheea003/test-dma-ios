//
//  ESPWidevineLicenseProxy.m
//  DMA
//
//  Created by JP on 6/12/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPWidevineLicenseProxy.h"
#import "ESPConvenience.h"
#import "AFHTTPClient.h"

@interface ESPWidevineLicenseProxy()
@property (assign, nonatomic) BOOL concurrencyViolation;
@property (assign, nonatomic) BOOL geoLocationViolation;
@property (assign, nonatomic) BOOL smilError;
@property (strong, nonatomic) NSString *cdnStreamURL;
@property (strong, nonatomic) NSMutableArray *results;
@property (strong, nonatomic) NSMutableString *textInProcess;
@property (strong, nonatomic) NSError *error;
- (NSDictionary *)dictionaryFromData:(NSData *)data;
@end


NSString *const TextNodeKey = @"text";
@implementation ESPWidevineLicenseProxy

- (void)processURL:(NSString *)url success:(void (^)(NSString *movieURL))success failure:(void (^)(NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:url];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    [httpClient getPath:@"" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionary = [self dictionaryFromData:responseObject];
        BOOL didParse = [self parseSMIL:dictionary];
        if (didParse) {
            ESPLog(@"CDN URL is %@", self.cdnStreamURL);
            success(self.cdnStreamURL);
        } else {
            int code = UnknownError;
            if (self.concurrencyViolation) {
                code = ConcurrencyViolation;
            } else if (self.geoLocationViolation) {
                code = GeolocationViolation;
            } else if (self.smilError) {
                code = InvalidSMIL;
            }
            NSError *error = [NSError errorWithDomain:[baseURL host] code:code userInfo:nil];
            failure(error);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (BOOL)parseSMIL:(NSDictionary *)smilDictionary
{
    // ESPLog(@"smilDictionary: %@", smilDictionary);
    
    self.concurrencyViolation = NO;
    self.geoLocationViolation = NO;
    self.cdnStreamURL = nil;
    
    NSString *smilAbstractPath = @"smil.body.seq.ref.abstract";
    NSString *smilAbstractText = [smilDictionary valueForKeyPath:smilAbstractPath];
    if ((! IsEmpty(smilAbstractText)) && ([smilAbstractText caseInsensitiveCompare:@"Unable to acquire a concurrency lock on this content."] == NSOrderedSame)) {
        self.concurrencyViolation = YES;
        return NO;
    }
    else if ((! IsEmpty(smilAbstractText)) && ([smilAbstractText caseInsensitiveCompare:@"This content is not available in your location."] == NSOrderedSame))
    {
        self.geoLocationViolation = YES;
        return NO;
    }
    
    NSString *srcKeypath = @"smil.body.seq.par.video.src";
    NSString *src = [smilDictionary valueForKeyPath:srcKeypath];
    if (IsEmpty(src)) {
        // alternate keypath component
        srcKeypath = @"smil.body.seq.video.src";
        src = [smilDictionary valueForKeyPath:srcKeypath];
    }
    
    self.cdnStreamURL = src;
    if (IsEmpty(self.cdnStreamURL)) {
        self.smilError = YES;
        return NO;
    }
    
    /*
        NSString *keypath = @"smil.head.meta";
        id paramArray = [smilDictionary valueForKeyPath:keypath];
        if ([paramArray isKindOfClass:[NSArray class]]) {
            for (id param in paramArray) {
                if ([param isKindOfClass:[NSDictionary class]]) {
                    NSString *key = [param valueForKey:@"name"];
                    NSString *value = [param valueForKey:@"content"];
                    if (IsEmpty(key) || IsEmpty(value)) {
                        continue;
                    }
                    [self setValue:value forKey:key];
                    ESPLog(@"********* set %@:%@", key, value); // this will potentially set errorTitle, errorDescription, etc.
                }
            }
        }
    */
    return YES;
}

- (NSDictionary *)dictionaryFromData:(NSData *)data
{
    self.results = [[NSMutableArray alloc] init];
    [self.results addObject:[NSMutableDictionary dictionary]];
    
    self.textInProcess = [[NSMutableString alloc] init];
    
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    [parser setDelegate:self];
    BOOL success = [parser parse];
    if (success) {
        NSDictionary *resultDict = [self.results objectAtIndex:0];
        return resultDict;
    }
    return nil;
}

#pragma mark - NSXMLParserDelegate methods
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    NSMutableDictionary *parentDict = [self.results lastObject];
    NSMutableDictionary *childDict = [NSMutableDictionary dictionary];
    for (NSString *key in attributeDict) {
        [childDict setValue:[attributeDict objectForKey:key] forKey:key];
    }
    
    id existingValue = [parentDict objectForKey:elementName];
    if (existingValue) { // already exists; so it's an array
        NSMutableArray *array = nil;
        if ([existingValue isKindOfClass:[NSMutableArray class]]) {
            array = (NSMutableArray *) existingValue;
        } else {
            array = [NSMutableArray array];
            [array addObject:existingValue];
            
            [parentDict setObject:array forKey:elementName];
        }
        [array addObject:childDict];
    } else  {
        [parentDict setObject:childDict forKey:elementName];
    }
    [self.results addObject:childDict];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    NSMutableDictionary *dictInProgress = [self.results lastObject];
    [self.results removeLastObject];
    
    if ([self.textInProcess length] > 0) {
        if ([dictInProgress count] > 0) {
            [dictInProgress setObject:self.textInProcess forKey:TextNodeKey];
        } else {
            NSMutableDictionary *parentDict = [self.results lastObject];
            id parentObject = [parentDict objectForKey:elementName];
            
            if ([parentObject isKindOfClass:[NSArray class]]) {
                [parentObject removeLastObject];
                [parentObject addObject:self.textInProcess];
            } else {
                [parentDict removeObjectForKey:elementName];
                [parentDict setObject:self.textInProcess forKey:elementName];
            }
        }
        
        self.textInProcess = [[NSMutableString alloc] init];
    } else if ([dictInProgress count] == 0) {
        NSMutableDictionary *parentDict = [self.results lastObject];
        [parentDict removeObjectForKey:elementName];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	[self.textInProcess appendString:[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    self.error = parseError;
}

@end
