#import <Foundation/Foundation.h>

@interface ESPWidevineSettings : NSObject

@property (strong, nonatomic) NSString *clientID;
@property (strong, nonatomic) NSMutableDictionary *settingsDictionary;

- (NSString *)hostName;

- (void)setupUsingPID:(NSString *)pid token:(NSString *)token;
- (void)setupWidevineForClient:(NSString *)clientID serverURL:(NSURL *)serverURL portalKey:(NSString *)portalKey userDataKey:(NSString *)userDataKey clientIP:(NSString *)clientIP sessionID:(NSString *)sessionID heartbeatURL:(NSURL *)heartbeatURL heartbeatInterval:(NSInteger)heartbeatInterval deviceID:(NSString *)deviceID assetDatabasePath:(NSString *)assetDatabasePath preloadTimeout:(NSInteger)timeoutInMicroseconds;

@end
