//
//  ESPWidevineLicenseProxy.h
//  DMA
//
//  Created by JP on 6/12/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    InvalidSMIL,
    ConcurrencyViolation,
    GeolocationViolation,
    UnknownError
} SMILError;

@interface ESPWidevineLicenseProxy : NSObject <NSXMLParserDelegate>

- (void)processURL:(NSString *)url success:(void (^)(NSString *movieURL))success failure:(void (^)(NSError *error))failure;

@end
