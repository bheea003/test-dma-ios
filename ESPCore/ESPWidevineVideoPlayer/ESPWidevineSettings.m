#import "ESPWidevineSettings.h"
#import "ESPConvenience.h"
#import "WViPhoneAPI.h"

@implementation ESPWidevineSettings

#pragma mark - Setup Widevine Settings
- (void)setupUsingPID:(NSString *)pid token:(NSString *)token
{
    if (IsEmpty(pid)) {
        pid = @"";
    }
    [self setupWidevineForClient:nil serverURL:nil portalKey:nil userDataKey:pid clientIP:nil sessionID:nil heartbeatURL:0 heartbeatInterval:0 deviceID:token assetDatabasePath:nil preloadTimeout:0];
}

- (void)setupWidevineForClient:(NSString *)clientID serverURL:(NSURL *)serverURL portalKey:(NSString *)portalKey userDataKey:(NSString *)userDataKey clientIP:(NSString *)clientIP sessionID:(NSString *)sessionID heartbeatURL:(NSURL *)heartbeatURL heartbeatInterval:(NSInteger)heartbeatInterval deviceID:(NSString *)deviceID assetDatabasePath:(NSString *)assetDatabasePath preloadTimeout:(NSInteger)timeoutInMicroseconds
{
    NSMutableDictionary *wvDictionary = [NSMutableDictionary dictionary];
    [wvDictionary setValue:(IsEmpty(clientID) ? self.clientID : clientID) forKey:WVClientIdKey];
    [wvDictionary setValue:(IsEmpty(serverURL)) ? [self serverURL] : [serverURL absoluteString] forKey:WVDRMServerKey];
    [wvDictionary setValue:(IsEmpty(portalKey) ? @"disney" : portalKey) forKey:WVPortalKey];
    [wvDictionary setValue:(IsEmpty(userDataKey) ? [[NSProcessInfo processInfo] operatingSystemVersionString] : userDataKey) forKey:WVCAUserDataKey];
    [wvDictionary setValue:(IsEmpty(clientIP) ? @"127.0.0.1" : clientIP) forKey:WVClientIPKey];
    [wvDictionary setValue:(IsEmpty(sessionID) ? [[NSProcessInfo processInfo] globallyUniqueString] : sessionID) forKey:WVSessionIdKey];
    [wvDictionary setValue:((heartbeatInterval > 0) ? [NSString stringWithFormat:@"%i", heartbeatInterval] : @"30") forKey:WVHeartbeatPeriodKey];
    [wvDictionary setValue:@"1" forKey:WVPlayerDrivenAdaptationKey];
    [wvDictionary setValue:(IsEmpty(deviceID) ? [[NSProcessInfo processInfo] processName] : deviceID) forKey:WVDeviceIdKey];
    [wvDictionary setValue:((timeoutInMicroseconds > 0) ? [NSString stringWithFormat:@"%i", timeoutInMicroseconds] : @"30000000") forKey:WVPreloadTimeoutKey];
    [wvDictionary setValue:[NSNumber numberWithBool:NO] forKey:WVUseJSONKey];
    
    if (! IsEmpty(heartbeatURL)) {
        [wvDictionary setValue:[heartbeatURL absoluteString] forKey:WVHeartbeatUrlKey];
        
    }
    if (! IsEmpty(assetDatabasePath)) {
        [wvDictionary setValue:assetDatabasePath forKey:WVAssetDBPathKey];
    }
    
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970];
    [wvDictionary setValue:[NSNumber numberWithInt:time] forKey:@"WVTimeKey"];
    self.settingsDictionary = wvDictionary;
}

#pragma mark - Convenience
- (NSString *)clientID
{
    if (IsEmpty(_clientID)) {
        self.clientID = @"DSAAiOSPlayer";
    }
    return _clientID;
}

- (NSString *)hostName
{
    return [self.settingsDictionary valueForKey:WVDRMServerKey];
}

- (NSString *)serverURL
{
    NSDictionary *serverDictionary = @{
                                       @"Disney"    :   @"https://apollo.disney.go.com/proxy_disney.cgi",
                                       @"Widevine"  :   @"https://pmweb.widevine.net/cgi-bin/prod/proxy_disney.cgi",
                                       @"TPC"       :   @"http://disney.entitlement.theplatform.com/license/web/WidevineProxy/GetEMMs.cgi"
                                      };
    return [serverDictionary valueForKey:@"TPC"];
}

@end
