//
//  ESPVideoPlayer.m
//  DMA
//
//  Created by JP on 6/4/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPVideoPlayer.h"
#import "ESPConvenience.h"
#import "ESPMedia.h"
#import "ESPMediaContent.h"
#import "ESPCatalog.h"
#import "ESPConsumer.h"
#import "ESPAuthorization.h"

#import "ESPWidevineVideoHandler.h"

@interface ESPVideoPlayer ()
@property (strong, nonatomic) ESPWidevineVideoHandler *videoHandler;
@property (strong, nonatomic) MPMoviePlayerViewController *player;
@end

@implementation ESPVideoPlayer

- (MPMoviePlayerController *)moviePlayer
{
    return [self.player moviePlayer];
}

- (UIView *)moviePlayerView
{
    return [self.player view];
}

#pragma mark - Video Playback
- (void)playVideoForURL:(NSURL *)url
{
    ESPLog(@"about to play URL %@", [url absoluteString]);
    self.player = [[MPMoviePlayerViewController alloc] initWithContentURL:url];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playBackStateDidChangeHandler:) name:MPMoviePlayerLoadStateDidChangeNotification object:[self.player moviePlayer]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playBackDidFinishHandler:) name:MPMoviePlayerPlaybackDidFinishNotification object:[self.player moviePlayer]];
    [self.delegate presentMoviePlayerViewControllerAnimated:self.player];
}

- (void)playTrailer:(ESPMedia *)media catalog:(ESPCatalog *)catalog
{
    if ([media isTrailer]) {
        NSString *urlString = [[media trailer] fileUrl];
        [self playVideoForURL:[NSURL URLWithString:urlString]];
    } else {
        [catalog obtainTrailerForGUID:[[media trailerIDs] lastObject] success:^(ESPMedia *media) {
            if ([media trailer]) {
                NSString *urlString = [[media trailer] fileUrl];
                if (! IsEmpty(urlString)) {
                    [self playVideoForURL:[NSURL URLWithString:urlString]];
                } else {
                    NSError *error = [NSError errorWithDomain:@"com.disney.esp.videoplayer" code:0 userInfo:@{NSLocalizedString(@"Trailer is unavailable.", @"Trailer is unavailable.") : @"reason"}];
                    [self.delegate videoPlayer:self didEncounterError:error];
                }
            }
        } failure:^(NSError *error) {
            [self.delegate videoPlayer:self didEncounterError:error];
        }];
    }
}

- (void)playFeature:(ESPMedia *)media consumer:(ESPConsumer *)consumer authorization:(ESPAuthorization *)authorization
{
    [consumer beginPlaybackSession:authorization success:^(ESPConsumerPlaybackToken *token) {
        authorization.playbackToken = token;
        self.videoHandler = [[ESPWidevineVideoHandler alloc] init];
        [self.videoHandler play:media authorization:authorization success:^(NSURL *playbackURL) {
            [self playVideoForURL:playbackURL];
        } failure:^(NSError *error) {
            [self.delegate videoPlayer:self didEncounterError:error];
        }];
    } failure:^(NSError *error) {
        [self.delegate videoPlayer:self didEncounterError:error];
    }];
}

- (void)stop
{
    [self.videoHandler endPlayback];
    self.player = nil;
}

#pragma mark - Notification handling
- (void)setupObservers:(MPMoviePlayerViewController *)moviePlayer
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playFinished:) name:MPMoviePlayerPlaybackDidFinishNotification object:[moviePlayer moviePlayer]];
}

- (void)teardownObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)playFinished:(NSNotification *)notification
{
    [self teardownObservers];
}

#pragma mark - MPMoviePlayer notifications
- (void)loadStateHandler:(NSNotification *)notification
{
    MPMovieLoadState state = [(MPMoviePlayerController *)[notification object] loadState];
    // 0  MPMovieLoadStateUnknown        = 0,
    // 1  MPMovieLoadStatePlayable       = 1 << 0,
    // 2  MPMovieLoadStatePlaythroughOK  = 1 << 1,  // Playback will be automatically started in this state when shouldAutoplay is YES (we don't presently use "shouldAutoplay")
    // 4  MPMovieLoadStateStalled        = 1 << 2,  // Playback will be automatically paused in this state, if started
	// Auto-resume playback on stall
	switch (state) {
        case MPMovieLoadStateUnknown:
            // is called when done touched, so do not show error. maybe do show WV stop if not ok?
            ESPLog(@"LoadState Notification: %d    LOAD STATE UNKNOWN (idle or running -- unknown)", state);
            break;
		case MPMovieLoadStatePlayable :
            ESPLog(@"LoadState Notification: %d    PLAYABLE (NO BUFFER AVAILABLE YET)", MPMovieLoadStatePlayable);
			//[self.moviePlayerController prepareToPlay];
			//[self.moviePlayerController play];
            break;
		case MPMovieLoadStatePlaythroughOK :
            ESPLog(@"LoadState Notification: %d    PLAYABLE WITH BUFFER AVAILABLE, WILL ATTEMPT RESUME WITH NO DELAY", MPMovieLoadStatePlaythroughOK);
            //[self.moviePlayerController prepareToPlay];
            //[self.moviePlayerController play];
            break;
		case MPMovieLoadStatePlayable | MPMovieLoadStatePlaythroughOK :
            ESPLog(@"LoadState Notification: %d    PLAYABLE and PLAYABLE WITH BUFFER AVAILABLE, WILL ATTEMPT RESUME WITH NO DELAY", (MPMovieLoadStatePlayable | MPMovieLoadStatePlaythroughOK));
            //[self.moviePlayerController prepareToPlay];
            //[self.moviePlayerController play];
            break;
		case MPMovieLoadStateStalled :
            ESPLog(@"LoadState Notification: %d    STALLED", MPMovieLoadStateStalled);
			break;
		case MPMovieLoadStateStalled | MPMovieLoadStatePlayable :
            ESPLog(@"LoadState Notification: %d    STALLED BUT PLAYABLE WITH NO BUFFER AVAILABLE -- WILL DO SHORT .5 SECOND SLEEP BEFORE ATTEMPTING TO RESUME", (MPMovieLoadStateStalled | MPMovieLoadStatePlayable));
			// Stalled, can resume but may stall again.
            [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(delayedPlayback:) userInfo:nil repeats:NO];
			break;
		case MPMovieLoadStateStalled | MPMovieLoadStatePlaythroughOK:
            ESPLog(@"LoadState Notification: %d    STALLED BUT PLAYABLE WITH BUFFER AVAILABLE", (MPMovieLoadStateStalled | MPMovieLoadStatePlaythroughOK));
            //[NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(delayedPlayback:) userInfo:nil repeats:NO];
			break;
		case MPMovieLoadStateStalled | MPMovieLoadStatePlayable |MPMovieLoadStatePlaythroughOK :
            ESPLog(@"LoadState Notification: %d    STALLED BUT PLAYABLE and PLAYABLE WITH BUFFER AVAILABLE", (MPMovieLoadStateStalled | MPMovieLoadStatePlaythroughOK));
            //[self.moviePlayerController prepareToPlay];
            //[self.moviePlayerController play];
			break;
		default:
			// Either already playing or cannot play
            ESPLog(@"loadState is undefined with %d", state);
			break;
	}
}

- (void)playBackStateDidChangeHandler:(NSNotification*)notification
{
    MPMoviePlaybackState state = [[notification object] playbackState];
    switch (state) {
        case MPMoviePlaybackStateStopped:
            ESPLog(@"PlaybackStateDidChange Notification - Playback state has stopped (%d)", state);
            break;
        case MPMoviePlaybackStatePlaying:
            ESPLog(@"PlaybackStateDidChange Notification - Playback state has started (%d)", state);
            break;
        case MPMoviePlaybackStatePaused:
            ESPLog(@"PlaybackStateDidChange Notification - Playback state has paused (%d)", state);
            break;
        case MPMoviePlaybackStateInterrupted:
            ESPLog(@"PlaybackStateDidChange Notification - Playback state has been interrupted (%d)", state);
            break;
        case MPMoviePlaybackStateSeekingForward:
        case MPMoviePlaybackStateSeekingBackward:
            // If the user seeks around in the file then we want to zap our saved playback time
            // value, otherwise we'll keep reseting the play time to what we want.
            ESPLog(@"Playback state is Seeking %@ (%d)", state == MPMoviePlaybackStateSeekingForward ? @"Forward" : @"Backward", state);
            break;
        default:
            break;
    }
    if ([self.delegate respondsToSelector:@selector(videoPlayer:didChangeState:)]) {
        [self.delegate videoPlayer:self didChangeState:[NSNumber numberWithInt:state]];
    }
}

- (void)playBackDidFinishHandler:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    NSDictionary *userInfo = [notification userInfo];
    ESPLog(@"playback finished with\n%@", userInfo);
    
    int reasonKey = [[userInfo objectForKey:@"MPMoviePlayerPlaybackDidFinishReasonUserInfoKey"]intValue];
    switch (reasonKey) {
        case MPMovieFinishReasonPlaybackEnded:
            ESPLog(@"\n\n\nMPMoviePlayerPlaybackDidFinishReasonUserInfoKey indicates that playback ended normally.");
            break;
        case MPMovieFinishReasonPlaybackError:
            ESPLog(@"\n\n\nMPMoviePlayerPlaybackDidFinishReasonUserInfoKey indicates that playback ended due to an error.");
            break;
        case MPMovieFinishReasonUserExited:
            ESPLog(@"\n\n\nMPMoviePlayerPlaybackDidFinishReasonUserInfoKey indicates that playback was ended by user.");
            break;
        default:
            break;
    }
    
    NSError *error = [userInfo valueForKey:@"error"];
    if (error)
    {
        NSInteger code = [error code];
        if (code == -11828) {
            error = [NSError errorWithDomain:error.domain code:WIDEVINE_PLAYBACK_ERROR userInfo:error.userInfo];
        }
    }
    
    if (error) {
        [self.delegate videoPlayer:self didEncounterError:error];
    }
    
    [self stop];
}

@end
