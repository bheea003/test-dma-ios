//
//  ESPFAQItem.h
//  
//
//  Created by Josh Paul on 6/17/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESPFAQItem : NSObject <NSCoding> {
    NSNumber *faqItemID;
    NSNumber *lastModifiedTimeStamp;
    NSString *section;
    NSString *text;
    NSString *textColor;
    NSString *title;
    NSString *titleColor;
}

@property (nonatomic, copy) NSNumber *faqItemID;
@property (nonatomic, copy) NSNumber *lastModifiedTimeStamp;
@property (nonatomic, copy) NSString *section;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *textColor;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *titleColor;

+ (ESPFAQItem *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
