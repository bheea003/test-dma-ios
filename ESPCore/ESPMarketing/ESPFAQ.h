//
//  ESPFAQ.h
//  
//
//  Created by Josh Paul on 6/17/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESPFAQ : NSObject <NSCoding> {
    NSNumber *faqID;
    NSArray *items;
    NSNumber *lastModifiedTimeStamp;
    NSString *section;
}

@property (nonatomic, copy) NSNumber *faqID;
@property (nonatomic, copy) NSArray *items;
@property (nonatomic, copy) NSNumber *lastModifiedTimeStamp;
@property (nonatomic, copy) NSString *section;

+ (ESPFAQ *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
