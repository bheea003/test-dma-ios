//
//  ESPMarketing.m
//  DMA
//
//  Created by JP on 6/17/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPMarketing.h"
#import "ESPConvenience.h"
#import "ESPFAQ.h"

@implementation ESPMarketing

+ (ESPMarketing *)marketingForApplication:(NSString *)applicationName language:(NSString *)iso639dash1 country:(NSString *)iso3166dash1alpha2
{
    static ESPMarketing *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURL *url = [NSURL URLWithString:ESPAPIBaseURL];
        __sharedInstance = [[ESPMarketing alloc] initWithBaseURL:url];
        [__sharedInstance setDefaultHeader:@"Client" value:applicationName];
        [__sharedInstance setDefaultHeader:@"Language" value:iso639dash1];
        [__sharedInstance setDefaultHeader:@"Country" value:iso3166dash1alpha2];
        
        [__sharedInstance registerHTTPOperationClass:[AFJSONRequestOperation class]];
    });
    
    if (! [__sharedInstance hasCorrectHeaders]) {
        ESPLog("ESPMarketing ALERT: Please check your client settings:\n\tapplicationName is:%@\n\tlanguage (should be ISO 639-1) is:%@\n\tcountry (should be ISO 3166-1 alpha-2) is:%@", [__sharedInstance defaultValueForHeader:@"Client"], [__sharedInstance defaultValueForHeader:@"Language"], [__sharedInstance defaultValueForHeader:@"Country"]);
    }
    return __sharedInstance;
}

#pragma mark - Convenience
- (NSString *)domain
{
    return @"com.disney.esp.marketing";
}

#pragma mark - Available Feeds
- (void)obtainAvailableContent:(void (^)(NSArray *result))success failure:(void (^)(NSError *error))failure
{
    if (! [self hasCorrectHeaders]) {
        failure([self errorForType:InvalidHeadersError userInfo:nil]);
        return;
    }
    
    NSString *restPath = [NSString stringWithFormat:@"%@/content/", [self restDomain]];
    [self getPath:restPath parameters:nil success:^(AFHTTPRequestOperation *operation, id response) {
        if (IsEmpty(response)) {
            failure([self errorForType:APIServiceError userInfo:nil]);
            return;
        }
        
        NSError *error = nil;
        id json = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        if (! error) {
            NSArray *result = [NSArray arrayWithArray:json];
            success(result);
        } else {
            failure(error);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([self errorForType:APIServiceError userInfo:error.userInfo]);
    }];
}

#pragma mark - FAQ
- (void)obtainFAQs:(void (^)(NSArray *faqs))success failure:(void (^)(NSError *error))failure
{
    if (! [self hasCorrectHeaders]) {
        failure([self errorForType:InvalidHeadersError userInfo:nil]);
        return;
    }
    
    NSString *restPath = [NSString stringWithFormat:@"%@/content/faqs", [self restDomain]];
    [self getPath:restPath parameters:nil success:^(AFHTTPRequestOperation *operation, id response) {
        if (IsEmpty(response)) {
            failure([self errorForType:APIServiceError userInfo:nil]);
            return;
        }
        
        NSError *error = nil;
        id json = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        if (! error) {
            NSMutableArray *faqs = [NSMutableArray array];
            if ([json isKindOfClass:[NSDictionary class]]) {
                [faqs addObject:[ESPFAQ instanceFromDictionary:json]];
            } else if ([json isKindOfClass:[NSArray class]]) {
                for (NSDictionary *dictionary in json) {
                    [faqs addObject:[ESPFAQ instanceFromDictionary:dictionary]];
                }
            }
            success(faqs);
        } else {
            failure(error);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([self errorForType:APIServiceError userInfo:error.userInfo]);
    }];
}
@end
