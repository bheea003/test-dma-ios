//
//  ESPFAQSection.m
//  
//
//  Created by Josh Paul on 6/17/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import "ESPFAQSection.h"

#import "ESPFAQItem.h"

@implementation ESPFAQSection

@synthesize faqSectionID;
@synthesize items;
@synthesize lastModifiedTimeStamp;
@synthesize section;
@synthesize title;

+ (ESPFAQSection *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    ESPFAQSection *instance = [[ESPFAQSection alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    [self setValuesForKeysWithDictionary:aDictionary];
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"items"]) {
        if ([value isKindOfClass:[NSArray class]]) {
            NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
            for (id valueMember in value) {
                ESPFAQItem *populatedMember = [ESPFAQItem instanceFromDictionary:valueMember];
                [myMembers addObject:populatedMember];
            }
            self.items = myMembers;
        }
    } else {
        [super setValue:value forKey:key];
    }
}


- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        [self setValue:value forKey:@"faqSectionID"];
    } else {
        NSLog(@"ESPFAQSection undefined key from API! %@ [%@]", key, value);
        // [super setValue:value forUndefinedKey:key];
    }
}


- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    if (self.faqSectionID) {
        [dictionary setObject:self.faqSectionID forKey:@"faqSectionID"];
    }
    if (self.items) {
        [dictionary setObject:self.items forKey:@"items"];
    }
    if (self.lastModifiedTimeStamp) {
        [dictionary setObject:self.lastModifiedTimeStamp forKey:@"lastModifiedTimeStamp"];
    }
    if (self.section) {
        [dictionary setObject:self.section forKey:@"section"];
    }
    if (self.title) {
        [dictionary setObject:self.title forKey:@"title"];
    }
    return dictionary;
}

//===========================================================
//  Keyed Archiving
//===========================================================
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.faqSectionID forKey:@"faqSectionID"];
    [encoder encodeObject:self.items forKey:@"items"];
    [encoder encodeObject:self.lastModifiedTimeStamp forKey:@"lastModifiedTimeStamp"];
    [encoder encodeObject:self.section forKey:@"section"];
    [encoder encodeObject:self.title forKey:@"title"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if ((self = [super init])) {
        self.faqSectionID = [decoder decodeObjectForKey:@"faqSectionID"];
        self.items = [decoder decodeObjectForKey:@"items"];
        self.lastModifiedTimeStamp = [decoder decodeObjectForKey:@"lastModifiedTimeStamp"];
        self.section = [decoder decodeObjectForKey:@"section"];
        self.title = [decoder decodeObjectForKey:@"title"];
    }
    return self;
}

@end
