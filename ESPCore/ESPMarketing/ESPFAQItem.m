//
//  ESPFAQItem.m
//  
//
//  Created by Josh Paul on 6/17/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import "ESPFAQItem.h"

@implementation ESPFAQItem

@synthesize faqItemID;
@synthesize lastModifiedTimeStamp;
@synthesize section;
@synthesize text;
@synthesize textColor;
@synthesize title;
@synthesize titleColor;

+ (ESPFAQItem *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    ESPFAQItem *instance = [[ESPFAQItem alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    [self setValuesForKeysWithDictionary:aDictionary];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        [self setValue:value forKey:@"faqItemID"];
    } else {
        NSLog(@"ESPFAQItem undefined key from API! %@ [%@]", key, value);
        // [super setValue:value forUndefinedKey:key];
    }
}


- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    if (self.faqItemID) {
        [dictionary setObject:self.faqItemID forKey:@"faqItemID"];
    }
    if (self.lastModifiedTimeStamp) {
        [dictionary setObject:self.lastModifiedTimeStamp forKey:@"lastModifiedTimeStamp"];
    }
    if (self.section) {
        [dictionary setObject:self.section forKey:@"section"];
    }
    if (self.text) {
        [dictionary setObject:self.text forKey:@"text"];
    }
    if (self.textColor) {
        [dictionary setObject:self.textColor forKey:@"textColor"];
    }
    if (self.title) {
        [dictionary setObject:self.title forKey:@"title"];
    }
    if (self.titleColor) {
        [dictionary setObject:self.titleColor forKey:@"titleColor"];
    }
    return dictionary;
}

//===========================================================
//  Keyed Archiving
//===========================================================
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.faqItemID forKey:@"faqItemID"];
    [encoder encodeObject:self.lastModifiedTimeStamp forKey:@"lastModifiedTimeStamp"];
    [encoder encodeObject:self.section forKey:@"section"];
    [encoder encodeObject:self.text forKey:@"text"];
    [encoder encodeObject:self.textColor forKey:@"textColor"];
    [encoder encodeObject:self.title forKey:@"title"];
    [encoder encodeObject:self.titleColor forKey:@"titleColor"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.faqItemID = [decoder decodeObjectForKey:@"faqItemID"];
        self.lastModifiedTimeStamp = [decoder decodeObjectForKey:@"lastModifiedTimeStamp"];
        self.section = [decoder decodeObjectForKey:@"section"];
        self.text = [decoder decodeObjectForKey:@"text"];
        self.textColor = [decoder decodeObjectForKey:@"textColor"];
        self.title = [decoder decodeObjectForKey:@"title"];
        self.titleColor = [decoder decodeObjectForKey:@"titleColor"];
    }
    return self;
}

@end
