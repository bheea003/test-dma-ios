//
//  ESPFAQ.m
//
//
//  Created by Josh Paul on 6/17/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import "ESPFAQ.h"

#import "ESPFAQSection.h"

@implementation ESPFAQ

@synthesize faqID;
@synthesize items;
@synthesize lastModifiedTimeStamp;
@synthesize section;

+ (ESPFAQ *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    ESPFAQ *instance = [[ESPFAQ alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    [self setValuesForKeysWithDictionary:aDictionary];
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"items"]) {
        if ([value isKindOfClass:[NSArray class]])
        {
            NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
            for (id valueMember in value) {
                ESPFAQSection *populatedMember = [ESPFAQSection instanceFromDictionary:valueMember];
                [myMembers addObject:populatedMember];
            }
            self.items = myMembers;
        }
    } else {
        [super setValue:value forKey:key];
    }
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        [self setValue:value forKey:@"faqID"];
    } else {
        NSLog(@"ESPFAQ undefined key from API! %@ [%@]", key, value);
        // [super setValue:value forUndefinedKey:key];
    }
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    if (self.faqID) {
        [dictionary setObject:self.faqID forKey:@"faqID"];
    }
    if (self.items) {
        [dictionary setObject:self.items forKey:@"items"];
    }
    if (self.lastModifiedTimeStamp) {
        [dictionary setObject:self.lastModifiedTimeStamp forKey:@"lastModifiedTimeStamp"];
    }
    if (self.section) {
        [dictionary setObject:self.section forKey:@"section"];
    }
    return dictionary;
}

//===========================================================
//  Keyed Archiving
//===========================================================
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.faqID forKey:@"faqID"];
    [encoder encodeObject:self.items forKey:@"items"];
    [encoder encodeObject:self.lastModifiedTimeStamp forKey:@"lastModifiedTimeStamp"];
    [encoder encodeObject:self.section forKey:@"section"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.faqID = [decoder decodeObjectForKey:@"faqID"];
        self.items = [decoder decodeObjectForKey:@"items"];
        self.lastModifiedTimeStamp = [decoder decodeObjectForKey:@"lastModifiedTimeStamp"];
        self.section = [decoder decodeObjectForKey:@"section"];
    }
    return self;
}

@end
