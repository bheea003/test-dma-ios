//
//  ESPMarketing.h
//  DMA
//
//  Created by JP on 6/17/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPAPIClient.h"

@interface ESPMarketing : ESPAPIClient

+ (ESPMarketing *)marketingForApplication:(NSString *)applicationName language:(NSString *)iso639dash1 country:(NSString *)iso3166dash1alpha2;

- (void)obtainAvailableContent:(void (^)(NSArray *result))success failure:(void (^)(NSError *error))failure;

- (void)obtainFAQs:(void (^)(NSArray *faqs))success failure:(void (^)(NSError *error))failure;

@end
