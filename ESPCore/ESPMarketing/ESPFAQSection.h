//
//  ESPFAQSection.h
//  
//
//  Created by Josh Paul on 6/17/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESPFAQSection : NSObject <NSCoding> {
    NSNumber *faqSectionID;
    NSArray *items;
    NSNumber *lastModifiedTimeStamp;
    NSString *section;
    NSString *title;
}

@property (nonatomic, copy) NSNumber *faqSectionID;
@property (nonatomic, copy) NSArray *items;
@property (nonatomic, copy) NSNumber *lastModifiedTimeStamp;
@property (nonatomic, copy) NSString *section;
@property (nonatomic, copy) NSString *title;

+ (ESPFAQSection *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
