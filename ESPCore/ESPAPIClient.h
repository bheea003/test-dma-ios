//
//  ESPAPIClient.h
//  ESPFoundation
//
//  Created by JP on 4/15/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"

typedef enum {
    InvalidHeadersError,
    AccessDenied,
    AccesssValidationError,
    ApplicationValidationError,
    APIServiceError
} ESPAPIBridgeError;

FOUNDATION_EXPORT NSString *const ESPAPIBaseURL;

@interface ESPAPIClient : AFHTTPClient

+ (ESPAPIClient *)clientForApplication:(NSString *)applicationName language:(NSString *)iso639dash1 country:(NSString *)iso3166dash1alpha2;


- (void)obtainStatus:(void (^)(NSDictionary *result))success failure:(void (^)(NSError *error))failure;

- (NSString *)domain;
- (NSString *)restDomain;

- (BOOL)hasCorrectHeaders;
- (NSError *)errorForType:(ESPAPIBridgeError)errorType userInfo:(NSDictionary *)userInfo;

- (NSString *)applicationName;
- (NSString *)language;
- (NSString *)country;

- (void)cancelAllHTTPOperations;
- (void)cancelAllHTTPOperationsWithMethod:(NSString *)method basePath:(NSString *)path;

@end
