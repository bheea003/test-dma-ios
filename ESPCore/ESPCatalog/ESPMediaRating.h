//
//  ESPMediaRating.h
//  
//
//  Created by Josh Paul on 4/24/13.
//  Copyright (c) 2013. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESPMediaRating : NSObject <NSCoding>
{
    NSString *rating;
    NSString *scheme;
    NSArray *subRatings;
}

@property (nonatomic, copy) NSString *rating;
@property (nonatomic, copy) NSString *scheme;
@property (nonatomic, copy) NSArray *subRatings;

+ (ESPMediaRating *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
