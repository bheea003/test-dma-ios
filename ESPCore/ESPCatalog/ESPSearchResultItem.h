//
//  ESPSearchResultItem.h
//  
//
//  Created by Josh Paul on 7/17/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESPSearchResultItem : NSObject <NSCoding> {
    NSString *sortTitle;
    NSString *guid;
    NSString *rating;
    NSString *title;
}

@property (nonatomic, copy) NSString *sortTitle;
@property (nonatomic, copy) NSString *guid;
@property (nonatomic, copy) NSString *rating;
@property (nonatomic, copy) NSString *title;

+ (ESPSearchResultItem *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
