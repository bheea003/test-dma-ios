//
//  ESPCatalog.m
//  ESPCatalog
//
//  Created by JP on 4/24/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPCatalog.h"
#import "ESPConvenience.h"
#import "ESPCatalogCategory.h"
#import "ESPCatalogType.h"
#import "ESPSearchResult.h"
#import "ESPMedia.h"
#import "ESPMediaCategory.h"

#define MAX_RETRIES 3

NSString *const FieldFilterAdded = @"added";
NSString *const FieldFilterBonusIDs = @"DSAA$BonusIds";
NSString *const FieldFilterContentType = @"DSAA$contentType";
NSString *const FieldFilterDescriptionText = @"description";
NSString *const FieldFilterGUID = @"guid";
NSString *const FieldFilterIsFree = @"DSAA$free";
NSString *const FieldFilterLongSynopsis = @"DSAA$long_synopsis";
NSString *const FieldFilterMediaCategories = @"media$categories";
NSString *const FieldFilterMediaContent = @"media$content";
NSString *const FieldFilterMediaCredits = @"media$credits";
NSString *const FieldFilterMediaDescriptionLocalized = @"plmedia$descriptionLocalized";
NSString *const FieldFilterMediaFilterwords = @"media$Filterwords";
NSString *const FieldFilterMediaFilterwordsLocalized = @"plmedia$FilterwordsLocalized";
NSString *const FieldFilterMediaRatings = @"media$ratings";
NSString *const FieldFilterMediaThumbnails = @"media$thumbnails";
NSString *const FieldFilterMediaTitleLocalized = @"plmedia$titleLocalized";
NSString *const FieldFilterMediumSynopsis = @"DSAA$medium_synopsis";
NSString *const FieldFilterMovieID = @"id";
NSString *const FieldFilterMPMI = @"DSAA$mpm_i";
NSString *const FieldFilterMPMP = @"DSAA$mpm_p";
NSString *const FieldFilterRestrictionsEndDate = @"DSAA$restrictionsEndDate";
NSString *const FieldFilterRestrictionsStartDate = @"DSAA$restrictionsStartDate";
NSString *const FieldFilterRuntimeMinutes = @"DSAA$runtime_minutes";
NSString *const FieldFilterSEOFilterwords = @"seo_Filterwords";
NSString *const FieldFilterSEOTitle = @"DSAA$seo_title";
NSString *const FieldFilterSortTitle = @"DSAA$sort_title";
NSString *const FieldFilterTagTitle = @"DSAA$tag_title";
NSString *const FieldFilterTheatricalReleaseDate = @"DSAA$theatrical_release_date";
NSString *const FieldFilterTitle = @"title";
NSString *const FieldFilterTitleRestrictions = @"DSAA$titleRestrictions";
NSString *const FieldFilterTrailerIDs = @"DSAA$TrailerIds";
NSString *const FieldFilterUMID = @"DSAA$umid";
NSString *const FieldFilterUMIDHD = @"DSAA$umid_hd";
NSString *const FieldFilterUMIDHDPlus = @"DSAA$umid_hdplus";
NSString *const FieldFilterUMIDSD = @"DSAA$umid_sd";
NSString *const FieldFilterUpdated = @"updated";

NSString *const MediaFilterWidevine = @"Widevine";
NSString *const MediaFilterClosedCaption = @"TT";
NSString *const MediaFilterFlash = @"Flash Access";
NSString *const MediaFilterPlayReady = @"PlayReady";

@implementation ESPCatalog

+ (ESPCatalog *)catalogForApplication:(NSString *)applicationName language:(NSString *)iso639dash1 country:(NSString *)iso3166dash1alpha2
{
    static ESPCatalog *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURL *url = [NSURL URLWithString:ESPAPIBaseURL];
        __sharedInstance = [[ESPCatalog alloc] initWithBaseURL:url];
        [__sharedInstance setDefaultHeader:@"Client" value:applicationName];
        [__sharedInstance setDefaultHeader:@"Language" value:iso639dash1];
        [__sharedInstance setDefaultHeader:@"Country" value:iso3166dash1alpha2];
        
        [__sharedInstance registerHTTPOperationClass:[AFJSONRequestOperation class]];
    });
    
    
    if (! [__sharedInstance hasCorrectHeaders]) {
        NSLog(@"ESPCatalog ALERT: Please check your client settings:\n\tapplicationName is:%@\n\tlanguage (should be ISO 639-1) is:%@\n\tcountry (should be ISO 3166-1 alpha-2) is:%@", __sharedInstance.applicationName, __sharedInstance.language, __sharedInstance.country);
    }
    return __sharedInstance;
}

+ (NSArray *)guidOnlyFilter;
{
    return @[FieldFilterGUID];
}

+ (NSArray *)guidTitleAndThumbnailsOnlyFilter
{
    return @[FieldFilterGUID, FieldFilterTitle, FieldFilterMediaThumbnails];
}

#pragma mark - Convenience
- (NSString *)domain
{
    return @"com.disney.esp.catalog";
}

#pragma mark - Category acquisition
- (void)obtainAllCategories:(void (^)(NSArray *categories))success failure:(void (^)(NSError *error))failure
{
    if (! [self hasCorrectHeaders]) {
        failure([self errorForType:InvalidHeadersError userInfo:nil]);
        return;
    }
    
    NSString *restPath = [NSString stringWithFormat:@"/%@/category", [self restDomain]];
    [self getPath:restPath parameters:nil success:^(AFHTTPRequestOperation *operation, id response) {
        if (IsEmpty(response)) {
            failure([self errorForType:APIServiceError userInfo:nil]);
            return;
        }
        
        NSError *error = nil;
        id result = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        if (! error) {
            NSMutableArray *categoryArray = [NSMutableArray array];
            if ([result isKindOfClass:[NSArray class]]) {
                for (NSDictionary *categoryDictionary in result) {
                    ESPCatalogCategory *category = [ESPCatalogCategory instanceFromDictionary:categoryDictionary];
                    [categoryArray addObject:category];
                }
                success(categoryArray);
            } else if ([result isKindOfClass:[NSDictionary class]]) {
                ESPCatalogCategory *category = [ESPCatalogCategory instanceFromDictionary:result];
                success(@[category]);
            }
        } else {
            failure([self errorForType:AccesssValidationError userInfo:error.userInfo]);
        }        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([self errorForType:APIServiceError userInfo:error.userInfo]);
    }];
}

- (void)obtainAllCategoriesFilteringOut:(NSArray *)categoriesToRemove success:(void (^)(NSArray *filteredCategories))success failure:(void (^)(NSError *error))failure
{
    [self obtainAllCategories:^(NSArray *categories) {
        NSMutableArray *filteredArray = [NSMutableArray array];
        // filter out non-categories
        for (ESPCatalogCategory *category in categories) {
            if (([categoriesToRemove containsObject:category]) || ([[category urlTitle] hasPrefix:@"_"])) {
                continue;
            }
            [filteredArray addObject:category];
        }
        [filteredArray sortUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = [(ESPCatalogCategory *)a urlTitle];
            NSString *second = [(ESPCatalogCategory *)b urlTitle];
            return [first localizedCaseInsensitiveCompare:second];
        }];
        
        // move All feed to top
        ESPCatalogCategory *allCategory = [[ESPCatalogCategory alloc] init];
        allCategory.userTitle = @"All";
        allCategory.urlTitle = @"All";
        int indexOfAll = [filteredArray indexOfObject:allCategory];
        if (indexOfAll != NSNotFound) {
            NSString *allCategory = [filteredArray objectAtIndex:indexOfAll];
            [filteredArray removeObjectAtIndex:indexOfAll];
            [filteredArray insertObject:allCategory atIndex:0];
        }
        success(filteredArray);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)obtainCountForCategory:(ESPCatalogCategory *)category updatedSince:(NSNumber *)msFromEpoch success:(void (^)(NSNumber *categoryCount))success failure:(void (^)(NSError *error))failure
{
    [self obtainCategory:category filterFields:nil lowerRange:[NSNumber numberWithInt:0] upperRange:[NSNumber numberWithInt:1] includeCount:YES ratings:nil mediaFilter:nil updatedSince:msFromEpoch success:^(NSArray *categories, NSNumber *categoryCount) {
        success(categoryCount);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)obtainCategoryForTitle:(NSString *)categoryTitle success:(void (^)(NSArray *categoryResults))success failure:(void (^)(NSError *error))failure
{
    ESPCatalogCategory *category = [[ESPCatalogCategory alloc] init];
    category.urlTitle = categoryTitle;
    category.userTitle = categoryTitle;
    [self obtainCategory:category updatedSince:nil success:^(NSArray *categoryResults, NSNumber *categoryCount) {
        success(categoryResults);
    } failure:^(NSError *error) {
        failure(error);
    }];
}


- (void)obtainCategory:(ESPCatalogCategory *)category updatedSince:(NSNumber *)msFromEpoch success:(void (^)(NSArray *categories, NSNumber *categoryCount))success failure:(void (^)(NSError *error))failure
{
    [self obtainCategory:category filterFields:nil lowerRange:[NSNumber numberWithInt:-1] upperRange:[NSNumber numberWithInt:-1] includeCount:NO ratings:nil mediaFilter:nil updatedSince:msFromEpoch success:^(NSArray *categories, NSNumber *categoryCount) {
        success(categories, categoryCount);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)obtainCategory:(ESPCatalogCategory *)category updatedSince:(NSNumber *)msFromEpoch includeCount:(BOOL)flag success:(void (^)(NSArray *categories, NSNumber *categoryCount))success failure:(void (^)(NSError *error))failure
{
    [self obtainCategory:category filterFields:nil lowerRange:[NSNumber numberWithInt:-1] upperRange:[NSNumber numberWithInt:-1] includeCount:YES ratings:nil mediaFilter:nil updatedSince:msFromEpoch success:^(NSArray *categories, NSNumber *categoryCount) {
        success(categories, categoryCount);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)obtainCategory:(ESPCatalogCategory *)category updatedSince:(NSNumber *)msFromEpoch includeCount:(BOOL)flag filterFields:(NSArray *)fields success:(void (^)(NSArray *categories, NSNumber *categoryCount))success failure:(void (^)(NSError *error))failure
{
    [self obtainCategory:category filterFields:fields lowerRange:[NSNumber numberWithInt:-1] upperRange:[NSNumber numberWithInt:-1] includeCount:flag ratings:nil mediaFilter:nil updatedSince:msFromEpoch success:^(NSArray *categories, NSNumber *categoryCount) {
        success(categories, categoryCount);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)obtainCategory:(ESPCatalogCategory *)category updatedSince:(NSNumber *)msFromEpoch includeCount:(BOOL)flag ratings:(NSArray *)ratings success:(void (^)(NSArray *categories, NSNumber *categoryCount))success failure:(void (^)(NSError *error))failure
{
    [self obtainCategory:category filterFields:nil lowerRange:[NSNumber numberWithInt:-1] upperRange:[NSNumber numberWithInt:-1] includeCount:flag ratings:ratings mediaFilter:nil updatedSince:msFromEpoch success:^(NSArray *categories, NSNumber *categoryCount) {
        success(categories, categoryCount);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)obtainCategory:(ESPCatalogCategory *)category updatedSince:(NSNumber *)msFromEpoch  filterFields:(NSArray *)fields success:(void (^)(NSArray *categories, NSNumber *categoryCount))success failure:(void (^)(NSError *error))failure
{
    [self obtainCategory:category filterFields:fields lowerRange:[NSNumber numberWithInt:-1] upperRange:[NSNumber numberWithInt:-1] includeCount:NO ratings:nil mediaFilter:nil updatedSince:msFromEpoch success:^(NSArray *categories, NSNumber *categoryCount) {
        success(categories, categoryCount);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)obtainCategory:(ESPCatalogCategory *)category updatedSince:(NSNumber *)msFromEpoch filterFields:(NSArray *)fields ratings:(NSArray *)ratings success:(void (^)(NSArray *categories, NSNumber *categoryCount))success failure:(void (^)(NSError *error))failure
{
    [self obtainCategory:category filterFields:fields lowerRange:[NSNumber numberWithInt:-1] upperRange:[NSNumber numberWithInt:-1] includeCount:NO ratings:ratings mediaFilter:nil updatedSince:msFromEpoch success:^(NSArray *categories, NSNumber *categoryCount) {
        success(categories, categoryCount);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)obtainCategory:(ESPCatalogCategory *)category updatedSince:(NSNumber *)msFromEpoch filterFields:(NSArray *)fields lowerRange:(NSNumber *)lowerRange upperRange:(NSNumber *)upperRange ratings:(NSArray *)ratings success:(void (^)(NSArray *categories, NSNumber *categoryCount))success failure:(void (^)(NSError *error))failure
{
    [self obtainCategory:category filterFields:fields lowerRange:lowerRange upperRange:upperRange includeCount:NO ratings:ratings mediaFilter:nil updatedSince:msFromEpoch success:^(NSArray *categories, NSNumber *categoryCount) {
        success(categories, categoryCount);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)obtainCategory:(ESPCatalogCategory *)category updatedSince:(NSNumber *)msFromEpoch filterFields:(NSArray *)fields lowerRange:(NSNumber *)lowerRange upperRange:(NSNumber *)upperRange ratings:(NSArray *)ratings mediaFilter:(NSArray *)filter success:(void (^)(NSArray *media, NSNumber *categoryCount))success failure:(void (^)(NSError *error))failure
{
    [self obtainCategory:category filterFields:fields lowerRange:lowerRange upperRange:upperRange includeCount:NO ratings:ratings mediaFilter:filter updatedSince:msFromEpoch success:^(NSArray *media, NSNumber *categoryCount) {
        success(media, categoryCount);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)obtainCategory:(ESPCatalogCategory *)category filterFields:(NSArray *)fields lowerRange:(NSNumber *)lowerRange upperRange:(NSNumber *)upperRange includeCount:(BOOL)flag ratings:(NSArray *)ratings mediaFilter:(NSArray *)filter updatedSince:(NSNumber *)msFromEpoch success:(void (^)(NSArray *media, NSNumber *categoryCount))success failure:(void (^)(NSError *error))failure
{
    if (! [self hasCorrectHeaders]) {
        failure([self errorForType:InvalidHeadersError userInfo:nil]);
        return;
    }
    
    NSString *restPath = [NSString stringWithFormat:@"/%@/category/%@", [self restDomain], [category urlTitle]];
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    if (! IsEmpty(fields)) {
        [dictionary setValue:[fields componentsJoinedByString:@","] forKey:@"fields"];
    }
    if (! IsEmpty(ratings)) {
        [dictionary setValue:[ratings componentsJoinedByString:@","] forKey:@"rating"];
    }
    if (! IsEmpty(filter)) {
        [dictionary setValue:[filter componentsJoinedByString:@","] forKey:@"mediaFilter"];
    }
    if ((lowerRange.intValue > -1) && (upperRange.intValue > lowerRange.intValue)) {
        [dictionary setValue:[NSString stringWithFormat:@"%i-%i", lowerRange.intValue, upperRange.intValue] forKey:@"range"];
    }
    if (flag) {
        [dictionary setValue:@"true" forKey:@"count"];
    }
    if (msFromEpoch) {
        [dictionary setValue:msFromEpoch forKey:@"updatedSince"];
    }
    
    if ([[dictionary allKeys] count] == 0) {
        dictionary = nil;
    }
    
    [self getPath:restPath parameters:dictionary success:^(AFHTTPRequestOperation *operation, id response) {
        if (IsEmpty(response)) {
            failure([self errorForType:APIServiceError userInfo:nil]);
            return;
        }
        
        NSError *error = nil;
        id result = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        if (! error) {
            NSMutableArray *movieArray = [NSMutableArray array];
            if ([result isKindOfClass:[NSArray class]]) {
                for (NSDictionary *movieDictionary in result) {
                    ESPMedia *movie = [ESPMedia instanceFromDictionary:movieDictionary];
                    [movieArray addObject:movie];
                }
                NSNumber *count = [NSNumber numberWithInt:[movieArray count]];
                success(movieArray, count);
            } else if ([result isKindOfClass:[NSDictionary class]]) {
                NSArray *movies = [result valueForKey:@"entries"];
                for (NSDictionary *movieDictionary in movies) {
                    ESPMedia *movie = [ESPMedia instanceFromDictionary:movieDictionary];
                    [movieArray addObject:movie];
                }
                NSNumber *count = [result valueForKey:@"totalResults"];
                success(movieArray, count);
            }
        } else {
            failure([self errorForType:AccesssValidationError userInfo:error.userInfo]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([self errorForType:APIServiceError userInfo:error.userInfo]);
    }];
}

#pragma mark - Movie acquisition
- (void)obtainMetadataForMovie:(ESPMedia *)movie shouldIncludeAncillaryContent:(BOOL)flag success:(void (^)(ESPMedia *media))success failure:(void (^)(NSError *error))failure
{
    ESPMediaCategory *mediaCategory = [[movie categories] lastObject];
    ESPCatalogCategory *category = [[ESPCatalogCategory alloc] init];
    category.urlTitle = [mediaCategory mediaName];
    category.userTitle = [mediaCategory mediaName];
    [self obtainMetadataForMovieGUID:[movie guid] category:category shouldIncludeAncillaryContent:flag fields:nil filter:nil success:^(ESPMedia *media) {
        success(media);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)obtainMetadataForMovieGUID:(NSString *)guid category:(ESPCatalogCategory *)category shouldIncludeAncillaryContent:(BOOL)flag success:(void (^)(ESPMedia *media))success failure:(void (^)(NSError *error))failure
{
    [self obtainMetadataForMovieGUID:guid category:category shouldIncludeAncillaryContent:flag fields:nil filter:nil success:^(ESPMedia *media) {
        success(media);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)obtainMetadataForMovieGUID:(NSString *)guid category:(ESPCatalogCategory *)category shouldIncludeAncillaryContent:(BOOL)flag fields:(NSArray *)fields filter:(NSArray *)filter success:(void (^)(ESPMedia *media))success failure:(void (^)(NSError *error))failure
{
    [self obtainMetadataForMovieGUID:guid category:category shouldIncludeAncillaryContent:flag fields:fields filter:filter retry:MAX_RETRIES success:^(ESPMedia *media) {
        success(media);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)obtainMetadataForMovieGUID:(NSString *)guid category:(ESPCatalogCategory *)category shouldIncludeAncillaryContent:(BOOL)flag fields:(NSArray *)fields filter:(NSArray *)filter retry:(NSUInteger)retries success:(void (^)(ESPMedia *media))success failure:(void (^)(NSError *error))failure
{
    if (! [self hasCorrectHeaders]) {
        failure([self errorForType:InvalidHeadersError userInfo:nil]);
        return;
    }
    
    if (retries <= 0) {
        if (failure) {
            failure([self errorForType:APIServiceError userInfo:nil]);
        }
    } else {
        NSString *restPath = [NSString stringWithFormat:@"/%@/category/%@/%@", [self restDomain], [category urlTitle], guid];
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        if (! IsEmpty(fields)) {
            [dictionary setValue:[fields componentsJoinedByString:@","] forKey:@"fields"];
        }
        if (! IsEmpty(filter)) {
            [dictionary setValue:[filter componentsJoinedByString:@","] forKey:@"mediaFilter"];
        }
        if (flag) {
            [dictionary setValue:@"true" forKey:@"expanded"];
        }
        
        [self getPath:restPath parameters:dictionary success:^(AFHTTPRequestOperation *operation, id response) {
            if (IsEmpty(response)) {
                failure([self errorForType:AccesssValidationError userInfo:nil]);
                return;
            }
            
            NSError *error = nil;
            id result = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
            if (! error) {
                if ([result isKindOfClass:[NSArray class]]) {
                    NSDictionary *movieDictionary = [result lastObject];
                    ESPMedia *movie = [ESPMedia instanceFromDictionary:movieDictionary];
                    success(movie);
                } else if ([result isKindOfClass:[NSDictionary class]]) {
                    ESPMedia *movie = [ESPMedia instanceFromDictionary:result];
                    success(movie);
                }
            } else {
                [self obtainMetadataForMovieGUID:guid category:category shouldIncludeAncillaryContent:flag fields:fields filter:filter retry:retries-1 success:success failure:failure];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure([self errorForType:APIServiceError userInfo:error.userInfo]);
        }];
    }
}

#pragma mark - Catalog Types
- (void)obtainAllTypes:(void (^)(NSArray *result))success failure:(void (^)(NSError *))failure
{
    if (! [self hasCorrectHeaders]) {
        failure([self errorForType:InvalidHeadersError userInfo:nil]);
        return;
    }
    
    NSString *restPath = [NSString stringWithFormat:@"/%@/media", [self restDomain]];
    [self getPath:restPath parameters:nil success:^(AFHTTPRequestOperation *operation, id response) {
        if (IsEmpty(response)) {
            failure([self errorForType:AccesssValidationError userInfo:nil]);
            return;
        }
        
        NSError *error = nil;
        id result = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        if (! error) {
            NSMutableArray *results = [NSMutableArray array];
            if ([result isKindOfClass:[NSArray class]]) {
                for (NSDictionary *resultDictionary in result) {
                    ESPCatalogType *type = [ESPCatalogType instanceFromDictionary:resultDictionary];
                    [results addObject:type];
                }
            } else if ([result isKindOfClass:[NSDictionary class]]) {
                ESPCatalogType *type = [ESPCatalogType instanceFromDictionary:result];
                [results addObject:type];
            }
            success(results);
        } else {
            failure([self errorForType:APIServiceError userInfo:error.userInfo]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([self errorForType:APIServiceError userInfo:error.userInfo]);
    }];
}

- (void)obtainAllMediaForType:(ESPCatalogType *)type success:(void (^)(NSArray *media))success failure:(void (^)(NSError *error))failure
{
    [self obtainAllMediaForType:type filterFields:nil lowerRange:nil upperRange:nil ratings:nil mediaFilter:nil updatedSince:nil success:^(NSArray *media) {
        success(media);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)obtainAllMediaForType:(ESPCatalogType *)type updatedSince:(NSNumber *)msFromEpoch success:(void (^)(NSArray *media))success failure:(void (^)(NSError *error))failure
{
    [self obtainAllMediaForType:type filterFields:nil lowerRange:nil upperRange:nil ratings:nil mediaFilter:nil updatedSince:msFromEpoch success:^(NSArray *media) {
        success(media);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)obtainAllMediaForType:(ESPCatalogType *)type updatedSince:(NSNumber *)msFromEpoch filterFields:(NSArray *)fields success:(void (^)(NSArray *media))success failure:(void (^)(NSError *error))failure
{
    [self obtainAllMediaForType:type filterFields:fields lowerRange:nil upperRange:nil ratings:nil mediaFilter:nil updatedSince:msFromEpoch success:^(NSArray *media) {
        success(media);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)obtainAllMediaForType:(ESPCatalogType *)type updatedSince:(NSNumber *)msFromEpoch filterFields:(NSArray *)fields ratings:(NSArray *)ratings success:(void (^)(NSArray *media))success failure:(void (^)(NSError *error))failure
{
    [self obtainAllMediaForType:type filterFields:fields lowerRange:nil upperRange:nil ratings:ratings mediaFilter:nil updatedSince:msFromEpoch success:^(NSArray *media) {
        success(media);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)obtainAllMediaForType:(ESPCatalogType *)type filterFields:(NSArray *)fields lowerRange:(NSNumber *)lowerRange upperRange:(NSNumber *)upperRange ratings:(NSArray *)ratings mediaFilter:(NSArray *)filter updatedSince:(NSNumber *)msFromEpoch success:(void (^)(NSArray *media))success failure:(void (^)(NSError *error))failure
{
    if (! [self hasCorrectHeaders]) {
        failure([self errorForType:InvalidHeadersError userInfo:nil]);
        return;
    }
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    if (! IsEmpty(fields)) {
        [dictionary setValue:[fields componentsJoinedByString:@","] forKey:@"fields"];
    }
    if (! IsEmpty(ratings)) {
        [dictionary setValue:[ratings componentsJoinedByString:@","] forKey:@"rating"];
    }
    if (! IsEmpty(filter)) {
        [dictionary setValue:[filter componentsJoinedByString:@","] forKey:@"mediaFilter"];
    }
    if ((lowerRange.intValue > -1) && (upperRange.intValue > lowerRange.intValue)) {
        [dictionary setValue:[NSString stringWithFormat:@"%i-%i", lowerRange.intValue, upperRange.intValue] forKey:@"range"];
    }
    if (msFromEpoch) {
        [dictionary setValue:msFromEpoch forKey:@"updatedSince"];
    }
    
    if ([[dictionary allKeys] count] == 0) {
        dictionary = nil;
    }
    
    NSString *restPath = [NSString stringWithFormat:@"/%@/media/%@", [self restDomain], [type title]];
    [self getPath:restPath parameters:dictionary success:^(AFHTTPRequestOperation *operation, id response) {
        if (IsEmpty(response)) {
            failure([self errorForType:AccesssValidationError userInfo:nil]);
            return;
        }
        
        NSError *error = nil;
        id result = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        if (! error) {
            NSMutableArray *movieArray = [NSMutableArray array];
            if ([result isKindOfClass:[NSArray class]]) {
                for (NSDictionary *movieDictionary in result) {
                    ESPMedia *movie = [ESPMedia instanceFromDictionary:movieDictionary];
                    [movieArray addObject:movie];
                }
            } else if ([result isKindOfClass:[NSDictionary class]]) {
                ESPMedia *movie = [ESPMedia instanceFromDictionary:result];
                [movieArray addObject:movie];
            }
            success(movieArray);
        } else {
            failure([self errorForType:APIServiceError userInfo:error.userInfo]);            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([self errorForType:APIServiceError userInfo:error.userInfo]);
    }];

}

- (void)obtainMetadataForMovieGUID:(NSString *)guid type:(ESPCatalogType *)type success:(void (^)(ESPMedia *media))success failure:(void (^)(NSError *))failure
{
    if (! [self hasCorrectHeaders]) {
        failure([self errorForType:InvalidHeadersError userInfo:nil]);
        return;
    }
    
    NSString *restPath = [NSString stringWithFormat:@"/%@/media/%@/%@", [self restDomain], [type title], guid];
    [self getPath:restPath parameters:nil success:^(AFHTTPRequestOperation *operation, id response) {
        if (IsEmpty(response)) {
            failure([self errorForType:AccesssValidationError userInfo:nil]);
            return;
        }
        
        NSError *error = nil;
        id result = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        if (! error) {
            if ([result isKindOfClass:[NSArray class]]) {
                NSDictionary *movieDictionary = [result lastObject];
                ESPMedia *movie = [ESPMedia instanceFromDictionary:movieDictionary];
                success(movie);
            } else if ([result isKindOfClass:[NSDictionary class]]) {
                ESPMedia *movie = [ESPMedia instanceFromDictionary:result];
                success(movie);
            }
        } else {
            failure([self errorForType:APIServiceError userInfo:error.userInfo]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([self errorForType:APIServiceError userInfo:error.userInfo]);
    }];
}

- (void)obtainTrailerForGUID:(NSString *)guid success:(void (^)(ESPMedia *media))success failure:(void (^)(NSError *error))failure
{
    [self obtainMetadataForMovieGUID:guid type:[self trailerType] success:^(ESPMedia *media) {
        success(media);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

#pragma mark - Convenience Types
- (ESPCatalogType *)trailerType
{
    ESPCatalogType *type = [[ESPCatalogType alloc] init];
    type.title = @"trailer";
    type.descriptionText = @"Trailer";
    return type;
}

#pragma mark - Query
- (void)query:(NSString *)queryString ratings:(NSArray *)ratings success:(void (^)(ESPSearchResult *searchResult))success failure:(void (^)(NSError *error))failure
{
    if (! [self hasCorrectHeaders]) {
        failure([self errorForType:InvalidHeadersError userInfo:nil]);
        return;
    }
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    if (! IsEmpty(queryString)) {
        [dictionary setValue:queryString forKey:@"q"];
    }
    if (! IsEmpty(ratings)) {
        [dictionary setValue:[ratings componentsJoinedByString:@","] forKey:@"rating"];
    }
    
    NSString *restPath = [NSString stringWithFormat:@"/%@/search/", [self restDomain]];
    [self getPath:restPath parameters:dictionary success:^(AFHTTPRequestOperation *operation, id response) {
        if (IsEmpty(response)) {
            failure([self errorForType:AccesssValidationError userInfo:nil]);
            return;
        }
        
        NSError *error = nil;
        id result = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
        if (! error) {
            ESPSearchResult *searchResult = nil;
            if ([result isKindOfClass:[NSArray class]]) {
                result = [result lastObject];
            }
            if ([result isKindOfClass:[NSDictionary class]]) {
                searchResult =[ESPSearchResult instanceFromDictionary:result];
            }
            success(searchResult);
        } else {
            failure([self errorForType:APIServiceError userInfo:error.userInfo]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure([self errorForType:APIServiceError userInfo:error.userInfo]);
    }];
    
}

@end
