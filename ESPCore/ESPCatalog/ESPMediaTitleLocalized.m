//
//  ESPMediaTitleLocalized.m
//  
//
//  Created by Josh Paul on 4/24/13.
//  Copyright (c) 2013. All rights reserved.
//

#import "ESPMediaTitleLocalized.h"

@implementation ESPMediaTitleLocalized

+ (ESPMediaTitleLocalized *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    ESPMediaTitleLocalized *instance = [[ESPMediaTitleLocalized alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    [self setValuesForKeysWithDictionary:aDictionary];
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    return dictionary;
}

@end
