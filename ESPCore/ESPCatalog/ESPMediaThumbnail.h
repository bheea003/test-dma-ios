//
//  ESPMediaThumbnail.h
//  
//
//  Created by Josh Paul on 4/24/13.
//  Copyright (c) 2013. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESPMediaThumbnail : NSObject <NSCoding>
{
    NSArray *fileAssetTypes;
    NSNumber *fileBitrate;
    NSString *fileContentType;
    NSNumber *fileDuration;
    NSNumber *fileHeight;
    NSString *fileLanguage;
    NSString *fileUrl;
    NSNumber *fileWidth;
    NSString *fileFormat;
}

@property (nonatomic, copy) NSArray *fileAssetTypes;
@property (nonatomic, copy) NSNumber *fileBitrate;
@property (nonatomic, copy) NSString *fileContentType;
@property (nonatomic, copy) NSNumber *fileDuration;
@property (nonatomic, copy) NSNumber *fileHeight;
@property (nonatomic, copy) NSString *fileLanguage;
@property (nonatomic, copy) NSString *fileUrl;
@property (nonatomic, copy) NSNumber *fileWidth;
@property (nonatomic, copy) NSString *fileFormat;

+ (ESPMediaThumbnail *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
