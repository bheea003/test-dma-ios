//
//  ESPMediaContent.m
//
//
//  Created by Josh Paul on 4/24/13.
//  Copyright (c) 2013. All rights reserved.
//

#import "ESPMediaContent.h"

@implementation ESPMediaContent

@synthesize fileAssetTypes;
@synthesize fileBitrate;
@synthesize fileContentType;
@synthesize fileDuration;
@synthesize fileHeight;
@synthesize fileLanguage;
@synthesize fileUrl;
@synthesize fileWidth;
@synthesize fileFormat;
@synthesize pid;

+ (ESPMediaContent *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    ESPMediaContent *instance = [[ESPMediaContent alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    [self setValuesForKeysWithDictionary:aDictionary];
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"plfile$assetTypes"]) {
        if ([value isKindOfClass:[NSArray class]])
        {
            NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
            for (id valueMember in value) {
                [myMembers addObject:valueMember];
            }
            self.fileAssetTypes = myMembers;
        }
    } else if ([key isEqualToString:@"plfile$releases"]) {
        if ([value isKindOfClass:[NSArray class]]) {
            for (id valueMember in value) {
                if ([valueMember isKindOfClass:[NSDictionary class]]) {
                    self.pid = [valueMember valueForKey:@"plrelease$pid"];
                }
            }
        }
    } else {
        [super setValue:value forKey:key];
    }
}


- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"plfile$assetTypes"]) {
        [self setValue:value forKey:@"fileAssetTypes"];
    } else if ([key isEqualToString:@"plfile$bitrate"]) {
        [self setValue:value forKey:@"fileBitrate"];
    } else if ([key isEqualToString:@"plfile$contentType"]) {
        [self setValue:value forKey:@"fileContentType"];
    } else if ([key isEqualToString:@"plfile$duration"]) {
        [self setValue:value forKey:@"fileDuration"];
    } else if ([key isEqualToString:@"plfile$height"]) {
        [self setValue:value forKey:@"fileHeight"];
    } else if ([key isEqualToString:@"plfile$language"]) {
        [self setValue:value forKey:@"fileLanguage"];
    } else if ([key isEqualToString:@"plfile$url"]) {
        [self setValue:value forKey:@"fileUrl"];
    } else if ([key isEqualToString:@"plfile$width"]) {
        [self setValue:value forKey:@"fileWidth"];
    } else if ([key isEqualToString:@"plfile$format"]) {
        [self setValue:value forKey:@"fileFormat"];
    } else {
        NSLog(@"%@ undefined key from API! %@ [%@]",[self class], key, value);
        // [super setValue:value forUndefinedKey:key];
    }
}


- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    
    if (self.fileAssetTypes) {
        [dictionary setObject:self.fileAssetTypes forKey:@"fileAssetTypes"];
    }
    if (self.fileBitrate) {
        [dictionary setObject:self.fileBitrate forKey:@"fileBitrate"];
    }
    if (self.fileContentType) {
        [dictionary setObject:self.fileContentType forKey:@"fileContentType"];
    }
    if (self.fileDuration) {
        [dictionary setObject:self.fileDuration forKey:@"fileDuration"];
    }
    if (self.fileHeight) {
        [dictionary setObject:self.fileHeight forKey:@"fileHeight"];
    }
    if (self.fileLanguage) {
        [dictionary setObject:self.fileLanguage forKey:@"fileLanguage"];
    }
    if (self.fileUrl) {
        [dictionary setObject:self.fileUrl forKey:@"fileUrl"];
    }
    if (self.fileWidth) {
        [dictionary setObject:self.fileWidth forKey:@"fileWidth"];
    }
    if (self.fileFormat) {
        [dictionary setObject:self.fileFormat forKey:@"fileFormat"];
    }
    
    return dictionary;
}


//===========================================================
//  Keyed Archiving
//===========================================================
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.fileAssetTypes forKey:@"fileAssetTypes"];
    [encoder encodeObject:self.fileBitrate forKey:@"fileBitrate"];
    [encoder encodeObject:self.fileContentType forKey:@"fileContentType"];
    [encoder encodeObject:self.fileDuration forKey:@"fileDuration"];
    [encoder encodeObject:self.fileHeight forKey:@"fileHeight"];
    [encoder encodeObject:self.fileLanguage forKey:@"fileLanguage"];
    [encoder encodeObject:self.fileUrl forKey:@"fileUrl"];
    [encoder encodeObject:self.fileWidth forKey:@"fileWidth"];
    [encoder encodeObject:self.fileFormat forKey:@"fileFormat"];
    [encoder encodeObject:self.pid forKey:@"pid"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.fileAssetTypes = [decoder decodeObjectForKey:@"fileAssetTypes"];
        self.fileBitrate = [decoder decodeObjectForKey:@"fileBitrate"];
        self.fileContentType = [decoder decodeObjectForKey:@"fileContentType"];
        self.fileDuration = [decoder decodeObjectForKey:@"fileDuration"];
        self.fileHeight = [decoder decodeObjectForKey:@"fileHeight"];
        self.fileLanguage = [decoder decodeObjectForKey:@"fileLanguage"];
        self.fileUrl = [decoder decodeObjectForKey:@"fileUrl"];
        self.fileWidth = [decoder decodeObjectForKey:@"fileWidth"];
        self.fileFormat = [decoder decodeObjectForKey:@"fileFormat"];
        self.pid = [decoder decodeObjectForKey:@"pid"];
    }
    return self;
}

@end
