//
//  ESPMediaCategory.m
//  
//
//  Created by Josh Paul on 4/24/13.
//  Copyright (c) 2013. All rights reserved.
//

#import "ESPMediaCategory.h"

@implementation ESPMediaCategory

@synthesize mediaLabel;
@synthesize mediaName;
@synthesize mediaScheme;

+ (ESPMediaCategory *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    ESPMediaCategory *instance = [[ESPMediaCategory alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

#pragma mark - Convenience
- (NSString *)categoryName
{
    return [self mediaName];
}

- (NSString *)urlFriendlyCategoryName
{
    return [[self mediaName] stringByReplacingOccurrencesOfString:@" " withString:@"-"];
}

#pragma mark - Parsing
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    [self setValuesForKeysWithDictionary:aDictionary];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"media$label"]) {
        [self setValue:value forKey:@"mediaLabel"];
    } else if ([key isEqualToString:@"media$name"]) {
        [self setValue:value forKey:@"mediaName"];
    } else if ([key isEqualToString:@"media$scheme"]) {
        [self setValue:value forKey:@"mediaScheme"];
    } else {
        NSLog(@"%@ undefined key from API! %@ [%@]",[self class], key, value);
        // [super setValue:value forUndefinedKey:key];
    }
}

#pragma mark - Archiving
- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    if (self.mediaLabel) {
        [dictionary setObject:self.mediaLabel forKey:@"mediaLabel"];
    }
    if (self.mediaName) {
        [dictionary setObject:self.mediaName forKey:@"mediaName"];
    }
    if (self.mediaScheme) {
        [dictionary setObject:self.mediaScheme forKey:@"mediaScheme"];
    }
    return dictionary;
}

//===========================================================
//  Keyed Archiving
//===========================================================
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.mediaLabel forKey:@"mediaLabel"];
    [encoder encodeObject:self.mediaName forKey:@"mediaName"];
    [encoder encodeObject:self.mediaScheme forKey:@"mediaScheme"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.mediaLabel = [decoder decodeObjectForKey:@"mediaLabel"];
        self.mediaName = [decoder decodeObjectForKey:@"mediaName"];
        self.mediaScheme = [decoder decodeObjectForKey:@"mediaScheme"];
    }
    return self;
}

@end
