//
//  ESPCatalogCategory.m
//  
//
//  Created by Josh Paul on 7/3/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import "ESPCatalogCategory.h"

@implementation ESPCatalogCategory

+ (ESPCatalogCategory *)allCategory
{
    ESPCatalogCategory *category = [[ESPCatalogCategory alloc] init];
    category.urlTitle = @"All";
    category.userTitle = @"All";
    return category;
}

- (NSString *)description
{
    return [self userTitle];
}

- (NSComparisonResult)compare:(ESPCatalogCategory *)otherCategory
{
    return [self.userTitle compare:otherCategory.userTitle];
}

#pragma mark - Override
- (BOOL)isEqual:(id)object
{
    if ([object isKindOfClass:[self class]]) {
        NSString *inCheck = [[object urlTitle] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *selfCheck = [[self urlTitle] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        BOOL isEqual = [selfCheck caseInsensitiveCompare:inCheck] == NSOrderedSame;
        return isEqual;
    }
    return NO;
}

- (NSUInteger)hash
{
    return [[self urlTitle] hash] | [[self userTitle] hash];
}

+ (ESPCatalogCategory *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    ESPCatalogCategory *instance = [[ESPCatalogCategory alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    [self setValuesForKeysWithDictionary:aDictionary];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"description"]) {
        [self setValue:value forKey:@"userTitle"];
    } else if ([key isEqualToString:@"title"]) {
        [self setValue:value forKey:@"urlTitle"];
    } else {
        NSLog(@"ESPCatalogCategory undefined key from API! %@ [%@]", key, value);
        // [super setValue:value forUndefinedKey:key];
    }
}

- (NSDictionary *)dictionaryRepresentation
{

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    if (self.urlTitle) {
        [dictionary setObject:self.urlTitle forKey:@"urlTitle"];
    }
    if (self.userTitle) {
        [dictionary setObject:self.userTitle forKey:@"userTitle"];
    }
    return dictionary;
}

//===========================================================
//  Keyed Archiving
//===========================================================
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.urlTitle forKey:@"urlTitle"];
    [encoder encodeObject:self.userTitle forKey:@"userTitle"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if ((self = [super init])) {
        self.urlTitle = [decoder decodeObjectForKey:@"urlTitle"];
        self.userTitle = [decoder decodeObjectForKey:@"userTitle"];
    }
    return self;
}

@end
