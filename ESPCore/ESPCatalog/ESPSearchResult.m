//
//  ESPSearchResult.m
//
//
//  Created by Josh Paul on 7/17/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import "ESPSearchResult.h"

#import "ESPSearchResultItem.h"

@implementation ESPSearchResult

@synthesize dirty;
@synthesize searchResultItems;

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:[NSNumber numberWithBool:self.dirty] forKey:@"dirty"];
    [encoder encodeObject:self.searchResultItems forKey:@"searchResultItems"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if ((self = [super init])) {
        self.dirty = [(NSNumber *)[decoder decodeObjectForKey:@"dirty"] boolValue];
        self.searchResultItems = [decoder decodeObjectForKey:@"searchResultItems"];
    }
    return self;
}

+ (ESPSearchResult *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    ESPSearchResult *instance = [[ESPSearchResult alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    [self setValuesForKeysWithDictionary:aDictionary];
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"Search"]) {
        if ([value isKindOfClass:[NSArray class]])
        {
            NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
            for (id valueMember in value) {
                ESPSearchResultItem *populatedMember = [ESPSearchResultItem instanceFromDictionary:valueMember];
                [myMembers addObject:populatedMember];
            }
            self.searchResultItems = myMembers;
        }
    } else {
        [super setValue:value forKey:key];
    }
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"Dirty"]) {
        [self setValue:value forKey:@"dirty"];
    } else if ([key isEqualToString:@"Search"]) {
        [self setValue:value forKey:@"search"];
    } else if ([key isEqualToString:@"searchedFor"]) {
        // [self setValue:value forKey:@"searchedFor"];
    } else if ([key isEqualToString:@"searchedInstead"]) {
        // [self setValue:value forKey:@"searchedInstead"];
    } else {
        NSLog(@"ESPSearchResult undefined key from API! %@ [%@]", key, value);
        // [super setValue:value forUndefinedKey:key];
    }
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    [dictionary setObject:[NSNumber numberWithBool:self.dirty] forKey:@"dirty"];
    if (self.searchResultItems) {
        [dictionary setObject:self.searchResultItems forKey:@"searchResultItems"];
    }
    return dictionary;
}

@end
