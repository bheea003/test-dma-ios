//
//  ESPSearchResultItem.m
//  
//
//  Created by Josh Paul on 7/17/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import "ESPSearchResultItem.h"

@implementation ESPSearchResultItem

@synthesize sortTitle;
@synthesize guid;
@synthesize rating;
@synthesize title;

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.sortTitle forKey:@"sortTitle"];
    [encoder encodeObject:self.guid forKey:@"guid"];
    [encoder encodeObject:self.rating forKey:@"rating"];
    [encoder encodeObject:self.title forKey:@"title"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if ((self = [super init])) {
        self.sortTitle = [decoder decodeObjectForKey:@"sortTitle"];
        self.guid = [decoder decodeObjectForKey:@"guid"];
        self.rating = [decoder decodeObjectForKey:@"rating"];
        self.title = [decoder decodeObjectForKey:@"title"];
    }
    return self;
}

+ (ESPSearchResultItem *)instanceFromDictionary:(NSDictionary *)aDictionary
{

    ESPSearchResultItem *instance = [[ESPSearchResultItem alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;

}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{

    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }

    [self setValuesForKeysWithDictionary:aDictionary];

}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{

    if ([key isEqualToString:@"DSAA$sort_title"]) {
        [self setValue:value forKey:@"sortTitle"];
    } else {
        NSLog(@"ESPSearchResultItem undefined key from API! %@ [%@]", key, value);
        // [super setValue:value forUndefinedKey:key];
    }

}


- (NSDictionary *)dictionaryRepresentation
{

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.sortTitle) {
        [dictionary setObject:self.sortTitle forKey:@"sortTitle"];
    }

    if (self.guid) {
        [dictionary setObject:self.guid forKey:@"guid"];
    }

    if (self.rating) {
        [dictionary setObject:self.rating forKey:@"rating"];
    }

    if (self.title) {
        [dictionary setObject:self.title forKey:@"title"];
    }

    return dictionary;

}

@end
