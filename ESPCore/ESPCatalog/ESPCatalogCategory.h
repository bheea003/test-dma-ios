//
//  ESPCatalogCategory.h
//  
//
//  Created by Josh Paul on 7/3/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESPCatalogCategory : NSObject <NSCoding>

@property (nonatomic, copy) NSString *urlTitle;
@property (nonatomic, copy) NSString *userTitle;

+ (ESPCatalogCategory *)instanceFromDictionary:(NSDictionary *)aDictionary;
+ (ESPCatalogCategory *)allCategory;

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
