//
//  ESPMediaCredit.h
//  
//
//  Created by Josh Paul on 4/24/13.
//  Copyright (c) 2013. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESPMediaCredit : NSObject <NSCoding>
{
    NSString *mediaRole;
    NSString *mediaScheme;
    NSString *mediaValue;
}

@property (nonatomic, copy) NSString *mediaRole;
@property (nonatomic, copy) NSString *mediaScheme;
@property (nonatomic, copy) NSString *mediaValue;

+ (ESPMediaCredit *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

- (NSString *)fullName;
- (NSString *)role;

@end
