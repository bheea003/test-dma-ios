//
//  ESPMedia.m
//
//
//  Created by Josh Paul on 4/24/13.
//  Copyright (c) 2013. All rights reserved.
//

#import "ESPMedia.h"
#import "ESPConvenience.h"

#import "ESPMediaCategory.h"
#import "ESPMediaContent.h"
#import "ESPMediaCredit.h"
#import "ESPMediaRating.h"
#import "ESPMediaThumbnail.h"
#import "ESPMediaDescriptionLocalized.h"
#import "ESPMediaKeywordsLocalized.h"
#import "ESPMediaTitleLocalized.h"

@interface ESPMedia()
@property (nonatomic, copy, readwrite) NSNumber *added;
@property (nonatomic, assign, readwrite) BOOL isFree;
@property (nonatomic, copy, readwrite) NSNumber *updated;
@end

@implementation ESPMedia

+ (ESPMedia *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    ESPMedia *instance = [[ESPMedia alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

#pragma mark - Override
- (BOOL)isEqual:(id)object
{
    if ([object respondsToSelector:@selector(theatricalReleaseDate)]) {
        NSString *inGUID = [[object guid] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *selfGUID = [[self guid] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        BOOL sameGUID = [selfGUID caseInsensitiveCompare:inGUID] == NSOrderedSame;

        NSString *inTitle = [[object title] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *selfTitle = [[self title] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        BOOL sameTitle = [selfTitle caseInsensitiveCompare:inTitle] == NSOrderedSame;
        
        return sameGUID && sameTitle;
    }
    return NO;
}

- (NSUInteger)hash
{
    return [[self guid] hash] | [[self title] hash];
}

#pragma mark - Videos
- (ESPMediaContent *)contentForType:(NSString *)type
{
    for (ESPMediaContent *content in self.mediaContent) {
        NSString *assetType = [content.fileAssetTypes componentsJoinedByString:@","];
        BOOL isTypeOfInterest = [assetType caseInsensitiveCompare:type] == NSOrderedSame;
        if (isTypeOfInterest) {
            return content;
        }
    }
    return nil;
}

- (ESPMediaContent *)widevineContent
{
    return [self contentForType:@"SD,Widevine"];
}

- (ESPMediaContent *)unencryptedContent
{
    return [self contentForType:@"SD,Unencrypted"];
}

- (ESPMediaContent *)closedCaptionContent
{
    return [self contentForType:@"TT"];
}

// Trailers
- (BOOL)isTrailer
{
    BOOL isTrailer = [self.title caseInsensitiveCompare:@"Trailer"] == NSOrderedSame;
    return isTrailer;
}

- (ESPMediaContent *)trailer
{
    if ([self isTrailer]) {
        return [self trailerForContent:self.mediaContent];
    } else {
        for (ESPMedia *movie in self.ancillaryContent) {
            NSString *movieTitle = movie.title;
            if ([movieTitle caseInsensitiveCompare:@"Trailer"] == NSOrderedSame) {
                ESPMediaContent *trailer = [self trailerForContent:movie.mediaContent];
                if (trailer) return trailer;
            }
        }
    }
    return nil;
}

- (ESPMediaContent *)trailerForContent:(NSArray *)contentArray
{
    for (ESPMediaContent *content in contentArray) {
        NSString *assetType = [content.fileAssetTypes componentsJoinedByString:@","];
        BOOL isUnencryptedSD = ([assetType caseInsensitiveCompare:@"SD,Unencrypted"] == NSOrderedSame) || ([assetType caseInsensitiveCompare:@"Unencrypted,SD"]);
        if (isUnencryptedSD) {
            return content;
        }
    }
    return nil;
}

- (NSArray *)sortedAncillaryContent
{
    return [self.ancillaryContent sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        BOOL first = [(ESPMedia *)a isTrailer];
        BOOL second = [(ESPMedia *)b isTrailer];
        
        if (first && !second) {
            return NSOrderedAscending;
        } else if (second && !first) {
            return NSOrderedDescending;
        }
        return NSOrderedSame;
    }];
}

#pragma mark - Thumbnails
- (ESPMediaThumbnail *)thumbnailForType:(NSString *)thumbnailType
{
    for (ESPMediaThumbnail *thumbnail in self.mediaThumbnails) {
        NSString *assetType = [thumbnail.fileAssetTypes componentsJoinedByString:@","];
        BOOL isTypeOfInterest = [assetType caseInsensitiveCompare:thumbnailType] == NSOrderedSame;
        if (isTypeOfInterest) {
            return thumbnail;
        }
    }
    return nil;
}

- (ESPMediaThumbnail *)posterThumbnail
{
    return [self thumbnailForType:@"ThumbnailPoster"];
}

- (ESPMediaThumbnail *)posterThumbnail2x
{
    return [self thumbnailForType:@"ThumbnailPoster2x"];
}

- (ESPMediaThumbnail *)smallThumbnail
{
    return [self thumbnailForType:@"ThumbnailSmall"];
}

- (ESPMediaThumbnail *)mediumThumbnail
{
    return [self thumbnailForType:@"ThumbnailMedium"];
}

- (ESPMediaThumbnail *)largeThumbnail
{
    return [self thumbnailForType:@"MovieDetailArt"];    
}

#pragma mark - Social Media
- (NSString *)shortSocialMediaDescription
{
    NSString *truncatedSynopsis = nil;
    
    int smsLength = 140;
    int padding = 3;
    int titleLength = [self title].length;
    int synopsisLength = [self mediumSynopsis].length;
    if ((titleLength + synopsisLength + padding) > smsLength) {
        int end = smsLength - [self title].length - padding;
        truncatedSynopsis = [[self mediumSynopsis] substringWithRange:NSMakeRange(0, end)];
    } else {
        truncatedSynopsis = [self mediumSynopsis];
    }
    return [NSString stringWithFormat:@"%@: %@", [self title], truncatedSynopsis];
}

- (NSString *)longSocialMediaDescription
{
    return [NSString stringWithFormat:@"%@: %@", [self title], [self longSynopsis]];
}


#pragma mark - Convenience
- (BOOL)hasBeenReleased
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *releaseDate = [dateFormat dateFromString:self.theatricalReleaseDate];
    return [releaseDate earlierDate:[NSDate date]] == releaseDate;
}

- (NSArray *)categories
{
    return [self mediaCategories];
}

- (NSString *)ratingsAsString
{
    NSMutableArray *array = [NSMutableArray array];
    for (ESPMediaRating *rating in self.mediaRatings) {
        [array addObject:[rating rating]];
    }
    return [[array componentsJoinedByString:@", "] uppercaseString];
}

- (NSString *)castAsString
{
    NSMutableArray *array = [NSMutableArray array];
    for (ESPMediaCredit *credit in self.mediaCredits) {
        [array addObject:[credit fullName]];
    }
    return [array componentsJoinedByString:@", "];
    
}

- (NSString *)userPresentableReleaseDate
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *releaseDate = [dateFormat dateFromString:self.theatricalReleaseDate];
    [dateFormat setDateFormat:@"MMMM d, YYYY"];
    return [dateFormat stringFromDate:releaseDate];
}

- (NSString *)userPresentableDescription
{
    NSString *runningTime = [NSString stringWithFormat:@"%@ mins.", [self runtimeMinutes]];
    return [NSString stringWithFormat:@"%@ [%@] (%@)", [self title], [self ratingsAsString], runningTime];
}

#pragma mark - Parsing
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    // BUG START
    // DATE: 25APRIL2013
    // INFO: API changes JSON structure when asking for 'count'
    // BUG: 6997628
    // WORKAROUND: locate 'entries' key and pull data from there
    // REMOVE: upon fix from API
    if ([aDictionary valueForKey:@"entries"]) {
        NSArray *entries = [aDictionary valueForKey:@"entries"];
        id object = [entries lastObject];
        if ([object isKindOfClass:[NSArray class]]) {
            aDictionary = [object lastObject];
        } else {
            aDictionary = object;
        }
    }
    // BUG END
    
    [self setValuesForKeysWithDictionary:aDictionary];
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"DSAA$BonusIds"]) {
        if ([value isKindOfClass:[NSArray class]])
        {
            NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
            for (id valueMember in value) {
                [myMembers addObject:valueMember];
            }
            self.bonusIDs = myMembers;
        }
    } else if ([key isEqualToString:@"DSAA$seo_keywords"]) {
        if ([value isKindOfClass:[NSArray class]])
        {  
            NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
            for (id valueMember in value) {
                [myMembers addObject:valueMember];
            }
            self.seoKeywords = myMembers;   
        }
    } else if ([key isEqualToString:@"DSAA$TrailerIds"]) {
        if ([value isKindOfClass:[NSArray class]])
        {
            NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
            for (id valueMember in value) {
                [myMembers addObject:valueMember];
            }
            self.trailerIDs = myMembers;
        }
    } else if ([key isEqualToString:@"media$categories"]) {
        if ([value isKindOfClass:[NSArray class]])
        {
            NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
            for (id valueMember in value) {
                ESPMediaCategory *populatedMember = [ESPMediaCategory instanceFromDictionary:valueMember];
                [myMembers addObject:populatedMember];
            }
            self.mediaCategories = myMembers;
        }
    } else if ([key isEqualToString:@"media$content"]) {
        if ([value isKindOfClass:[NSArray class]])
        {
            NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
            for (id valueMember in value) {
                ESPMediaContent *populatedMember = [ESPMediaContent instanceFromDictionary:valueMember];
                [myMembers addObject:populatedMember];
            }
            self.mediaContent = myMembers;
        }
    } else if ([key isEqualToString:@"media$credits"]) {
        if ([value isKindOfClass:[NSArray class]])
        {
            NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
            for (id valueMember in value) {
                ESPMediaCredit *populatedMember = [ESPMediaCredit instanceFromDictionary:valueMember];
                [myMembers addObject:populatedMember];
            }
            self.mediaCredits = myMembers;
        }
    } else if ([key isEqualToString:@"media$ratings"]) {
        if ([value isKindOfClass:[NSArray class]])
        {
            NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
            for (id valueMember in value) {
                ESPMediaRating *populatedMember = [ESPMediaRating instanceFromDictionary:valueMember];
                [myMembers addObject:populatedMember];
            }
            self.mediaRatings = myMembers;
        }
    } else if ([key isEqualToString:@"media$thumbnails"]) {
        if ([value isKindOfClass:[NSArray class]])
        {
            NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
            for (id valueMember in value) {
                ESPMediaThumbnail *populatedMember = [ESPMediaThumbnail instanceFromDictionary:valueMember];
                [myMembers addObject:populatedMember];
            }
            self.mediaThumbnails = myMembers;
        }
     } else if ([key isEqualToString:@"plmedia$descriptionLocalized"]) {
        /*if ([value isKindOfClass:[NSDictionary class]]) {
            self.mediaDescriptionLocalized = [ESPMediaDescriptionLocalized instanceFromDictionary:value];
        }*/
    } else if ([key isEqualToString:@"plmedia$keywordsLocalized"]) {
        /*if ([value isKindOfClass:[NSDictionary class]]) {
            self.mediaKeywordsLocalized = [ESPMediaKeywordsLocalized instanceFromDictionary:value];
        }*/
    } else if ([key isEqualToString:@"plmedia$titleLocalized"]) {
        /*if ([value isKindOfClass:[NSDictionary class]]) {
            self.mediaTitleLocalized = [ESPMediaTitleLocalized instanceFromDictionary:value];
        }*/
    } else if ([key isEqualToString:@"ancillary"]) {
        if ([value isKindOfClass:[NSArray class]])
        {
            NSMutableArray *ancillaryArray = [NSMutableArray arrayWithCapacity:[value count]];
            for (id valueMember in value) {
                ESPMedia *movie = [ESPMedia instanceFromDictionary:valueMember];
                [ancillaryArray addObject:movie];
            }
            self.ancillaryContent = ancillaryArray;
        }
    } else {
        [super setValue:value forKey:key];
    }
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"added"]) {
        [self setValue:value forKey:@"added"];
    } else if ([key isEqualToString:@"updated"]) {
        [self setValue:value forKey:@"updated"];
    } else if ([key isEqualToString:@"description"]) {
        [self setValue:value forKey:@"descriptionText"];
    } else if ([key isEqualToString:@"DSAA$BonusIds"]) {
        [self setValue:value forKey:@"bonusIDs"];
    } else if ([key isEqualToString:@"DSAA$contentType"]) {
        [self setValue:value forKey:@"contentType"];
    } else if ([key isEqualToString:@"DSAA$free"]) {
        [self setValue:value forKey:@"isFree"];
    } else if ([key isEqualToString:@"DSAA$long_synopsis"]) {
        [self setValue:value forKey:@"longSynopsis"];
    } else if ([key isEqualToString:@"DSAA$medium_synopsis"]) {
        [self setValue:value forKey:@"mediumSynopsis"];
    } else if ([key isEqualToString:@"DSAA$mpm_i"]) {
        [self setValue:value forKey:@"mpmi"];
    } else if ([key isEqualToString:@"DSAA$mpm_p"]) {
        [self setValue:value forKey:@"mpmp"];
    } else if ([key isEqualToString:@"DSAA$runtime_minutes"]) {
        [self setValue:value forKey:@"runtimeMinutes"];
    } else if ([key isEqualToString:@"DSAA$seo_keywords"]) {
        [self setValue:value forKey:@"seoKeywords"];
    } else if ([key isEqualToString:@"DSAA$seo_title"]) {
        [self setValue:value forKey:@"seoTitle"];
    } else if ([key isEqualToString:@"DSAA$sort_title"]) {
        [self setValue:value forKey:@"sortTitle"];
    } else if ([key isEqualToString:@"DSAA$tag_title"]) {
        [self setValue:value forKey:@"tagTitle"];
    } else if ([key isEqualToString:@"DSAA$theatrical_release_date"]) {
        [self setValue:value forKey:@"theatricalReleaseDate"];
    } else if ([key isEqualToString:@"DSAA$titleRestrictions"]) {
        [self setValue:value forKey:@"titleRestrictions"];
    } else if ([key isEqualToString:@"DSAA$TrailerIds"]) {
        [self setValue:value forKey:@"trailerIDs"];
    } else if ([key isEqualToString:@"DSAA$umid"]) {
        [self setValue:value forKey:@"umid"];
    } else if ([key isEqualToString:@"DSAA$umid_hd"]) {
        [self setValue:value forKey:@"umidHD"];
    } else if ([key isEqualToString:@"DSAA$umid_hdplus"]) {
        [self setValue:value forKey:@"umidHDPlus"];
    } else if ([key isEqualToString:@"DSAA$umid_sd"]) {
        [self setValue:value forKey:@"umidSD"];
    } else if ([key isEqualToString:@"id"]) {
        [self setValue:value forKey:@"movieID"];
    } else if ([key isEqualToString:@"media$categories"]) {
        [self setValue:value forKey:@"mediaCategories"];
    } else if ([key isEqualToString:@"media$content"]) {
        [self setValue:value forKey:@"mediaContent"];
    } else if ([key isEqualToString:@"media$credits"]) {
        [self setValue:value forKey:@"mediaCredits"];
    } else if ([key isEqualToString:@"media$keywords"]) {
        [self setValue:value forKey:@"mediaKeywords"];
    } else if ([key isEqualToString:@"media$ratings"]) {
        [self setValue:value forKey:@"mediaRatings"];
    } else if ([key isEqualToString:@"media$thumbnails"]) {
        [self setValue:value forKey:@"mediaThumbnails"];
    } else if ([key isEqualToString:@"plmedia$descriptionLocalized"]) {
        [self setValue:value forKey:@"mediaDescriptionLocalized"];
    } else if ([key isEqualToString:@"plmedia$keywordsLocalized"]) {
        [self setValue:value forKey:@"mediaKeywordsLocalized"];
    } else if ([key isEqualToString:@"plmedia$titleLocalized"]) {
        [self setValue:value forKey:@"mediaTitleLocalized"];
    } else if ([key isEqualToString:@"DSAA$restrictionsStartDate"]) {
        [self setValue:value forKey:@"restrictionsStartDate"];
    } else if ([key isEqualToString:@"DSAA$restrictionsEndDate"]) {
        [self setValue:value forKey:@"restrictionsEndDate"];
    } else if ([key isEqualToString:@"ancillary"]) {
        [self setValue:value forKey:@"ancillary"];
    } else if ([key isEqualToString:@"entitlementLevel"]) {
        [self setValue:value forKey:@"entitlementLevel"];
    } else if ([key isEqualToString:@"$xmlns"]) {
        // do nothing
    }
    else {
        NSLog(@"%@ undefined key from API! %@ [%@]",[self class], key, value);
        // [super setValue:value forUndefinedKey:key];
    }
}

#pragma mark - Archiving
- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.added) {
        [dictionary setObject:self.added forKey:@"added"];
    }
    if (self.descriptionText) {
        [dictionary setObject:self.descriptionText forKey:@"descriptionText"];
    }
    if (self.bonusIDs) {
        [dictionary setObject:self.bonusIDs forKey:@"bonusIDs"];
    }
    if (self.contentType) {
        [dictionary setObject:self.contentType forKey:@"contentType"];
    }
    [dictionary setObject:[NSNumber numberWithBool:self.isFree] forKey:@"isFree"];
    if (self.longSynopsis) {
        [dictionary setObject:self.longSynopsis forKey:@"longSynopsis"];
    }
    if (self.mediumSynopsis) {
        [dictionary setObject:self.mediumSynopsis forKey:@"mediumSynopsis"];
    }
    if (self.mpmi) {
        [dictionary setObject:self.mpmi forKey:@"mpmi"];
    }
    if (self.mpmp) {
        [dictionary setObject:self.mpmp forKey:@"mpmp"];
    }
    if (self.runtimeMinutes) {
        [dictionary setObject:self.runtimeMinutes forKey:@"runtimeMinutes"];
    }
    if (self.seoKeywords) {
        [dictionary setObject:self.seoKeywords forKey:@"seoKeywords"];
    }
    if (self.seoTitle) {
        [dictionary setObject:self.seoTitle forKey:@"seoTitle"];
    }
    if (self.sortTitle) {
        [dictionary setObject:self.sortTitle forKey:@"sortTitle"];
    }
    if (self.tagTitle) {
        [dictionary setObject:self.tagTitle forKey:@"tagTitle"];
    }
    if (self.theatricalReleaseDate) {
        [dictionary setObject:self.theatricalReleaseDate forKey:@"theatricalReleaseDate"];
    }
    if (self.titleRestrictions) {
        [dictionary setObject:self.titleRestrictions forKey:@"titleRestrictions"];
    }
    if (self.trailerIDs) {
        [dictionary setObject:self.trailerIDs forKey:@"trailerIDs"];
    }
    if (self.umid) {
        [dictionary setObject:self.umid forKey:@"umid"];
    }
    if (self.umidHD) {
        [dictionary setObject:self.umidHD forKey:@"umidHD"];
    }
    if (self.umidHDPlus) {
        [dictionary setObject:self.umidHDPlus forKey:@"umidHDPlus"];
    }
    if (self.umidSD) {
        [dictionary setObject:self.umidSD forKey:@"umidSD"];
    }
    if (self.guid) {
        [dictionary setObject:self.guid forKey:@"guid"];
    }
    if (self.movieID) {
        [dictionary setObject:self.movieID forKey:@"movieID"];
    }
    if (self.mediaCategories) {
        [dictionary setObject:self.mediaCategories forKey:@"mediaCategories"];
    }
    if (self.mediaContent) {
        [dictionary setObject:self.mediaContent forKey:@"mediaContent"];
    }
    if (self.mediaCredits) {
        [dictionary setObject:self.mediaCredits forKey:@"mediaCredits"];
    }
    if (self.mediaKeywords) {
        [dictionary setObject:self.mediaKeywords forKey:@"mediaKeywords"];
    }
    if (self.mediaRatings) {
        [dictionary setObject:self.mediaRatings forKey:@"mediaRatings"];
    }
    if (self.mediaThumbnails) {
        [dictionary setObject:self.mediaThumbnails forKey:@"mediaThumbnails"];
    }
    /*
    if (self.mediaDescriptionLocalized) {
        [dictionary setObject:self.mediaDescriptionLocalized forKey:@"mediaDescriptionLocalized"];
    }
    if (self.mediaKeywordsLocalized) {
        [dictionary setObject:self.mediaKeywordsLocalized forKey:@"mediaKeywordsLocalized"];
    }
    if (self.mediaTitleLocalized) {
        [dictionary setObject:self.mediaTitleLocalized forKey:@"mediaTitleLocalized"];
    }
    */
    if (self.title) {
        [dictionary setObject:self.title forKey:@"title"];
    }
    if (self.updated) {
        [dictionary setObject:self.updated forKey:@"updated"];
    }
    if (self.ancillaryContent) {
        [dictionary setObject:self.ancillaryContent forKey:@"ancillaryContent"];
    }
    if (self.restrictionsEndDate) {
        [dictionary setObject:self.restrictionsEndDate forKey:@"restrictionsEndDate"];
    }
    if (self.entitlementLevel) {
        [dictionary setObject:self.entitlementLevel forKey:@"entitlementLevel"];
    }
    return dictionary;
}


//===========================================================
//  Keyed Archiving
//===========================================================
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.title forKey:@"title"];
    [encoder encodeObject:self.guid forKey:@"guid"];
    [encoder encodeObject:self.mediaThumbnails forKey:@"mediaThumbnails"];
    [encoder encodeObject:self.added forKey:@"added"];
    [encoder encodeObject:self.descriptionText forKey:@"descriptionText"];
    [encoder encodeObject:self.bonusIDs forKey:@"bonusIDs"];
    [encoder encodeObject:self.contentType forKey:@"contentType"];
    [encoder encodeBool:self.isFree forKey:@"isFree"];
    [encoder encodeObject:self.longSynopsis forKey:@"longSynopsis"];
    [encoder encodeObject:self.mediumSynopsis forKey:@"mediumSynopsis"];
    [encoder encodeObject:self.mpmi forKey:@"mpmi"];
    [encoder encodeObject:self.mpmp forKey:@"mpmp"];
    [encoder encodeObject:self.runtimeMinutes forKey:@"runtimeMinutes"];
    [encoder encodeObject:self.seoKeywords forKey:@"seoKeywords"];
    [encoder encodeObject:self.seoTitle forKey:@"seoTitle"];
    [encoder encodeObject:self.sortTitle forKey:@"sortTitle"];
    [encoder encodeObject:self.tagTitle forKey:@"tagTitle"];
    [encoder encodeObject:self.theatricalReleaseDate forKey:@"theatricalReleaseDate"];
    [encoder encodeObject:self.titleRestrictions forKey:@"titleRestrictions"];
    [encoder encodeObject:self.trailerIDs forKey:@"trailerIDs"];
    [encoder encodeObject:self.umid forKey:@"umid"];
    [encoder encodeObject:self.umidHD forKey:@"umidHD"];
    [encoder encodeObject:self.umidHDPlus forKey:@"umidHDPlus"];
    [encoder encodeObject:self.umidSD forKey:@"umidSD"];
    [encoder encodeObject:self.movieID forKey:@"movieID"];
    [encoder encodeObject:self.mediaCategories forKey:@"mediaCategories"];
    [encoder encodeObject:self.mediaContent forKey:@"mediaContent"];
    [encoder encodeObject:self.mediaCredits forKey:@"mediaCredits"];
    [encoder encodeObject:self.mediaKeywords forKey:@"mediaKeywords"];
    [encoder encodeObject:self.mediaRatings forKey:@"mediaRatings"];
    [encoder encodeObject:self.updated forKey:@"updated"];
    [encoder encodeObject:self.restrictionsStartDate forKey:@"restrictionsStartDate"];
    [encoder encodeObject:self.restrictionsEndDate forKey:@"restrictionsEndDate"];
    [encoder encodeObject:self.ancillaryContent forKey:@"ancillaryContent"];
    [encoder encodeObject:self.entitlementLevel forKey:@"entitlementLevel"];
    // [encoder encodeObject:self.mediaDescriptionLocalized forKey:@"mediaDescriptionLocalized"];
    // [encoder encodeObject:self.mediaKeywordsLocalized forKey:@"mediaKeywordsLocalized"];
    // [encoder encodeObject:self.mediaTitleLocalized forKey:@"mediaTitleLocalized"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.title = [decoder decodeObjectForKey:@"title"];
        self.guid = [decoder decodeObjectForKey:@"guid"];
        self.mediaThumbnails = [decoder decodeObjectForKey:@"mediaThumbnails"];
        self.added = [decoder decodeObjectForKey:@"added"];
        self.descriptionText = [decoder decodeObjectForKey:@"descriptionText"];
        self.bonusIDs = [decoder decodeObjectForKey:@"bonusIDs"];
        self.contentType = [decoder decodeObjectForKey:@"contentType"];
        self.isFree = [decoder decodeBoolForKey:@"isFree"];
        self.longSynopsis = [decoder decodeObjectForKey:@"longSynopsis"];
        self.mediumSynopsis = [decoder decodeObjectForKey:@"mediumSynopsis"];
        self.mpmi = [decoder decodeObjectForKey:@"mpmi"];
        self.mpmp = [decoder decodeObjectForKey:@"mpmp"];
        self.runtimeMinutes = [decoder decodeObjectForKey:@"runtimeMinutes"];
        self.seoKeywords = [decoder decodeObjectForKey:@"seoKeywords"];
        self.seoTitle = [decoder decodeObjectForKey:@"seoTitle"];
        self.sortTitle = [decoder decodeObjectForKey:@"sortTitle"];
        self.tagTitle = [decoder decodeObjectForKey:@"tagTitle"];
        self.theatricalReleaseDate = [decoder decodeObjectForKey:@"theatricalReleaseDate"];
        self.titleRestrictions = [decoder decodeObjectForKey:@"titleRestrictions"];
        self.trailerIDs = [decoder decodeObjectForKey:@"trailerIDs"];
        self.umid = [decoder decodeObjectForKey:@"umid"];
        self.umidHD = [decoder decodeObjectForKey:@"umidHD"];
        self.umidHDPlus = [decoder decodeObjectForKey:@"umidHDPlus"];
        self.umidSD = [decoder decodeObjectForKey:@"umidSD"];
        self.movieID = [decoder decodeObjectForKey:@"movieID"];
        self.mediaCategories = [decoder decodeObjectForKey:@"mediaCategories"];
        self.mediaContent = [decoder decodeObjectForKey:@"mediaContent"];
        self.mediaCredits = [decoder decodeObjectForKey:@"mediaCredits"];
        self.mediaKeywords = [decoder decodeObjectForKey:@"mediaKeywords"];
        self.mediaRatings = [decoder decodeObjectForKey:@"mediaRatings"];
        self.updated = [decoder decodeObjectForKey:@"updated"];
        self.restrictionsStartDate = [decoder decodeObjectForKey:@"restrictionsStartDate"];
        self.restrictionsEndDate = [decoder decodeObjectForKey:@"restrictionsEndDate"];
        self.ancillaryContent = [decoder decodeObjectForKey:@"ancillaryContent"];
        self.entitlementLevel = [decoder decodeObjectForKey:@"entitlementLevel"];
        // self.mediaDescriptionLocalized = [decoder decodeObjectForKey:@"mediaDescriptionLocalized"];
        // self.mediaKeywordsLocalized = [decoder decodeObjectForKey:@"mediaKeywordsLocalized"];
        // self.mediaTitleLocalized = [decoder decodeObjectForKey:@"mediaTitleLocalized"];
    }
    return self;
}

@end
