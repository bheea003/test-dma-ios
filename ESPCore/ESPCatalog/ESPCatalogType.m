//
//  ESPCatalogType.m
//  
//
//  Created by Josh Paul on 7/9/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import "ESPCatalogType.h"

@implementation ESPCatalogType

@synthesize descriptionText;
@synthesize title;

+ (ESPCatalogType *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    ESPCatalogType *instance = [[ESPCatalogType alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    [self setValuesForKeysWithDictionary:aDictionary];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"description"]) {
        [self setValue:value forKey:@"descriptionText"];
    } else {
        NSLog(@"ESPCatalogType undefined key from API! %@ [%@]", key, value);
        // [super setValue:value forUndefinedKey:key];
    }
}


- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    if (self.descriptionText) {
        [dictionary setObject:self.descriptionText forKey:@"descriptionText"];
    }
    if (self.title) {
        [dictionary setObject:self.title forKey:@"title"];
    }
    return dictionary;
}

//===========================================================
//  Keyed Archiving
//===========================================================
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.descriptionText forKey:@"descriptionText"];
    [encoder encodeObject:self.title forKey:@"title"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.descriptionText = [decoder decodeObjectForKey:@"descriptionText"];
        self.title = [decoder decodeObjectForKey:@"title"];
    }
    return self;
}

@end
