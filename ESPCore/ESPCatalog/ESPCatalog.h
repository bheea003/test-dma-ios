//
//  ESPCatalog.h
//  ESPCatalog
//
//  Created by JP on 4/24/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESPAPIClient.h"

// Field Filters
FOUNDATION_EXPORT NSString *const FieldFilterAdded;
FOUNDATION_EXPORT NSString *const FieldFilterBonusIDs;
FOUNDATION_EXPORT NSString *const FieldFilterContentType;
FOUNDATION_EXPORT NSString *const FieldFilterDescriptionText;
FOUNDATION_EXPORT NSString *const FieldFilterGUID;
FOUNDATION_EXPORT NSString *const FieldFilterIsFree;
FOUNDATION_EXPORT NSString *const FieldFilterLongSynopsis;
FOUNDATION_EXPORT NSString *const FieldFilterMediaCategories;
FOUNDATION_EXPORT NSString *const FieldFilterMediaContent;
FOUNDATION_EXPORT NSString *const FieldFilterMediaCredits;
FOUNDATION_EXPORT NSString *const FieldFilterMediaDescriptionLocalized;
FOUNDATION_EXPORT NSString *const FieldFilterMediaFilterwords;
FOUNDATION_EXPORT NSString *const FieldFilterMediaFilterwordsLocalized;
FOUNDATION_EXPORT NSString *const FieldFilterMediaRatings;
FOUNDATION_EXPORT NSString *const FieldFilterMediaThumbnails;
FOUNDATION_EXPORT NSString *const FieldFilterMediaTitleLocalized;
FOUNDATION_EXPORT NSString *const FieldFilterMediumSynopsis;
FOUNDATION_EXPORT NSString *const FieldFilterMovieID;
FOUNDATION_EXPORT NSString *const FieldFilterMPMI;
FOUNDATION_EXPORT NSString *const FieldFilterMPMP;
FOUNDATION_EXPORT NSString *const FieldFilterRestrictionsEndDate;
FOUNDATION_EXPORT NSString *const FieldFilterRestrictionsStartDate;
FOUNDATION_EXPORT NSString *const FieldFilterRuntimeMinutes;
FOUNDATION_EXPORT NSString *const FieldFilterSEOFilterwords;
FOUNDATION_EXPORT NSString *const FieldFilterSEOTitle;
FOUNDATION_EXPORT NSString *const FieldFilterSortTitle;
FOUNDATION_EXPORT NSString *const FieldFilterTagTitle;
FOUNDATION_EXPORT NSString *const FieldFilterTheatricalReleaseDate;
FOUNDATION_EXPORT NSString *const FieldFilterTitle;
FOUNDATION_EXPORT NSString *const FieldFilterTitleRestrictions;
FOUNDATION_EXPORT NSString *const FieldFilterTrailerIDs;
FOUNDATION_EXPORT NSString *const FieldFilterUMID;
FOUNDATION_EXPORT NSString *const FieldFilterUMIDHD;
FOUNDATION_EXPORT NSString *const FieldFilterUMIDHDPlus;
FOUNDATION_EXPORT NSString *const FieldFilterUMIDSD;
FOUNDATION_EXPORT NSString *const FieldFilterUpdated;

// Media Filters
FOUNDATION_EXPORT NSString *const MediaFilterWidevine;
FOUNDATION_EXPORT NSString *const MediaFilterClosedCaption;
FOUNDATION_EXPORT NSString *const MediaFilterFlash;
FOUNDATION_EXPORT NSString *const MediaFilterPlayReady;

@class ESPMedia;
@class ESPCatalogCategory;
@class ESPCatalogType;
@class ESPSearchResult;
@interface ESPCatalog : ESPAPIClient

+ (ESPCatalog *)catalogForApplication:(NSString *)applicationName language:(NSString *)iso639dash1 country:(NSString *)iso3166dash1alpha2;
+ (NSArray *)guidOnlyFilter;
+ (NSArray *)guidTitleAndThumbnailsOnlyFilter;

// Categories
- (void)obtainAllCategories:(void (^)(NSArray *categoryResults))success failure:(void (^)(NSError *error))failure;
- (void)obtainAllCategoriesFilteringOut:(NSArray *)categoriesToRemove success:(void (^)(NSArray *filteredCategories))success failure:(void (^)(NSError *error))failure;

- (void)obtainCountForCategory:(ESPCatalogCategory *)category updatedSince:(NSNumber *)msFromEpoch success:(void (^)(NSNumber *categoryCount))success failure:(void (^)(NSError *error))failure;

- (void)obtainCategoryForTitle:(NSString *)categoryTitle success:(void (^)(NSArray *categoryResults))success failure:(void (^)(NSError *error))failure;

- (void)obtainCategory:(ESPCatalogCategory *)category updatedSince:(NSNumber *)msFromEpoch success:(void (^)(NSArray *categoryResults, NSNumber *categoryCount))success failure:(void (^)(NSError *error))failure;
- (void)obtainCategory:(ESPCatalogCategory *)category updatedSince:(NSNumber *)msFromEpoch includeCount:(BOOL)flag success:(void (^)(NSArray *categoryResults, NSNumber *categoryCount))success failure:(void (^)(NSError *error))failure;
- (void)obtainCategory:(ESPCatalogCategory *)category updatedSince:(NSNumber *)msFromEpoch includeCount:(BOOL)flag filterFields:(NSArray *)fields success:(void (^)(NSArray *categoryResults, NSNumber *categoryCount))success failure:(void (^)(NSError *error))failure;
- (void)obtainCategory:(ESPCatalogCategory *)category updatedSince:(NSNumber *)msFromEpoch includeCount:(BOOL)flag ratings:(NSArray *)ratings success:(void (^)(NSArray *categoryResults, NSNumber *categoryCount))success failure:(void (^)(NSError *error))failure;
- (void)obtainCategory:(ESPCatalogCategory *)category updatedSince:(NSNumber *)msFromEpoch filterFields:(NSArray *)fields ratings:(NSArray *)ratings success:(void (^)(NSArray *categoryResults, NSNumber *categoryCount))success failure:(void (^)(NSError *error))failure;
- (void)obtainCategory:(ESPCatalogCategory *)category updatedSince:(NSNumber *)msFromEpoch filterFields:(NSArray *)fields lowerRange:(NSNumber *)lowerRange upperRange:(NSNumber *)upperRange ratings:(NSArray *)ratings success:(void (^)(NSArray *categoryResults, NSNumber *categoryCount))success failure:(void (^)(NSError *error))failure;
- (void)obtainCategory:(ESPCatalogCategory *)category updatedSince:(NSNumber *)msFromEpoch filterFields:(NSArray *)fields lowerRange:(NSNumber *)lowerRange upperRange:(NSNumber *)upperRange ratings:(NSArray *)ratings mediaFilter:(NSArray *)filter success:(void (^)(NSArray *categoryResults, NSNumber *categoryCount))success failure:(void (^)(NSError *error))failure;
- (void)obtainCategory:(ESPCatalogCategory *)category filterFields:(NSArray *)fields lowerRange:(NSNumber *)lowerRange upperRange:(NSNumber *)upperRange includeCount:(BOOL)flag ratings:(NSArray *)ratings mediaFilter:(NSArray *)filter updatedSince:(NSNumber *)msFromEpoch success:(void (^)(NSArray *media, NSNumber *categoryCount))success failure:(void (^)(NSError *error))failure;

// Types
- (void)obtainAllTypes:(void (^)(NSArray *result))success failure:(void (^)(NSError *))failure;
- (void)obtainAllMediaForType:(ESPCatalogType *)type success:(void (^)(NSArray *result))success failure:(void (^)(NSError *))failure;
- (void)obtainAllMediaForType:(ESPCatalogType *)type updatedSince:(NSNumber *)msFromEpoch success:(void (^)(NSArray *media))success failure:(void (^)(NSError *error))failure;
- (void)obtainAllMediaForType:(ESPCatalogType *)type updatedSince:(NSNumber *)msFromEpoch filterFields:(NSArray *)fields success:(void (^)(NSArray *media))success failure:(void (^)(NSError *error))failure;
- (void)obtainAllMediaForType:(ESPCatalogType *)type updatedSince:(NSNumber *)msFromEpoch filterFields:(NSArray *)fields ratings:(NSArray *)ratings success:(void (^)(NSArray *media))success failure:(void (^)(NSError *error))failure;
- (void)obtainAllMediaForType:(ESPCatalogType *)type filterFields:(NSArray *)fields lowerRange:(NSNumber *)lowerRange upperRange:(NSNumber *)upperRange ratings:(NSArray *)ratings mediaFilter:(NSArray *)filter updatedSince:(NSNumber *)msFromEpoch success:(void (^)(NSArray *media))success failure:(void (^)(NSError *error))failure;

// Convenience
- (void)obtainMetadataForMovie:(ESPMedia *)movie shouldIncludeAncillaryContent:(BOOL)flag success:(void (^)(ESPMedia *media))success failure:(void (^)(NSError *error))failure;
- (void)obtainMetadataForMovieGUID:(NSString *)guid category:(ESPCatalogCategory *)category shouldIncludeAncillaryContent:(BOOL)flag success:(void (^)(ESPMedia *media))success failure:(void (^)(NSError *error))failure;
- (void)obtainMetadataForMovieGUID:(NSString *)guid type:(ESPCatalogType *)type success:(void (^)(ESPMedia *media))success failure:(void (^)(NSError *))failure;
- (void)obtainTrailerForGUID:(NSString *)guid success:(void (^)(ESPMedia *media))success failure:(void (^)(NSError *error))failure;

// Search
- (void)query:(NSString *)queryString ratings:(NSArray *)ratings success:(void (^)(ESPSearchResult *searchResult))success failure:(void (^)(NSError *error))failure;

@end
