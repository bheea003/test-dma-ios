//
//  ESPCatalogType.h
//  
//
//  Created by Josh Paul on 7/9/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESPCatalogType : NSObject <NSCoding>
{
    NSString *descriptionText;
    NSString *title;
}

@property (nonatomic, copy) NSString *descriptionText;
@property (nonatomic, copy) NSString *title;

+ (ESPCatalogType *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
