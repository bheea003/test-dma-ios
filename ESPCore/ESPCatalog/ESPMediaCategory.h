//
//  ESPMediaCategory.h
//  
//
//  Created by Josh Paul on 4/24/13.
//  Copyright (c) 2013. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESPMediaCategory : NSObject <NSCoding>
{
    NSString *mediaLabel;
    NSString *mediaName;
    NSString *mediaScheme;
}

@property (nonatomic, copy) NSString *mediaLabel;
@property (nonatomic, copy) NSString *mediaName;
@property (nonatomic, copy) NSString *mediaScheme;

+ (ESPMediaCategory *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

- (NSString *)categoryName;
- (NSString *)urlFriendlyCategoryName;

@end
