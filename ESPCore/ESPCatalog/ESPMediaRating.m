//
//  ESPMediaRating.m
//  
//
//  Created by Josh Paul on 4/24/13.
//  Copyright (c) 2013. All rights reserved.
//

#import "ESPMediaRating.h"

@implementation ESPMediaRating

@synthesize rating;
@synthesize scheme;
@synthesize subRatings;

+ (ESPMediaRating *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    ESPMediaRating *instance = [[ESPMediaRating alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    [self setValuesForKeysWithDictionary:aDictionary];
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"subRatings"]) {
        if ([value isKindOfClass:[NSArray class]]) {
            NSMutableArray *myMembers = [NSMutableArray arrayWithCapacity:[value count]];
            for (id valueMember in value) {
                [myMembers addObject:valueMember];
            }
            self.subRatings = myMembers;
        }
    } else {
        [super setValue:value forKey:key];
    }
}


- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    if (self.rating) {
        [dictionary setObject:self.rating forKey:@"rating"];
    }
    if (self.scheme) {
        [dictionary setObject:self.scheme forKey:@"scheme"];
    }
    if (self.subRatings) {
        [dictionary setObject:self.subRatings forKey:@"subRatings"];
    }
    return dictionary;
}

//===========================================================
//  Keyed Archiving
//===========================================================
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.rating forKey:@"rating"];
    [encoder encodeObject:self.scheme forKey:@"scheme"];
    [encoder encodeObject:self.subRatings forKey:@"subRatings"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.rating = [decoder decodeObjectForKey:@"rating"];
        self.scheme = [decoder decodeObjectForKey:@"scheme"];
        self.subRatings = [decoder decodeObjectForKey:@"subRatings"];
    }
    return self;
}

@end
