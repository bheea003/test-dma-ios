//
//  ESPMediaContent.h
//  
//
//  Created by Josh Paul on 4/24/13.
//  Copyright (c) 2013. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESPMediaContent : NSObject <NSCoding>
{
    NSArray *fileAssetTypes;
    NSNumber *fileBitrate;
    NSString *fileContentType;
    NSNumber *fileDuration;
    NSNumber *fileHeight;
    NSString *fileLanguage;
    NSString *fileUrl;
    NSNumber *fileWidth;
    NSString *fileFormat;
    NSString *pid;
}

@property (nonatomic, copy) NSArray *fileAssetTypes;
@property (nonatomic, copy) NSNumber *fileBitrate;
@property (nonatomic, copy) NSString *fileContentType;
@property (nonatomic, copy) NSNumber *fileDuration;
@property (nonatomic, copy) NSNumber *fileHeight;
@property (nonatomic, copy) NSString *fileLanguage;
@property (nonatomic, copy) NSString *fileUrl;
@property (nonatomic, copy) NSNumber *fileWidth;
@property (nonatomic, copy) NSString *fileFormat;
@property (nonatomic, copy) NSString *pid;

+ (ESPMediaContent *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
