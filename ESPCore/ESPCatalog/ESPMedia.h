//
//  ESPMedia.h
//  
//
//  Created by Josh Paul on 4/24/13.
//  Copyright (c) 2013. All rights reserved.
//

#import <Foundation/Foundation.h>

// @class ESPMediaDescriptionLocalized;
// @class ESPMediaKeywordsLocalized;
// @class ESPMediaTitleLocalized;
@class ESPMediaContent;
@class ESPMediaThumbnail;

@interface ESPMedia : NSObject <NSCoding>

@property (nonatomic, copy, readonly) NSNumber *added;
@property (nonatomic, copy) NSString *descriptionText;
@property (nonatomic, copy) NSArray *bonusIDs;
@property (nonatomic, copy) NSString *contentType;
@property (nonatomic, assign, readonly) BOOL isFree;
@property (nonatomic, copy) NSString *longSynopsis;
@property (nonatomic, copy) NSString *mediumSynopsis;
@property (nonatomic, copy) NSString *mpmi;
@property (nonatomic, copy) NSString *mpmp;
@property (nonatomic, copy) NSNumber *runtimeMinutes;
@property (nonatomic, copy) NSArray *seoKeywords;
@property (nonatomic, copy) NSString *seoTitle;
@property (nonatomic, copy) NSString *sortTitle;
@property (nonatomic, copy) NSString *tagTitle;
@property (nonatomic, copy) NSString *theatricalReleaseDate;
@property (nonatomic, copy) NSString *titleRestrictions;
@property (nonatomic, copy) NSArray *trailerIDs;
@property (nonatomic, copy) NSString *umid;
@property (nonatomic, copy) NSString *umidHD;
@property (nonatomic, copy) NSString *umidHDPlus;
@property (nonatomic, copy) NSString *umidSD;
@property (nonatomic, copy) NSString *guid;
@property (nonatomic, copy) NSString *movieID;
@property (nonatomic, copy) NSArray *mediaCategories;
@property (nonatomic, copy) NSArray *mediaContent;
@property (nonatomic, copy) NSArray *mediaCredits;
@property (nonatomic, copy) NSString *mediaKeywords;
@property (nonatomic, copy) NSArray *mediaRatings;
@property (nonatomic, copy) NSArray *mediaThumbnails;
// @property (nonatomic, strong) ESPMediaDescriptionLocalized *mediaDescriptionLocalized;
// @property (nonatomic, strong) ESPMediaKeywordsLocalized *mediaKeywordsLocalized;
// @property (nonatomic, strong) ESPMediaTitleLocalized *mediaTitleLocalized;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy, readonly) NSNumber *updated;
@property (nonatomic, copy) NSString *restrictionsStartDate;
@property (nonatomic, copy) NSString *restrictionsEndDate;
@property (nonatomic, copy) NSArray *ancillaryContent;
@property (nonatomic, copy) NSNumber *entitlementLevel;

+ (ESPMedia *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSArray *)categories;
- (NSString *)ratingsAsString;
- (NSString *)castAsString;
- (NSString *)userPresentableReleaseDate;

- (NSString *)userPresentableDescription;
- (NSDictionary *)dictionaryRepresentation;

- (BOOL)isTrailer;
- (ESPMediaContent *)trailer;

- (BOOL)hasBeenReleased;

- (NSArray *)sortedAncillaryContent;

- (ESPMediaContent *)widevineContent;
- (ESPMediaContent *)unencryptedContent;
- (ESPMediaContent *)closedCaptionContent;

- (ESPMediaThumbnail *)posterThumbnail;
- (ESPMediaThumbnail *)posterThumbnail2x;
- (ESPMediaThumbnail *)smallThumbnail;
- (ESPMediaThumbnail *)mediumThumbnail;
- (ESPMediaThumbnail *)largeThumbnail;

- (NSString *)shortSocialMediaDescription;
- (NSString *)longSocialMediaDescription;

@end
