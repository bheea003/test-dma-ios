//
//  ESPSearchResult.h
//  
//
//  Created by Josh Paul on 7/17/13.
//  Copyright (c) 2013 Disney Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESPSearchResult : NSObject <NSCoding> {
    BOOL dirty;
    NSArray *searchResultItems;
}

@property (nonatomic, assign) BOOL dirty;
@property (nonatomic, copy) NSArray *searchResultItems;

+ (ESPSearchResult *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

- (NSDictionary *)dictionaryRepresentation;

@end
