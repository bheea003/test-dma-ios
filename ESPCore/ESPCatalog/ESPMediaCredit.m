//
//  ESPMediaCredit.m
//  
//
//  Created by Josh Paul on 4/24/13.
//  Copyright (c) 2013. All rights reserved.
//

#import "ESPMediaCredit.h"

@implementation ESPMediaCredit

@synthesize mediaRole;
@synthesize mediaScheme;
@synthesize mediaValue;

+ (ESPMediaCredit *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    ESPMediaCredit *instance = [[ESPMediaCredit alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    [self setValuesForKeysWithDictionary:aDictionary];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"media$role"]) {
        [self setValue:value forKey:@"mediaRole"];
    } else if ([key isEqualToString:@"media$scheme"]) {
        [self setValue:value forKey:@"mediaScheme"];
    } else if ([key isEqualToString:@"media$value"]) {
        [self setValue:value forKey:@"mediaValue"];
    } else {
        NSLog(@"%@ undefined key from API! %@ [%@]",[self class], key, value);
        // [super setValue:value forUndefinedKey:key];
    }
}


- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    if (self.mediaRole) {
        [dictionary setObject:self.mediaRole forKey:@"mediaRole"];
    }
    if (self.mediaScheme) {
        [dictionary setObject:self.mediaScheme forKey:@"mediaScheme"];
    }
    if (self.mediaValue) {
        [dictionary setObject:self.mediaValue forKey:@"mediaValue"];
    }

    return dictionary;
}

- (NSString *)fullName
{
    return [self mediaValue];
}

- (NSString *)role
{
    return [[self mediaRole] capitalizedString];
}

//===========================================================
//  Keyed Archiving
//===========================================================
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.mediaRole forKey:@"mediaRole"];
    [encoder encodeObject:self.mediaScheme forKey:@"mediaScheme"];
    [encoder encodeObject:self.mediaValue forKey:@"mediaValue"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.mediaRole = [decoder decodeObjectForKey:@"mediaRole"];
        self.mediaScheme = [decoder decodeObjectForKey:@"mediaScheme"];
        self.mediaValue = [decoder decodeObjectForKey:@"mediaValue"];
    }
    return self;
}

@end
