//
//  ESPNetworkActivityProtocol.h
//  ESPCore
//
//  Created by JP on 5/23/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ESPNetworkActivityProtocol <NSObject>

@property (assign, nonatomic) BOOL isLoading;

@end
