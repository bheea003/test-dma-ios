//
//  ESPEmbeddedIndexedCollectionViewTableViewCell.h
//  DMA
//
//  Created by JP on 7/2/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <UIKit/UIKit.h>

FOUNDATION_EXPORT NSString *const ESPEmbeddedIndexedCollectionViewTableViewCellIdentifier;

@class ESPIndexedCollectionView;
@interface ESPEmbeddedIndexedCollectionViewTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet ESPIndexedCollectionView *indexedCollectionView;

- (void)setCollectionViewDataSourceAndDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)delegate index:(NSInteger)index;

@end
