//
//  ESPCollectionViewController.h
//  ESPCore
//
//  Created by JP on 5/21/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESPViewController.h"
#import "ESPNetworkActivityProtocol.h"
#import "ESPTableProxy.h"

@interface ESPCollectionViewController : ESPViewController <UICollectionViewDataSource, UICollectionViewDelegate, ESPNetworkActivityProtocol, ESPTablePagerDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) NSMutableOrderedSet *collection;
@property (strong, nonatomic) ESPTableProxy *tablePager;

- (void)gatherMoreItemsFromPagerForIndexPath:(NSIndexPath *)indexPath;

@end
