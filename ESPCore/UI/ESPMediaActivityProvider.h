//
//  ESPMediaActivityProvider.h
//  DMA
//
//  Created by JP on 6/4/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

typedef enum {
    MediaSynopsisString,
    MediaPosterImage,
    MediaShareCount
} ESPMediaActivityItems;

@class ESPMedia;
@interface ESPMediaActivityProvider : UIActivityItemProvider

@property (strong, nonatomic) ESPMedia *media;

- (id)initWithMedia:(ESPMedia *)media;
- (NSArray *)placeholderItems;

@end
