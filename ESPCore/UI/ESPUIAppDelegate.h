//
//  ESPCoreAppDelegate.h
//  ESPCore
//
//  Created by JP on 5/2/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ESPReachability;
@interface ESPUIAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (NSURL *)applicationDocumentsDirectory;

// Reachability
- (void)startReachabilityForHost:(NSString *)hostDomain  reachable:(void (^)(ESPReachability *reach))success unreachable:(void (^)(ESPReachability *reach))failure;

// Identifiers
+ (NSString *)appIdentifier;
- (NSString *)uniqueDeviceIdentifier;

// KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context;

@end