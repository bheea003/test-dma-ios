//
//  ESPViewController.h
//  ESPCoreDemo
//
//  Created by JP on 5/9/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESPNetworkActivityProtocol.h"

@interface ESPViewController : UIViewController <ESPNetworkActivityProtocol>

@property (assign, nonatomic) BOOL isLoading;

/**
	@returns whether the app is running on a tablet
 */
- (BOOL)isTablet;

/**
	Displays a UIAlertView with the provided errorMessage
	@param errorMessage the message to display
 */
- (void)displayError:(NSString *)errorMessage;

/**
	Enables easy install/setting of a given font for the view and (by choice) its subviews
	@param fontFamily the font to install
	@param view the view (likely this viewController's)
	@param shouldApplyToSubviews whether to traverse the subviews and apply
 */
- (void)setFontFamily:(NSString*)fontFamily view:(UIView *)view shouldApplyToSubviews:(BOOL)shouldApplyToSubviews;

/**
	KVO helper; will call the supplied selector when the following template is used:
    [{object} addObserver:self forKeyPath:{aKeyPath} options:{NSKeyValueObservingOption} context:@selector({thisObjectSelector}:)];

	@param keyPath the keyPath being observer
	@param object the object
	@param change the change dictionary
	@param context the selector to use
 */
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context;

@end
