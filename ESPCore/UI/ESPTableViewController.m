//
//  ESPTableViewController.m
//  ESPCoreDemo
//
//  Created by JP on 5/9/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPTableViewController.h"
#import "MBProgressHUD.h"

@interface ESPTableViewController ()

@end

@implementation ESPTableViewController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([self.tablePager requestStatus] == RequestStatusInProgress) {
        self.isLoading = YES;
    }
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *TableCellIdentifier = @"TableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableCellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

#pragma mark - ESPTablePager
- (void)gatherMoreItemsFromPagerForIndexPath:(NSIndexPath *)indexPath
{
    BOOL isFetching = [self.tablePager requestStatus] == RequestStatusInProgress;
    if (! isFetching) {
        int fetchTrigger = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? 25 : 12;
        BOOL shouldFetch = (indexPath.row + fetchTrigger) >= [self.collection count];
        if  ((shouldFetch) && (! [self.tablePager didReachLastPage])) {
            [self.tablePager fetchNextPage];
        }
    }
}

- (void)pager:(id)pager didReceiveResults:(NSArray *)results
{
    self.collection = [self.tablePager results];
    
    NSMutableArray *indexPaths = [NSMutableArray array];
    NSInteger i = [self.collection count] - [results count];
    for (NSDictionary *result in results) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
        i++;
    }
    [self.tableView reloadData];
    // [self.tableView insertItemsAtIndexPaths:indexPaths];
    self.isLoading = NO;
}

- (void)pagerDidFail:(id)pager
{
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                message:NSLocalizedString(@"Unable to obtain Category data.", @"Unable to obtain Category data.")
                               delegate:self
                      cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                      otherButtonTitles:nil] show];
    [self.tablePager reset];
    self.isLoading = NO;
}

- (void)pagerDidReset:(id)pager
{
    self.collection = [self.tablePager results];
    [self.tableView reloadData];
    self.isLoading = NO;
}

@end
