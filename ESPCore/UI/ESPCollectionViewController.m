//
//  ESPCollectionViewController.m
//  ESPCore
//
//  Created by JP on 5/21/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPCollectionViewController.h"
#import "MBProgressHUD.h"

@interface ESPCollectionViewController ()

@end

@implementation ESPCollectionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.collection = [NSMutableOrderedSet orderedSet];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([self.tablePager requestStatus] == RequestStatusInProgress) {
        self.isLoading = YES;
    }
}
#pragma mark - ESPTablePager
- (void)gatherMoreItemsFromPagerForIndexPath:(NSIndexPath *)indexPath
{
    BOOL isFetching = [self.tablePager requestStatus] == RequestStatusInProgress;
    if (! isFetching) {
        int fetchTrigger = 14;
        BOOL shouldFetch = (indexPath.row + fetchTrigger) >= [self.collection count];
        if  ((shouldFetch) && (! [self.tablePager didReachLastPage])) {
            [self.tablePager fetchNextPage];
        }
    }
}

- (void)pager:(id)pager didReceiveResults:(NSArray *)results
{
    NSMutableArray *indexPaths = [NSMutableArray array];
    NSInteger i = [self.collection count] - [results count];
    for (NSDictionary *result in results) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
        i++;
    }
    
    [self.collection addObjectsFromArray:results];
    
    // BUG: UICollectionView animations are "funky" when reusing cells
    [UIView setAnimationsEnabled:NO];
        // BUG: Inserting into a collection view at index 0 when using Headers/Footers causes crash
        // RADAR: http://openradar.appspot.com/12954582
        if ([[self.collectionView indexPathsForVisibleItems] count] != 0) {
            [self.collectionView insertItemsAtIndexPaths:indexPaths];
        } else {
            [self.collectionView reloadData];
        }
        // END BUG
    [UIView setAnimationsEnabled:YES];
    // END BUG

    self.isLoading = NO;
}

- (void)pagerDidFail:(id)pager
{
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                message:NSLocalizedString(@"Unable to obtain Category data.", @"Unable to obtain Category data.")
                               delegate:self
                      cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                      otherButtonTitles:nil] show];
    [pager reset];
}

- (void)pagerDidReset:(id)pager
{
    self.collection = [self.tablePager results];
    [self.collectionView reloadData];
    self.isLoading = NO;
}

#pragma mark - UICollectionView
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    return [self.collection count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *CollectionIdentifier = @"CollectionCell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CollectionIdentifier forIndexPath:indexPath];

    // configure cell...
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CollectionFooterIdentifier = @"CollectionFooterCell";
    UICollectionReusableView *reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:CollectionFooterIdentifier forIndexPath:indexPath];

    // configure cell...
    
    return reusableview;
}

@end
