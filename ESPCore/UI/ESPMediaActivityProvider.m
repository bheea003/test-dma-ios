//
//  ESPMediaActivityProvider.m
//  DMA
//
//  Created by JP on 6/4/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPMediaActivityProvider.h"
#import "ESPMedia.h"
#import "ESPMediaThumbnail.h"

@interface ESPMediaActivityProvider ()

@end

@implementation ESPMediaActivityProvider

- (id)initWithMedia:(ESPMedia *)media
{
    self = [super init];
    if (self) {
        self.media = media;
    }
    return self;
}

- (id)activityViewController:(UIActivityViewController *)activityViewController itemForActivityType:(NSString *)activityType
{
    BOOL isShortService = [activityType isEqualToString:UIActivityTypePostToTwitter] || [activityType isEqualToString:UIActivityTypeMessage];
    
    static UIActivityViewController *shareController;
    static int index;
    if ((shareController == activityViewController) && (index < MediaShareCount)) {
        index++;
    } else {
        index = 0;
        shareController = activityViewController;
    }
    switch (index) {
        case 0: return (isShortService) ? [self.media shortSocialMediaDescription] : [self.media longSocialMediaDescription];
        case 1: return [self poster];
        default: return nil;
    }
}

- (id)activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController
{
    static UIActivityViewController *shareController;
    static int index;
    if ((shareController == activityViewController) && (index < MediaShareCount)) {
        index++;
    } else {
        index = 0;
        shareController = activityViewController;
    }

    switch (index) {
        case 0: return @"";
        case 1: return [UIImage new];
        default: return nil;
    }
}

#pragma mark - Convenience
- (NSArray *)placeholderItems
{
    return @[self, self];
}

- (UIImage *)poster
{
    NSString *imageURL = [[self.media posterThumbnail2x] fileUrl];
    NSURL *url = [NSURL URLWithString:imageURL];
    NSData *imageData = [NSData dataWithContentsOfURL:url];
    return [UIImage imageWithData:imageData];
}

@end
