//
//  ESPViewController.m
//  ESPCoreDemo
//
//  Created by JP on 5/9/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPViewController.h"
#import "ESPMediaActivityProvider.h"
#import "ESPMedia.h"
#import "ESPMediaThumbnail.h"
#import "MBProgressHUD.h"

@interface ESPViewController ()

@end

@implementation ESPViewController

- (void)setIsLoading:(BOOL)isLoading
{
    if (_isLoading != isLoading) {
        _isLoading = isLoading;
        if (_isLoading) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.labelText = NSLocalizedString(@"Loading...", @"Loading...");
        } else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
    }
}

#pragma mark - iPhone/iPad
- (BOOL)isTablet
{
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
}

#pragma mark - Error Display
- (void)displayError:(NSString *)errorMessage
{
    self.isLoading = NO;

    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                message:errorMessage
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                      otherButtonTitles:nil]
     show];
}

#pragma mark - Appearance
- (void)setFontFamily:(NSString*)fontFamily view:(UIView *)view shouldApplyToSubviews:(BOOL)shouldApplyToSubviews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if (shouldApplyToSubviews)
    {
        for (UIView *subview in view.subviews)
        {
            [self setFontFamily:fontFamily view:subview shouldApplyToSubviews:YES];
        }
    }
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self performSelector:(SEL)context withObject:object withObject:change];
#pragma clang diagnostic pop
}

@end
