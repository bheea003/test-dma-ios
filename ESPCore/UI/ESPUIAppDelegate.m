//
//  ESPCoreAppDelegate.m
//  ESPCore
//
//  Created by JP on 5/2/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPUIAppDelegate.h"
#import "ESPReachability.h"
#import "ESPKeychainItemWrapper.h"
#import "ESPConvenience.h"

@implementation ESPUIAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // [ESPCore shouldEnableNetworkActivityIndicator:YES];
    [self setupCache];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
}

#pragma mark - Cache
- (void)setupCache
{
    int memoryMB = 4;
    int memoryCache = memoryMB * 1024 * 1024;
    int diskMB = 32;
    int diskCache = diskMB * 1024 * 1024;
    NSURLCache *URLCache = [[NSURLCache alloc] initWithMemoryCapacity:memoryCache diskCapacity:diskCache diskPath:@"sharedCache"];
    [NSURLCache setSharedURLCache:URLCache];
}

#pragma mark - Application's Documents directory
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Reachability
- (void)startReachabilityForHost:(NSString *)hostDomain reachable:(void (^)(ESPReachability *reach))success unreachable:(void (^)(ESPReachability *reach))failure
{
	ESPReachability *reach = [ESPReachability reachabilityWithHostname:hostDomain];
	reach.reachableBlock = success;
	reach.unreachableBlock = failure;
	[reach startNotifier];
}

#pragma mark - UUID replacement
+ (NSString *)appIdentifier
{
    NSDictionary *query = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)kSecClassGenericPassword, kSecClass, @"bundleSeedID", kSecAttrAccount, @"", kSecAttrService, (id)kCFBooleanTrue, kSecReturnAttributes, nil];
    CFDictionaryRef result = nil;
    OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)query, (CFTypeRef *)&result);
    if (status == errSecItemNotFound) {
        status = SecItemAdd((__bridge CFDictionaryRef)query, (CFTypeRef *)&result);
    }
    if (status != errSecSuccess) {
        return nil;
    }
    NSString *accessGroup = [[(__bridge NSDictionary *)result objectForKey:(__bridge id)kSecAttrAccessGroup] copy];
    CFRelease(result);
    return accessGroup;
}

- (NSString *)uniqueDeviceIdentifier
{
    NSString *appIdentifier = [ESPUIAppDelegate appIdentifier]; // @"G58WL7FR8E.com.disney.GenericKeychainSuite";
    ESPKeychainItemWrapper *wrapper = [[ESPKeychainItemWrapper alloc] initWithIdentifier:@"Device" accessGroup:appIdentifier];
    NSString *uniqueDeviceIdentifier = [wrapper objectForKey:(__bridge id)kSecAttrAccount];
    if (IsEmpty(uniqueDeviceIdentifier)) {
        ESPLog(@"uniqueDeviceIdentifier is empty...creating");
        uniqueDeviceIdentifier = [[NSProcessInfo processInfo] globallyUniqueString];
        [wrapper setObject:uniqueDeviceIdentifier forKey:(__bridge id)kSecAttrAccount];
    }
    ESPLog(@"uniqueDeviceIdentifier is %@", uniqueDeviceIdentifier);

    return uniqueDeviceIdentifier;
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self performSelector:(SEL)context withObject:object withObject:change];
#pragma clang diagnostic pop
}

@end
