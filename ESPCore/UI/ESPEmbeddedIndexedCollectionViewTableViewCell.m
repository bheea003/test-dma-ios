//
//  ESPEmbeddedIndexedCollectionViewTableViewCell.m
//  DMA
//
//  Created by JP on 7/2/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPEmbeddedIndexedCollectionViewTableViewCell.h"
#import "ESPIndexedCollectionView.h"

NSString *const ESPEmbeddedIndexedCollectionViewTableViewCellIdentifier = @"ESPEmbeddedIndexedCollectionViewTableViewCellIdentifier";

@implementation ESPEmbeddedIndexedCollectionViewTableViewCell

- (void)setCollectionViewDataSourceAndDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)delegate index:(NSInteger)index
{
    self.indexedCollectionView.dataSource = delegate;
    self.indexedCollectionView.delegate = delegate;
    self.indexedCollectionView.index = index;
    
    [self.indexedCollectionView reloadData];
}

@end
