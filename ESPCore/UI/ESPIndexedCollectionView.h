//
//  ESPIndexedCollectionView.h
//  DMA
//
//  Created by JP on 7/2/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ESPIndexedCollectionView : UICollectionView

@property (assign, nonatomic) NSInteger index;

@end
