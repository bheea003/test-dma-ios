//
//  ESPTableViewController.h
//  ESPCoreDemo
//
//  Created by JP on 5/9/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESPViewController.h"
#import "ESPNetworkActivityProtocol.h"
#import "ESPTableProxy.h"

@interface ESPTableViewController : ESPViewController <UITableViewDataSource, UITableViewDelegate, ESPNetworkActivityProtocol, ESPTablePagerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSOrderedSet *collection;
@property (strong, nonatomic) ESPTableProxy *tablePager;

- (void)gatherMoreItemsFromPagerForIndexPath:(NSIndexPath *)indexPath;

@end
