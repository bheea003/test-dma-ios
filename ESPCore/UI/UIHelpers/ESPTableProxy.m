//
//  ESPTablePager.m
//  ESPCoreDemo
//
//  Created by JP on 5/3/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPTableProxy.h"

@interface ESPTableProxy()
    @property (assign, readwrite) NSInteger resultsPerPage;
    @property (assign, readwrite) NSInteger currentPage;
    @property (assign, readwrite) NSInteger totalResults;
    @property (nonatomic, strong, readwrite) NSMutableOrderedSet *results;
    @property (assign, readwrite) ESPTablePageRequestStatus requestStatus;
@end

@implementation ESPTableProxy

- (id)initWithPageSize:(NSInteger)pageSize delegate:(id<ESPTablePagerDelegate>)delegate
{
    if (self = [super init])
    {
        [self setDefaultValues];
        self.resultsPerPage = pageSize;
        self.delegate = delegate;
    }
    
    return self;
}

- (void)setDefaultValues
{
    self.totalResults = 0;
    self.currentPage = 0;
    self.results = [NSMutableOrderedSet orderedSet];
    self.requestStatus = RequestStatusNone;
}

- (void)fetchResultsWithPage:(NSInteger)page pageSize:(NSInteger)pageSize
{
    // override this in subclass
}

- (void)receivedResults:(NSArray *)results total:(NSInteger)total
{    
    [self.results addObjectsFromArray:results];
    self.currentPage++;
    self.totalResults = total;
    self.requestStatus = RequestStatusComplete;
    
    [self.delegate pager:self didReceiveResults:results];
}

#pragma mark - Convenience
- (NSInteger)totalNumberOfPages
{
    NSInteger totalPages = ceil((float)self.totalResults/(float)self.resultsPerPage);
    return totalPages;
}

- (void)fetchFirstPage
{
    [self reset];
    [self fetchNextPage];
}

- (void)fetchNextPage
{
    if (self.requestStatus == RequestStatusInProgress) {
        return;
    }
    
    if (! [self didReachLastPage]) {
        self.requestStatus = RequestStatusInProgress;
        [self fetchResultsWithPage:self.currentPage+1 pageSize:self.resultsPerPage];
    }
}

- (BOOL)didReachLastPage
{
    if (self.requestStatus == RequestStatusNone) {
        return NO;
    }
    
    return self.currentPage >= [self totalNumberOfPages];
}

- (void)failed
{
    self.requestStatus = RequestStatusComplete;
    [self.delegate pagerDidFail:self];
}

- (void)reset
{
    [self setDefaultValues];
    [self.delegate pagerDidReset:self];
}

@end
