//
//  ESPMediaTableProxy.m
//  ESPCoreDemo
//
//  Created by JP on 5/6/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPMediaTableProxy.h"
#import "ESPConvenience.h"
#import "ESPCatalog.h"
#import "ESPCatalogCategory.h"

@implementation ESPMediaTableProxy

- (void)fetchResultsWithPage:(NSInteger)page pageSize:(NSInteger)pageSize
{
    if (! self.catalog) {
        ESPLog(@"Unable to fetch catalog, as it's nil. Please set the catalog before attempting to fetch.");
        [self.delegate pagerDidFail:self];
    }
    
    int indexPage = page - 1;
    
    NSNumber *lowerRange = [NSNumber numberWithInt:(indexPage * pageSize)+1];
    NSNumber *upperRange = [NSNumber numberWithInt:(lowerRange.intValue-1 + pageSize)];    
    [[self catalog] obtainCategory:self.category filterFields:nil lowerRange:lowerRange upperRange:upperRange includeCount:YES ratings:self.ratingsLimit mediaFilter:nil updatedSince:nil success:^(NSArray *movies, NSNumber *categoryCount) {
        [self receivedResults:movies total:[categoryCount intValue]];
    } failure:^(NSError *error) {
        [self.delegate pagerDidFail:self];
    }];
}

@end
