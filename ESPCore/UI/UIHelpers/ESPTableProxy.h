//
//  ESPTablePager.h
//  ESPCoreDemo
//
//  Created by JP on 5/3/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    RequestStatusNone,
    RequestStatusInProgress,
    RequestStatusComplete
} ESPTablePageRequestStatus;

@protocol ESPTablePagerDelegate
- (void)pager:(id)paginator didReceiveResults:(NSArray *)results;
- (void)pagerDidFail:(id)pager;
- (void)pagerDidReset:(id)pager;
@end


@interface ESPTableProxy : NSObject

@property (weak) id<ESPTablePagerDelegate> delegate;
@property (assign, readonly) NSInteger resultsPerPage;
@property (assign, readonly) NSInteger currentPage;
@property (assign, readonly) NSInteger totalResults;
@property (assign, readonly) ESPTablePageRequestStatus requestStatus;
@property (nonatomic, strong, readonly) NSMutableOrderedSet *results;

#pragma mark - Override in subclasses -
- (void)fetchResultsWithPage:(NSInteger)page pageSize:(NSInteger)pageSize;

#pragma mark - Convenience methods -
- (id)initWithPageSize:(NSInteger)pageSize delegate:(id<ESPTablePagerDelegate>)delegate;

- (void)reset;
- (void)fetchFirstPage;
- (void)fetchNextPage;
- (BOOL)didReachLastPage;

- (void)receivedResults:(NSArray *)results total:(NSInteger)total;
- (void)failed;

@end
