//
//  ESPMediaTableProxy.h
//  ESPCoreDemo
//
//  Created by JP on 5/6/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPTableProxy.h"

@class ESPCatalog;
@class ESPCatalogCategory;
@interface ESPMediaTableProxy : ESPTableProxy

@property (strong, nonatomic) ESPCatalog *catalog;
@property (strong, nonatomic) ESPCatalogCategory *category;
@property (strong, nonatomic) NSArray *ratingsLimit; // i.e. G, PG, PG-13

@end
