//
//  ESPCollectionFlowLayout.m
//  DMA
//
//  Created by JP on 6/5/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//  Solution from: http://stackoverflow.com/questions/14506139/weird-animation-behaviour-with-uicollectionview-insert-delete-item

#import "ESPCollectionFlowLayout.h"

@interface ESPCollectionFlowLayout()

@property (strong, nonatomic) NSMutableArray *deleteIndexPaths;
@property (strong, nonatomic) NSMutableArray *insertIndexPaths;
@property (assign, nonatomic) float rowOffset;

@end

@implementation ESPCollectionFlowLayout

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        // minimumInteritemSpacing may be adjusted upwards but this example ignores that
        self.rowOffset = self.itemSize.height + self.minimumInteritemSpacing;
    }
    return self;
}

- (void)prepareForCollectionViewUpdates:(NSArray *)updateItems
{
    // As per Mark Pospesel corrections to CircleLayout
    // Keep track of insert and delete index paths
    [super prepareForCollectionViewUpdates:updateItems];
    
    self.deleteIndexPaths = [NSMutableArray array];
    self.insertIndexPaths = [NSMutableArray array];
    
    for (UICollectionViewUpdateItem *update in updateItems)
    {
        if (update.updateAction == UICollectionUpdateActionDelete)
        {
            [self.deleteIndexPaths addObject:update.indexPathBeforeUpdate];
        }
        else if (update.updateAction == UICollectionUpdateActionInsert)
        {
            [self.insertIndexPaths addObject:update.indexPathAfterUpdate];
        }
    }
}

- (void)finalizeCollectionViewUpdates
{
    [super finalizeCollectionViewUpdates];
    // release the insert and delete index paths
    self.deleteIndexPaths = nil;
    self.insertIndexPaths = nil;
}

// The next two methods have misleading names as they get called for all visible cells on both insert and delete
- (UICollectionViewLayoutAttributes *)initialLayoutAttributesForAppearingItemAtIndexPath:(NSIndexPath *)itemIndexPath
{
    // Must call super
    UICollectionViewLayoutAttributes *attributes = [super initialLayoutAttributesForAppearingItemAtIndexPath:itemIndexPath];
    if (!attributes)
        attributes = [self layoutAttributesForItemAtIndexPath:itemIndexPath];
    
    if ([self.insertIndexPaths containsObject:itemIndexPath]) {
        // Initial position for an inserted cell is it's final position - fades in
        CGRect frame = attributes.frame;
        frame.origin.y = itemIndexPath.row * self.rowOffset;
        attributes.frame = frame;
        attributes.zIndex = -1; // stop the inserted cell bleeding through too early in the animation
    }
    if ([self.deleteIndexPaths count]) {
        NSIndexPath *deletedPath = self.deleteIndexPaths[0];  // Might be more than one but this example ignores that
        if (itemIndexPath.row > deletedPath.row) {
            // Anything after the deleted cell needs to slide up from the position below it's final position
            // Anything before the deleted cell doesn't need adjusting
            CGRect frame = attributes.frame;
            frame.origin.y = ((itemIndexPath.row + 1) * self.rowOffset);
            attributes.frame = frame;
        }
    }
    
    return attributes;
}

- (UICollectionViewLayoutAttributes *)finalLayoutAttributesForDisappearingItemAtIndexPath:(NSIndexPath *)itemIndexPath
{
    UICollectionViewLayoutAttributes *attributes = [super finalLayoutAttributesForDisappearingItemAtIndexPath:itemIndexPath];
    if (!attributes)
        attributes = [self layoutAttributesForItemAtIndexPath:itemIndexPath];
    
    // I would have expected the final positions to already be correct but my guess is that re-used cells
    // are not considered until after the animation block settings have been generated
    CGRect frame = attributes.frame;
    frame.origin.y = itemIndexPath.row * self.rowOffset;
    attributes.frame = frame;
    
    if ([self.deleteIndexPaths containsObject:itemIndexPath]) {
        // Fade out the deleted cell
        attributes.alpha = 0.0;
    }
    
    return attributes;
}

@end
