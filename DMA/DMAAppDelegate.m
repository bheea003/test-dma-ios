//
//  DMAAppDelegate.m
//  DMA
//
//  Created by JP on 5/23/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMAAppDelegate.h"
#import "DMACommonProxy.h"
#import "DMASettingsConstants.h"

@interface DMAAppDelegate()
@property (assign, nonatomic) BOOL hasSetupKVO;
@end

@implementation DMAAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{    
    [self customizeAppearance];
    [self synchronizeUserDefaults];
    
    NSString *currentLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
    [[DMACommonProxy sharedProxy] setApplicationLanguage:currentLanguage];
    
    [self setupKVO];

    return [super application:application didFinishLaunchingWithOptions:launchOptions];
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // TODO: something?
    [super applicationWillResignActive:application];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [self teardownKVO];

    [super applicationDidEnterBackground:application];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [super applicationWillEnterForeground:application];
    
    [self setupKVO];
    [[DMACommonProxy sharedProxy] validateAccess];
    [[DMACommonProxy sharedProxy] validateUpgradeStatus];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [super applicationDidBecomeActive:application];
    
    [self synchronizeUserDefaults];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [self teardownKVO];

    [super applicationWillTerminate:application];
}

#pragma mark - Convenience
- (UITabBarController *)tabBarController
{
    return (UITabBarController *)self.window.rootViewController;
}

- (UITabBar *)tabBar
{
    return [[self tabBarController] tabBar];
}

#pragma mark - KVO
- (void)setupKVO
{
    if (! self.hasSetupKVO) {
        self.hasSetupKVO = YES;
        [[DMACommonProxy sharedProxy] addObserver:self forKeyPath:@"clientUpgrade" options:NSKeyValueObservingOptionNew context:@selector(upgradeStatusChanged:)];
        [[DMACommonProxy sharedProxy] addObserver:self forKeyPath:@"hasValidAccess" options:NSKeyValueObservingOptionNew context:@selector(accessValidationChanged:)];
    }
}

- (void)teardownKVO
{
    if (self.hasSetupKVO) {
        self.hasSetupKVO = NO;
        [[DMACommonProxy sharedProxy] removeObserver:self forKeyPath:@"clientUpgrade"];
        [[DMACommonProxy sharedProxy] removeObserver:self forKeyPath:@"hasValidAccess"];
    }
}

- (void)upgradeStatusChanged:(id)change
{
    NSString *upgradeStatusDescription = nil;
    switch ([[DMACommonProxy sharedProxy] upgradeStatus]) {
        case UpgradeAvailable:
            upgradeStatusDescription = NSLocalizedString(@"Available", @"Available");
            break;
        case UpgradeRequired:
            upgradeStatusDescription = NSLocalizedString(@"Required", @"Required");
            break;
        case UpgradeNotApplicable:
            upgradeStatusDescription = NSLocalizedString(@"Not Applicable", @"Not Applicable");
            break;
        case UpgradeUnsupported:
            upgradeStatusDescription = NSLocalizedString(@"Not Supported", @"Not Supported");
            break;
        case UpgradeError:
            upgradeStatusDescription = NSLocalizedString(@"Error", @"Error");
            break;
        default:
            break;
    }
    ESPLog(@"what to do? %@", upgradeStatusDescription);
}

- (void)accessValidationChanged:(id)change
{
    if (! [[DMACommonProxy sharedProxy] hasValidAccess]) {
        ESPLog(@"what to do? app shouldn't be accessible.");
    }
}

#pragma mark - Appearance
- (void)customizeAppearance
{
    [self customizeNavigationBar];
    [self customizeTabBar];
}

- (void)customizeNavigationBar
{
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                          [UIColor whiteColor], UITextAttributeTextColor,
                                                          [UIFont fontWithName:@"MatterhornSerif-Bold" size:20.0], UITextAttributeFont,
                                                          nil]];
    [[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                          [UIColor whiteColor], UITextAttributeTextColor,
                                                          [UIFont fontWithName:@"MatterhornSerif-Bold" size:14.0], UITextAttributeFont,
                                                          nil] forState:UIControlStateNormal];    
}

- (void)customizeTabBar
{
    [[UITabBar appearance] setTintColor:[UIColor clearColor]];
    [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"tab_selection_indicator"]];
    [[UITabBar appearance] setBackgroundImage:[UIImage imageNamed:@"bottom_nav_bar"]];
    
    UIImage *primaryImage = [UIImage imageNamed:@"btn_my_collections_inactive"];
    UIImage *highlightImage = [UIImage imageNamed:@"btn_my_collections_active"];
    int numberOfTabs = [self.tabBar.items count];
    int middleTab = numberOfTabs/2; // yeah, it'll round :-)
    UITabBarItem *centerTab = [self.tabBar.items objectAtIndex:middleTab];
    [centerTab setFinishedSelectedImage:highlightImage withFinishedUnselectedImage:primaryImage];
}

#pragma mark - UserDefaults
- (void)synchronizeUserDefaults
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *defaultValues = @[[NSNumber numberWithBool:YES], [NSNumber numberWithBool:NO], [NSNumber numberWithInt:RatingsG_PG_PG13], @"coolMovies", @"US", @"EN"];
    NSArray *defaultKeys = @[StreamingEnabledKey, EmailEnabledKey, RatingsFilterKey, ApplicationNameKey, ApplicationCountryKey, ApplicationLanguageKey];
    NSDictionary *appDefaults = [NSDictionary dictionaryWithObjects:defaultValues forKeys:defaultKeys];
    [defaults registerDefaults:appDefaults];
    [defaults synchronize];
}

- (NSArray *)ratingsFilter
{
    NSNumber *ratingsLimits = [[NSUserDefaults standardUserDefaults] valueForKey:RatingsFilterKey];
    switch ([ratingsLimits intValue]) {
        case RatingsG:
            return [NSArray arrayWithObjects:@"G", nil];
        case RatingsG_PG:
            return [NSArray arrayWithObjects:@"G", @"PG", nil];
        case RatingsG_PG_PG13:
        default:
            return nil;
    }
}

- (BOOL)isStreamingEnabled
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:StreamingEnabledKey];
}

- (BOOL)isEmailOptIn
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:EmailEnabledKey];
}

@end
