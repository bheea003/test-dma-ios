//
//  DMAMoviesWithHeroViewController.h
//  DMA
//
//  Created by JP on 7/10/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMAMoviesByCategoryCollectionViewController.h"
#import "DMAHeroView.h"

@interface DMAMoviesWithHeroViewController : DMAMoviesByCategoryCollectionViewController <UIScrollViewDelegate, DMAHeroViewDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *masterScrollView;
@property (strong, nonatomic) IBOutlet UIView *heroContainer;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UIScrollView *heroScrollView;

@end
