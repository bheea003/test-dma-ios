//
//  DMACollectionViewController.h
//  DMA
//
//  Created by JP on 5/23/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DMACommonProxy.h"
#import "DMAMediaCollectionViewCell.h"

#import "ESPConvenience.h"
#import "ESPCollectionViewController.h"

#define DELAY_REFRESH_FOR 0

FOUNDATION_EXPORT NSString *const CollectionFooterID;

@interface DMACollectionViewController : ESPCollectionViewController

@property (strong, nonatomic) NSDate *refreshedAt;

- (DMACommonProxy *)proxy;
- (BOOL)shouldRefresh;

// KVO callbacks
- (BOOL)defaultsChanged:(NSNotification *)notification;
- (void)authorizationChanged:(id)change;
- (void)ownedMoviesChaged:(id)change;

@end
