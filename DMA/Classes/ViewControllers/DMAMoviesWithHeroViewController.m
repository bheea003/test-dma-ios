//
//  DMAMoviesWithHeroViewController.m
//  DMA
//
//  Created by JP on 7/10/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMAMoviesWithHeroViewController.h"
#import "DMACommonProxy.h"
#import "ESPConvenience.h"
#import "ESPCatalogCategory.h"
#import "DMAHeroView.h"

#import <QuartzCore/QuartzCore.h>

@interface DMAMoviesWithHeroViewController ()
@property (strong, nonatomic) NSArray *heroMovies;
@property (strong, nonatomic) DMAHeroView *currentHero;
@property (strong, nonatomic) DMAHeroView *nextHero;
@end

@implementation DMAMoviesWithHeroViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (! self.heroMovies) {
        [self.heroScrollView setPagingEnabled:YES];
        [self.heroScrollView setShowsHorizontalScrollIndicator:NO];
        [self configureHeroMovies];
    }
        
    [self.masterScrollView setContentSize:CGSizeMake(self.tableView.frame.size.width, self.tableView.frame.origin.y + self.tableView.frame.size.height)];
}

#pragma mark - IBActions
- (void)heroView:(DMAHeroView *)heroView didSelectAction:(DMAHeroAction)heroState button:(UIButton *)button
{
    self.isLoading = YES;
    
    ESPMedia *movie = [self.heroMovies objectAtIndex:heroView.pageIndex];
    switch (heroState) {
        case DMAHeroFavorite:
            if ([[self proxy] isFavorite:movie]) {
                [[self proxy] removeFromFavorites:movie];
                [[heroView favoriteButton] setSelected:NO];
            } else {
                [[self proxy] addToFavorites:movie];
                [[heroView favoriteButton] setSelected:YES];
            }
            break;
        case DMAHeroShare:
            [self shareMovie:movie fromRect:button.frame];
            break;
        case DMAHeroDetails:
            [self presentMovieDetails:movie];
            break;
        case DMAHeroDone:
            break;
        case DMAHeroPlayMovie:
            [self playMedia:movie];
            break;
        case DMAHeroPlayTrailer:
            [self.videoPlayer playTrailer:movie catalog:[self catalog]];
            break;
        case DMAHeroPurchase:
            break;
        case DMAHeroShowtimes:
            break;
        default:
            break;
    }
}

#pragma mark - Movie Detail Presentation
- (void)presentMovieDetails:(ESPMedia *)movie
{
    [[self catalog] obtainMetadataForMovieGUID:[movie guid] category:[ESPCatalogCategory allCategory] shouldIncludeAncillaryContent:YES success:^(ESPMedia *media) {
        self.selectedMedia = media;
        [self performSegueWithIdentifier:@"MovieDetailSelectedSegue" sender:self];
        self.isLoading = NO;
    } failure:^(NSError *error) {
        self.isLoading = NO;
    }];
}

#pragma mark - Hero
- (void)configureHeroMovies
{
    ESPCatalog *catalog = [[self proxy] catalog];
    [catalog obtainCategoryForTitle:@"Disney-Pixar" success:^(NSArray *result) {
        self.heroMovies = result;
        self.pageControl.numberOfPages = [self.heroMovies count];
        self.pageControl.currentPage = 0;
        [self reloadHeroScrollView];
    } failure:^(NSError *error) {
        ESPLog(@"Categories error: %@", error);
    }];
}

- (void)reloadHeroScrollView
{
    self.currentHero = [[DMAHeroView alloc] initWithFrame:self.heroScrollView.frame];
    self.currentHero.delegate = self;
    self.currentHero.shareButton.hidden = YES;
    self.currentHero.favoriteButton.hidden = YES;
	[self.heroScrollView addSubview:self.currentHero];
	self.nextHero = [[DMAHeroView alloc] initWithFrame:self.heroScrollView.frame];
    self.nextHero.delegate = self;
    self.nextHero.shareButton.hidden = YES;
    self.nextHero.favoriteButton.hidden = YES;
	[self.heroScrollView addSubview:self.nextHero];
                                             
    NSInteger widthCount = [self.heroMovies count];
	if (widthCount == 0) {
		widthCount = 1;
	}
    self.heroScrollView.contentSize = CGSizeMake(self.heroScrollView.frame.size.width * widthCount, self.heroScrollView.frame.size.height);
	self.heroScrollView.contentOffset = CGPointMake(0, 0);
    
	self.pageControl.numberOfPages = [self.heroMovies count];
	self.pageControl.currentPage = 0;
	
	[self applyNewIndex:0 pageController:self.currentHero];
	[self applyNewIndex:1 pageController:self.nextHero];
}

- (void)applyNewIndex:(NSInteger)newIndex pageController:(DMAHeroView *)heroView
{
	NSInteger pageCount = [self.heroMovies count];
	BOOL outOfBounds = newIndex >= pageCount || newIndex < 0;
	if (! outOfBounds) {
		CGRect pageFrame = heroView.frame;
		pageFrame.origin.y = 0;
		pageFrame.origin.x = self.heroScrollView.frame.size.width * newIndex;
		heroView.frame = pageFrame;
        
        ESPMedia *media = [self.heroMovies objectAtIndex:newIndex];
        BOOL hasEntitlement = [[self proxy] hasEntitlementToMedia:media];
        [heroView configureViewForMovie:media isPreview:YES hasEntitlement:hasEntitlement];
	} else {
		CGRect pageFrame = heroView.frame;
		pageFrame.origin.y = self.heroScrollView.frame.size.height;
		heroView.frame = pageFrame;
	}
	heroView.pageIndex = newIndex;
}

#pragma mark - UIScrollView delegate (Hero)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.heroScrollView) {
        CGFloat pageWidth = self.heroScrollView.frame.size.width;
        float fractionalPage = self.heroScrollView.contentOffset.x / pageWidth;
        
        NSInteger lowerNumber = floor(fractionalPage);
        NSInteger upperNumber = lowerNumber + 1;
        
        if (lowerNumber == self.currentHero.pageIndex) {
            if (upperNumber != self.nextHero.pageIndex) {
                [self applyNewIndex:upperNumber pageController:self.nextHero];
            }
        } else if (upperNumber == self.currentHero.pageIndex) {
            if (lowerNumber != self.nextHero.pageIndex) {
                [self applyNewIndex:lowerNumber pageController:self.nextHero];
            }
        } else {
            if (lowerNumber == self.nextHero.pageIndex) {
                [self applyNewIndex:upperNumber pageController:self.currentHero];
            } else if (upperNumber == self.nextHero.pageIndex) {
                [self applyNewIndex:lowerNumber pageController:self.currentHero];
            } else {
                [self applyNewIndex:lowerNumber pageController:self.currentHero];
                [self applyNewIndex:upperNumber pageController:self.nextHero];
            }
        }
    } else if (scrollView == self.masterScrollView) {
        CGRect frame = [self.heroContainer frame];
        
        float y = [scrollView contentOffset].y;
        if (y > 0) {
            [self blurView:self.heroContainer];
        } else {
            [self unblurView:self.heroContainer];
        }
        self.heroContainer.frame = CGRectMake(0, y, frame.size.width, frame.size.height);
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if (scrollView == self.heroScrollView) {
        CGFloat pageWidth = self.heroScrollView.frame.size.width;
        float fractionalPage = self.heroScrollView.contentOffset.x / pageWidth;
        NSInteger nearestNumber = lround(fractionalPage);
        
        if (self.currentHero.pageIndex != nearestNumber) {
            DMAHeroView *swap = self.currentHero;
            self.currentHero = self.nextHero;
            self.nextHero = swap;
        }
    } else if (scrollView == self.masterScrollView) {

    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.heroScrollView) {
        [self scrollViewDidEndScrollingAnimation:scrollView];
        self.pageControl.currentPage = self.currentHero.pageIndex;
    } else if (scrollView == self.masterScrollView) {
        
    }    
}

#pragma mark - Blur
- (void)blurView:(UIView *)view
{
    CALayer *layer = [view layer];
    [layer setRasterizationScale:0.0275];
    [layer setShouldRasterize:YES];

}

- (void)unblurView:(UIView *)view
{
    CALayer *layer = [view layer];
    [layer setRasterizationScale:1.0];
    [layer setShouldRasterize:NO];
}
    
@end
