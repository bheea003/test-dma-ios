//
//  DMASettingsViewController.m
//  DMA
//
//  Created by JP on 6/6/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMASettingsViewController.h"
#import "DMASettingsConstants.h"

@implementation DMASettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureNavBar];
    [self configureTableView];
    
    //if (! DEBUG) {
    //    [self hideDevelopmentTools];
    //}
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [super viewWillDisappear:animated];
}

#pragma mark - UI config
- (void)configureNavBar
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        UIBarButtonItem *logo = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Disney_logo_navigation.png"]]];
        self.navigationController.navigationBar.topItem.leftBarButtonItem = logo;
    }
}

- (void)configureTableView
{
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)hideDevelopmentTools
{
    NSSet *hiddenKeys = [NSSet setWithArray:@[ApplicationNameKey, ApplicationCountryKey, ApplicationLanguageKey]];
    [self setHiddenKeys:hiddenKeys];
}

@end
