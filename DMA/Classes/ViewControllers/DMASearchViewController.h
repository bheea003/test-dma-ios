//
//  DMASearchViewController.h
//  DMA
//
//  Created by JP on 7/15/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMATableViewController.h"

@interface DMASearchViewController : DMATableViewController <UISearchBarDelegate>

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@end
