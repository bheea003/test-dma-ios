//
//  DMATableViewController.h
//  DMA
//
//  Created by JP on 5/23/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPTableViewController.h"

@class DMACommonProxy;
@class ESPCatalog;
@class ESPConsumer;
@interface DMATableViewController : ESPTableViewController

- (DMACommonProxy *)proxy;
- (ESPCatalog *)catalog;
- (ESPConsumer *)consumer;

@end
