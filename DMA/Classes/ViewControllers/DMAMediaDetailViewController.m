//
//  DMAMediaDetailViewController.m
//  DMA
//
//  Created by JP on 5/8/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMAMediaDetailViewController.h"
#import "DMAAppDelegate.h"
#import "DMACommonProxy.h"
#import "DMAAppleAtomProxy.h"
#import "DMAMediaCollectionViewCell.h"
#import "DMAMediaCreditCollectionViewCell.h"
#import "ESPConvenience.h"
#import "ESPConsumer.h"
#import "ESPMedia.h"
#import "ESPMediaThumbnail.h"
#import "ESPMediaCredit.h"
#import "ESPMediaContent.h"
#import "ESPMediaCategory.h"
#import "ESPCatalogCategory.h"
#import "ESPMediaActivityProvider.h"
#import "UIImageView+AFNetworking.h"

#define BONUS_HEADER_TAG 9

@interface DMAMediaDetailViewController()
@property (assign, nonatomic) BOOL isGatheringBonusMaterial;
@property (strong, nonatomic) DMAHeroView *heroView;
@property (assign, nonatomic) BOOL isBonusMaterialVisible;
@property (strong, nonatomic) NSArray *bonusArray;
@property (strong, nonatomic) ESPMedia *selectedBonus;
@end

@implementation DMAMediaDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UINib *bonuslNib = [UINib nibWithNibName:@"DMAMediaCollectionViewCell" bundle:nil];
    [self.bonusContent registerNib:bonuslNib forCellWithReuseIdentifier:MediaCollectionCellID];
    [self.bonusContent registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"BonusCollectionHeaderID"];
    
    UINib *creditNib = [UINib nibWithNibName:@"DMAMediaCreditCollectionViewCell" bundle:nil];
    [self.movieCreditsCollection registerNib:creditNib forCellWithReuseIdentifier:MediaCreditCollectionCellID];
    
    self.bonusContainer.alpha = 0;
    [self configureHeroView];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self configureView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateMasterScrollView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.heroView configureViewForMovie:self.movie isPreview:NO hasEntitlement:[self hasEntitlement]];
    self.heroView.delegate = self;
    self.isLoading = NO;

    self.backgroundView = nil;
}

- (void)viewDidDisappear:(BOOL)animated
{
    self.isLoading = NO;
    [self.masterScrollView setContentOffset:CGPointZero];

    [super viewDidDisappear:animated];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"SuggestedMovieDetailsSegue"]) {
        DMAMediaDetailViewController *viewController = [segue destinationViewController];
        viewController.movie = self.selectedBonus;
        viewController.hidesBottomBarWhenPushed = YES;
    }
}

#pragma mark - Convenience
- (BOOL)hasEntitlement
{
    return [[self proxy] hasEntitlementToMedia:self.movie];
}

- (ESPMedia *)contentForIndexPath:(NSIndexPath *)indexPath
{
    int row = indexPath.row;
    if (row < [self.bonusArray count]) {
        return [self.bonusArray objectAtIndex:row];
    }
    return nil;
}

- (ESPMediaCredit *)creditForIndexPath:(NSIndexPath *)indexPath
{
    int row = indexPath.row;
    NSArray *credits = [self.movie mediaCredits];
    if (row < [credits count]) {
        return [credits objectAtIndex:row];
    }
    return nil;
}

#pragma mark - IBActions
- (void)heroView:(DMAHeroView *)heroView didSelectAction:(DMAHeroAction)heroState button:(UIButton *)button
{
    switch (heroState) {
        case DMAHeroFavorite:
            if ([[[self proxy] favorites] containsObject:self.movie]) {
                [[self proxy] removeFromFavorites:self.movie];
                [[heroView favoriteButton] setSelected:NO];
            } else {
                [[self proxy] addToFavorites:self.movie];
                [[heroView favoriteButton] setSelected:YES];
            }
            break;
        case DMAHeroShare:
            [self shareMovie:self.movie fromRect:button.frame];
            break;
        case DMAHeroDetails:
            break;
        case DMAHeroDone:
            break;
        case DMAHeroPlayMovie:
            [self playFeature];
            break;
        case DMAHeroPlayTrailer:
            [self.videoPlayer playTrailer:self.movie catalog:[self catalog]];
            break;
        case DMAHeroPurchase:
            [self buyFeature];
            break;
        case DMAHeroShowtimes:
            break;
        default:
            break;
    }
}

- (IBAction)toggleBonuses:(id)sender
{
    int padding = 44;
    CGRect bonusFrame = self.bonusContainer.frame;
    CGRect posterFrame = self.heroViewContainer.frame;
    [UIView animateWithDuration:0.25 animations:^{
        int x = 0;
        if (self.isBonusMaterialVisible) {
            x = self.view.bounds.size.width - padding;
            self.isBonusMaterialVisible = NO;
        } else {
            x = posterFrame.size.width/5 - padding;
            self.isBonusMaterialVisible = YES;
        }
        self.bonusContainer.frame = CGRectMake(x, 0, bonusFrame.size.width, bonusFrame.size.height);
    }];
}

- (IBAction)closeAction:(id)sender
{
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
}

- (IBAction)watchOrBuyAction:(id)sender
{
    BOOL hasEntitlement = [[self proxy] hasEntitlementToMedia:self.movie];
    if (hasEntitlement) {
        [self playFeature];
    } else {
        [self buyFeature];
    }
}

- (IBAction)share:(id)sender
{
    if ([sender isKindOfClass:[UIView class]]) {
        [self shareMovie:self.movie fromRect:[sender frame]];
    } else {
        [self shareMovie:self.movie fromRect:CGRectZero];
    }
}

#pragma mark - Video Playback
- (void)playFeature
{
    self.isLoading = YES;
    ESPAuthorization *authorzation = [[self proxy] authorization];
    [[self videoPlayer] playFeature:self.movie consumer:[self consumer] authorization:authorzation];
}

#pragma mark - VideoPlayer Delegate
- (void)videoPlayer:(ESPVideoPlayer *)videoPlayer didChangeState:(NSNumber *)mpMoviePlayerStateAsNumber
{
    self.isLoading = NO;
}

- (void)videoPlayer:(ESPVideoPlayer *)videoPlayer didEncounterError:(NSError *)error
{
    self.isLoading = NO;
    NSString *errorString = [error.userInfo valueForKey:@"reason"];
    if (IsEmpty(errorString)) {
        errorString = [error localizedDescription];
    }
    [self displayError:errorString];
}

#pragma mark - Video Purchase
- (void)buyFeature
{
    self.isLoading = YES;
    
    int identifier = [[DMAAppleAtomProxy sharedProxy] appleAtomForTitle:[self.movie title]];
    if (identifier > 0) {
        SKStoreProductViewController *storeViewController = [[SKStoreProductViewController alloc] init];
        storeViewController.delegate = self;
        
        NSDictionary *parameters = @{SKStoreProductParameterITunesItemIdentifier:[NSNumber numberWithInteger:identifier]};
        [storeViewController loadProductWithParameters:parameters completionBlock:^(BOOL result, NSError *error) {
            if (result) {
                [self presentViewController:storeViewController animated:YES completion:nil];
            }
            self.isLoading = NO;
        }];
    } else {
        self.isLoading = NO;
        NSString *errorString = NSLocalizedString(@"We're sorry. '%@' isn't available for purchase at this time.", @"We're sorry. '{movie title}' isn't available for purchase at this time.");
        NSString *displayError = [NSString stringWithFormat:errorString, [self.movie title]];
        [self displayError:displayError];
    }
}

- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

#pragma mark - UI configuration
- (void)updateMasterScrollView
{
    if ([self.bonusArray count]) {
        [self.masterScrollView setContentSize:CGSizeMake(self.bonusContainer.frame.size.width, self.bonusContainer.frame.origin.y + self.bonusContainer.frame.size.height)];
    } else {
        [self.masterScrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
    }
}

- (void)configureHeroView
{
    self.heroView = [[DMAHeroView alloc] initWithFrame:self.heroViewContainer.frame];
    [self.heroView configureViewForMovie:self.movie isPreview:NO hasEntitlement:NO];
    self.heroView.delegate = self;
    [self.heroViewContainer addSubview:self.heroView];
}

- (void)configureView
{
    BOOL isFavorite = [[[self proxy] favorites] containsObject:self.movie];
    [[self.heroView favoriteButton] setSelected:isFavorite];
    
    self.movieMetadata.text = [NSString stringWithFormat:@"%@   %@ ", [self.movie ratingsAsString], [self.movie userPresentableReleaseDate]];
    [self configureSynopsis];
    [self configureCredits];
    [self gatherBonusMaterial];
}

- (void)configureSynopsis
{
    self.movieSynopsis.text = [self.movie longSynopsis];
    [self.movieSynopsis sizeToFit];
}

- (void)configureCredits
{
    if ([[self.movie mediaCredits] count]) {
        self.castAndCreditsLabel.hidden = NO;
        self.cast.hidden = NO;
        self.cast.text = [self.movie castAsString];
        /*
            CGRect synopsisFrame = [self.movieSynopsis frame];
            self.castAndCreditsLabel.frame = CGRectMake(synopsisFrame.origin.x, synopsisFrame.origin.y + synopsisFrame.size.height + 10, self.castAndCreditsLabel.frame.size.width, self.castAndCreditsLabel.frame.size.height);
            
            int creditsX = self.castAndCreditsLabel.frame.origin.x + 10;
            int creditsY = self.castAndCreditsLabel.frame.origin.y + 20;
            int height =  self.view.frame.size.height - creditsY;
            self.movieCreditsCollection.frame = CGRectMake(creditsX, creditsY, self.movieCreditsCollection.frame.size.width, height);
        */
    } else {
        self.castAndCreditsLabel.hidden = YES;
        self.cast.hidden = YES;
    }
}

- (void)gatherBonusMaterial
{
    if ([self.bonusArray count] > 0) {
        [self showBonusContent];
        return;
    } else if (! self.isGatheringBonusMaterial) {
        if ([self hasEntitlement]) {
            [self gatherEntitledBonusMaterial];
        } else {
            [self gatherSuggestedMaterial];
        }
    }
}

- (void)gatherSuggestedMaterial
{
    self.bonusContainer.alpha = 0;
    self.isGatheringBonusMaterial = YES;

    ESPMediaCategory *category = [[self.movie categories] lastObject];
    ESPCatalogCategory *catalogCategory = [[ESPCatalogCategory alloc] init];
    catalogCategory.urlTitle = [category urlFriendlyCategoryName];
    catalogCategory.userTitle = [category urlFriendlyCategoryName];
    [[self catalog] obtainCategory:catalogCategory updatedSince:nil filterFields:[ESPCatalog guidTitleAndThumbnailsOnlyFilter] ratings:[APP_DELEGATE ratingsFilter] success:^(NSArray *categoryResults, NSNumber *categoryCount) {
        self.bonusArray = categoryResults;
        [self showBonusContent];
        self.isGatheringBonusMaterial = NO;
    } failure:^(NSError *error) {
        self.isGatheringBonusMaterial = NO;
        [self displayError:NSLocalizedString(@"Unable to gather Suggested Material.", @"Unable to gather Suggested Material")];
        ESPLog(@"gatherSuggestedMaterial Error: %@", error);
    }];
}

- (void)gatherEntitledBonusMaterial
{    
    self.bonusContainer.alpha = 0;
    self.isGatheringBonusMaterial = YES;
    
    [[[self proxy] catalog] obtainMetadataForMovie:self.movie shouldIncludeAncillaryContent:YES success:^(ESPMedia *media) {
        self.bonusArray = [media ancillaryContent];
        [self.movie setAncillaryContent:self.bonusArray];
        [self showBonusContent];
        self.isGatheringBonusMaterial = NO;
    } failure:^(NSError *error) {
        self.isGatheringBonusMaterial = NO;
        [self displayError:NSLocalizedString(@"Unable to gather Bonus Material.", @"Unable to gather Bonus Material")];
        ESPLog(@"gatherEntitledBonusMaterial Error: %@", error);
    }];
}

- (void)showBonusContent
{
    if ([self.bonusArray count]) {
        [UIView animateWithDuration:0.25 animations:^{
            [self.bonusContent reloadData];
            self.bonusContainer.alpha = 1;
        }];
    } else {
        self.bonusContainer.alpha = 0;
    }
    [self updateMasterScrollView];
}

#pragma mark - UICollectionView
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    if (view == self.bonusContent) {
        return [self bonusCollectionView:view numberOfItemsInSection:section];
    } else {
        return [self creditsCollectionView:view numberOfItemsInSection:section];
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    if (collectionView == self.bonusContent) {
        return [self bonusCollectionView:collectionView cellForItemAtIndexPath:indexPath];
    } else {
        return [self creditsCollectionView:collectionView cellForItemAtIndexPath:indexPath];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.bonusContent) {
        return [self bonusCollectionView:collectionView didSelectItemAtIndexPath:indexPath];
    } else {
        return [self creditsCollectionView:collectionView didSelectItemAtIndexPath:indexPath];
    }
}

#pragma mark - Bonus CollectionView
- (NSInteger)bonusCollectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    return [self.bonusArray count];
}

- (UICollectionViewCell *)bonusCollectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    DMAMediaCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MediaCollectionCellID forIndexPath:indexPath];
    id object = [self contentForIndexPath:indexPath];
    BOOL isMovie = [object isKindOfClass:[ESPMedia class]];
    NSString *text = nil;
    if (isMovie) {
        text = [(ESPMedia *)object title];
        ESPMediaThumbnail *thumbnail = [(ESPMedia *)object posterThumbnail2x];
        if (thumbnail) {
            NSURL *imageURL = [NSURL URLWithString:[thumbnail fileUrl]];
            [cell.posterImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        }
        /*
            BOOL shouldEnable = ([object isTrailer]) || ([[self.movie entitlementLevel] intValue] > 0);
            cell.disabledOverlay.hidden = shouldEnable;
        */
        [cell setBannerType:BannerTypeNone]; // Bonus' shouldn't display banners
    } else {
        text = [object description];
        [cell.posterImageView setImage:[UIImage imageNamed:@"placeholder.png"]];
    }
    cell.titleLabel.text = text;
    return cell;
}

- (void)bonusCollectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.isLoading = YES;
    
    if ([self hasEntitlement]) {
        [self presentAncillaryContentForIndexPath:indexPath];
    } else {
        [self presentMovieDetailsForIndexPath:indexPath];
    }
}

- (void)presentMovieDetailsForIndexPath:(NSIndexPath *)indexPath
{
    ESPMedia *movie = [self contentForIndexPath:indexPath];
    [[self catalog] obtainMetadataForMovieGUID:[movie guid] category:[ESPCatalogCategory allCategory] shouldIncludeAncillaryContent:YES success:^(ESPMedia *media) {
        self.selectedBonus = media;
        [self performSegueWithIdentifier:@"SuggestedMovieDetailsSegue" sender:self];
        self.isLoading = NO;
    } failure:^(NSError *error) {
        self.isLoading = NO;
    }];
}

- (void)presentAncillaryContentForIndexPath:(NSIndexPath *)indexPath
{
    ESPMedia *movie = [self contentForIndexPath:indexPath];
    self.selectedBonus = movie;
    if ([movie isTrailer]) {
        [[self videoPlayer] playTrailer:movie catalog:[self catalog]];
    } else {
        int entitlementLevel = [[self.movie entitlementLevel] intValue];
        if (entitlementLevel > 0) {
            ESPAuthorization *authorzation = [[self proxy] authorization];
            [[self videoPlayer] playFeature:movie consumer:[self consumer] authorization:authorzation];
        } else {
            [self displayError:NSLocalizedString(@"Bonus Features are only available with purchase.", @"Bonus Features are only available with purchase.")];
            self.isLoading = NO;
        }
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (collectionView == self.bonusContent && kind == UICollectionElementKindSectionHeader) {
        reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"BonusCollectionHeaderID" forIndexPath:indexPath];
        NSString *bonusTitle = [self hasEntitlement] ? NSLocalizedString(@"Bonus Content", @"Bonus Content") : NSLocalizedString(@"More Like This", @"More Like This");
        [(UILabel *)[reusableview viewWithTag:BONUS_HEADER_TAG] setText:bonusTitle];
    }
    return reusableview;
}

#pragma mark - Credits CollectionView
- (NSInteger)creditsCollectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    return [[self.movie mediaCredits] count];
}

- (UICollectionViewCell *)creditsCollectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    DMAMediaCreditCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MediaCreditCollectionCellID forIndexPath:indexPath];
    id object = [self creditForIndexPath:indexPath];
    BOOL isCredit = [object isKindOfClass:[ESPMediaCredit class]];
    if (isCredit) {
        cell.mediaRole.text = [object role];
        cell.mediaFullName.text = [object fullName];
    } else {
        cell.mediaRole.text = [object description];
        cell.mediaFullName.text = nil;
    }
    return cell;
}

- (void)creditsCollectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return;
}

@end
