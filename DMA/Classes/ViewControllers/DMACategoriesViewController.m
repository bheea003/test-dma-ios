//
//  DMACategoriesViewController.m
//  DMA
//
//  Created by JP on 5/23/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMACategoriesViewController.h"
#import "DMACommonProxy.h"
#import "ESPCatalogCategory.h"

NSString *CategoryTableCell = @"CategoryTableCell";

@implementation DMACategoriesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView setTableFooterView:[UIView new]];
    [self setupAndFetchCategories];
}

#pragma mark - Convenience
- (void)setupAndFetchCategories
{
    self.isLoading = YES;
    [[self proxy] obtainCategoriesMinus:nil success:^(NSArray *categories) {
        self.collection = [NSOrderedSet orderedSetWithArray:categories];
        [self.tableView reloadData];
        self.isLoading = NO;
    } failure:^(NSError *error) {
        // TODO: display error
        self.isLoading = NO;
    }];
}

#pragma mark - TableView datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.collection count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CategoryTableCell forIndexPath:indexPath];
    ESPCatalogCategory *category = [self.collection objectAtIndex:indexPath.row];
    [cell.textLabel setText:[category userTitle]];
    
    if (! IsEmpty(self.selectedCategory)) {
        BOOL isSelected =  [category isEqual:self.selectedCategory];
        cell.accessoryType = isSelected ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    }
    
    return cell;
}

#pragma mark - TableView delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedCategory = [self.collection objectAtIndex:indexPath.row];
    [self.tableView reloadData];
}

@end
