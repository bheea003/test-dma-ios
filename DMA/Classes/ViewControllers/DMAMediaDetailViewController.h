//
//  DMAMediaDetailViewController.h
//  DMA
//
//  Created by JP on 5/8/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
#import "DMAViewController.h"
#import "DMAHeroView.h"
#import "ESPVideoPlayer.h"

@class ESPMedia;
@interface DMAMediaDetailViewController : DMAViewController <UICollectionViewDataSource, UICollectionViewDelegate, SKStoreProductViewControllerDelegate, ESPVideoPlayerDelegate, DMAHeroViewDelegate>

@property (strong, nonatomic) ESPMedia *movie;
@property (strong, nonatomic) ESPVideoPlayer *videoPlayer;

@property (strong, nonatomic) IBOutlet UIView *heroViewContainer;
@property (strong, nonatomic) IBOutlet UIScrollView *masterScrollView;

@property (strong, nonatomic) IBOutlet UILabel *movieMetadata;
@property (strong, nonatomic) IBOutlet UILabel *movieSynopsisLabel;
@property (strong, nonatomic) IBOutlet UITextView *movieSynopsis;
@property (strong, nonatomic) IBOutlet UILabel *castAndCreditsLabel;
@property (strong, nonatomic) IBOutlet UITextView *cast;
@property (strong, nonatomic) IBOutlet UICollectionView *movieCreditsCollection;

@property (strong, nonatomic) IBOutlet UIView *bonusContainer;
@property (strong, nonatomic) IBOutlet UILabel *bonusTitleLabel;
@property (strong, nonatomic) IBOutlet UICollectionView *bonusContent;

- (IBAction)toggleBonuses:(id)sender;
- (IBAction)closeAction:(id)sender;

@end
