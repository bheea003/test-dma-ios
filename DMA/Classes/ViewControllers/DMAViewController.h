//
//  DMAViewController.h
//  DMA
//
//  Created by JP on 5/23/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPViewController.h"
#import "DMACommonProxy.h"
#import "ESPVideoPlayer.h"
#import "ESPCatalog.h"
#import "ESPConsumer.h"
#import "ESPMedia.h"
#import "DMAVideoControlView.h"

@interface DMAViewController : ESPViewController <ESPVideoPlayerDelegate, DMAVideoControlDelegate>

@property (strong, nonatomic) id sharePopover; // only used on iPad
@property (strong, nonatomic) IBOutlet UIImageView *backgroundView;
@property (strong, nonatomic) ESPVideoPlayer *videoPlayer;

- (DMACommonProxy *)proxy;
- (ESPCatalog *)catalog;
- (ESPConsumer *)consumer;

- (void)showMovieDetails:(ESPMedia *)movie;
- (void)shareMovie:(ESPMedia *)movie fromRect:(CGRect)rect;

- (void)playMedia:(ESPMedia *)media;

@end
