//
//  DMASearchViewController.m
//  DMA
//
//  Created by JP on 7/15/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMASearchViewController.h"
#import "ESPCatalog.h"
#import "ESPCatalogCategory.h"
#import "ESPSearchResult.h"
#import "ESPSearchResultItem.h"
#import "DMAMediaDetailViewController.h"

@interface DMASearchViewController ()
@property (strong, nonatomic) ESPMedia *selectedMedia;
@end

@implementation DMASearchViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.searchBar becomeFirstResponder];
    [self.tableView setTableFooterView:[UIView new]];
    
    // ensure we keep the Cancel button enabled, as it disables when the keyboard is dismissed
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardHidden:) name:UIKeyboardDidHideNotification object:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    DMAMediaDetailViewController *viewController = [segue destinationViewController];
    viewController.movie = self.selectedMedia;
    viewController.hidesBottomBarWhenPushed = YES;
}


#pragma mark - Keyboard Notification
- (void)keyboardHidden:(NSNotification *)notification
{
    // HACK!
    for (UIView *possibleButton in [self.searchBar subviews]) { 
        if ([possibleButton isKindOfClass:[UIButton class]]) {
            [(UIButton *)possibleButton setEnabled:YES];
        }
    }
}

#pragma mark - Search
- (void)search:(NSString *)query
{
    [[self catalog] query:query ratings:nil success:^(ESPSearchResult *searchResult) {
        self.collection = [NSOrderedSet orderedSetWithArray:[searchResult searchResultItems]];
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        //
    }];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.collection count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *TableCellIdentifier = @"SearchResultTableCellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableCellIdentifier forIndexPath:indexPath];
    
    ESPSearchResultItem *item = [[self collection] objectAtIndex:indexPath.row];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.text = [item title];
    
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.searchBar resignFirstResponder];
    [self.view endEditing:YES];
    
    self.isLoading = YES;
    if (indexPath.row < [self.collection count]) {
        self.selectedMedia = [self.collection objectAtIndex:indexPath.row];
        [[self catalog] obtainMetadataForMovieGUID:[self.selectedMedia guid] category:[ESPCatalogCategory allCategory] shouldIncludeAncillaryContent:YES success:^(ESPMedia *media) {
            self.selectedMedia = media;
            [self performSegueWithIdentifier:@"SearchSelectedSegue" sender:self];
            self.isLoading = NO;
        } failure:^(NSError *error) {
            self.isLoading = NO;
        }];
    }
}

#pragma mark - UISearchBar delegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText length] > 2) {
        [self search:searchText];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    // called when keyboard search button pressed
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    [self dismissViewControllerAnimated:YES completion:^{
        self.selectedMedia = nil;
    }];
}

@end
