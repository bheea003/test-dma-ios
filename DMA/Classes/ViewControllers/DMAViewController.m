//
//  DMAViewController.m
//  DMA
//
//  Created by JP on 5/23/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMAViewController.h"
#import "DMAMediaDetailViewController.h"
#import "ESPMediaActivityProvider.h"

@interface DMAViewController()
@property (strong, nonatomic) DMAVideoControlView *videoController;
@end


@implementation DMAViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setFontFamily:@"MatterhornSerif-Bold" view:self.view shouldApplyToSubviews:YES];
}

#pragma mark - API convenience
- (DMACommonProxy *)proxy
{
    return [DMACommonProxy sharedProxy];
}

- (ESPCatalog *)catalog
{
    return [[self proxy] catalog];
}

- (ESPConsumer *)consumer
{
    return [[self proxy] consumer];
}

#pragma mark - MovieDetails
- (void)showMovieDetails:(ESPMedia *)movie
{
    self.isLoading = YES;
    
    [[self catalog] obtainMetadataForMovie:movie shouldIncludeAncillaryContent:YES success:^(ESPMedia *media) {
        NSString *nibName = ([self isTablet]) ? @"DMAMediaDetailViewController_iPad" : @"DMAMediaDetailViewController_iPhone";
        DMAMediaDetailViewController *movieDetailViewController = [[DMAMediaDetailViewController alloc] initWithNibName:nibName bundle:nil];
        movieDetailViewController.modalPresentationStyle = UIModalPresentationFormSheet;
        [movie setAncillaryContent:[media ancillaryContent]];
        movieDetailViewController.movie = movie;
        self.isLoading = NO;
        [self presentViewController:movieDetailViewController animated:YES completion:NULL];
    } failure:^(NSError *error) {
        self.isLoading = NO;
        [self displayError:[error description]];
    }];
}

- (void)shareMovie:(ESPMedia *)movie fromRect:(CGRect)rectForPopover
{
    ESPMediaActivityProvider *sharingProvider = [[ESPMediaActivityProvider alloc] initWithMedia:movie];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:[sharingProvider placeholderItems] applicationActivities:nil];
    activityViewController.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll, UIActivityTypePostToWeibo];
    activityViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    if ([self isTablet]) {
        if (rectForPopover.size.width > 0 && rectForPopover.size.height > 0) {
            self.sharePopover = [[UIPopoverController alloc] initWithContentViewController:activityViewController];
            [self.sharePopover presentPopoverFromRect:rectForPopover inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            return;
        }
    }
    
    [self presentViewController:activityViewController animated:YES completion:nil];
}

#pragma mark - Video Playback
- (void)playMedia:(ESPMedia *)media
{
    self.isLoading = YES;
    BOOL isEntitled = [[self proxy] hasEntitlementToMedia:media];
    if (isEntitled) {
        ESPAuthorization *authorzation = [[self proxy] authorization];
        [[self videoPlayer] playFeature:media consumer:[self consumer] authorization:authorzation];
    } else {
        [[self videoPlayer] playTrailer:media catalog:[self catalog]];
    }
}

- (ESPVideoPlayer *)videoPlayer
{
    if (! _videoPlayer) {
        _videoPlayer = [[ESPVideoPlayer alloc] init];
        _videoPlayer.delegate = self;
    }
    return _videoPlayer;
}

#pragma mark - VideoPlayer Delegate
- (DMAVideoControlView *)videoController
{
    if (! _videoController) {
        NSArray *videoControlNib = [[NSBundle mainBundle] loadNibNamed:@"DMAVideoControlView" owner:self options:nil];
        _videoController = [videoControlNib lastObject];
        _videoController.delegate = self;
    }
    return _videoController;
}

- (void)videoPlayer:(ESPVideoPlayer *)videoPlayer didChangeState:(NSNumber *)mpMoviePlayerStateAsNumber
{
    // if (self.isLoading) {
    //    self.isLoading = NO;
    [[videoPlayer moviePlayer] setControlStyle:MPMovieControlStyleNone];
    [[videoPlayer moviePlayerView] addSubview:self.videoController];
    // } else {
        self.isLoading = NO;
    //  }
}

- (void)videoPlayer:(ESPVideoPlayer *)videoPlayer didEncounterError:(NSError *)error
{
    self.isLoading = NO;
    NSString *errorString = [error.userInfo valueForKey:@"reason"];
    if (IsEmpty(errorString)) {
        errorString = [error localizedDescription];
    }
    [self displayError:errorString];
}

#pragma makr - DMAVideoControlDelegate
- (void)control:(DMAVideoControlView *)control wantsJumpBack:(id)sender
{
    NSTimeInterval currentTime = [[self.videoPlayer moviePlayer] currentPlaybackTime];
    NSTimeInterval requestedTime = currentTime - 10; // 10 second jump back
    [[self.videoPlayer moviePlayer] setCurrentPlaybackTime:requestedTime];
}

- (void)control:(DMAVideoControlView *)control beginRewind:(id)sender
{
    [[self.videoPlayer moviePlayer] beginSeekingBackward];
}

- (void)control:(DMAVideoControlView *)control endRewind:(id)sender
{
    [[self.videoPlayer moviePlayer] endSeeking];
}

- (void)control:(DMAVideoControlView *)control wantsTogglePlay:(id)sender
{
    if ([[self.videoPlayer moviePlayer] playbackState] == MPMoviePlaybackStatePlaying) {
        [[self.videoPlayer moviePlayer] pause];
        [control.togglePlayButton setTitle:NSLocalizedString(@"Play", @"Play") forState:UIControlStateNormal];
    } else {
        [[self.videoPlayer moviePlayer] play];
        [control.togglePlayButton setTitle:NSLocalizedString(@"Pause", @"Pause") forState:UIControlStateNormal];
    }
}

- (void)control:(DMAVideoControlView *)control beginFastForward:(id)sender
{
    [[self.videoPlayer moviePlayer] beginSeekingForward];
}

- (void)control:(DMAVideoControlView *)control endFastForward:(id)sender
{
    [[self.videoPlayer moviePlayer] endSeeking];
}

- (void)control:(DMAVideoControlView *)control wantsBookmark:(id)sender
{
    [self displayError:@"Feature Forthcoming"];
}

@end
