//
//  DMAFirstViewController.m
//  DMA
//
//  Created by JP on 5/23/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMAMoviesCollectionViewController.h"
#import "DMAAppDelegate.h"
#import "DMACategoriesViewController.h"
#import "DMAMediaCollectionViewCell.h"
#import "DMAMediaDetailViewController.h"

#import "ESPCatalogCategory.h"
#import "ESPMediaTableProxy.h"
#import "ESPMediaThumbnail.h"

#import "UIImageView+AFNetworking.h"
#import "NSString+ESP.h"

@interface DMAMoviesCollectionViewController ()
@end

@implementation DMAMoviesCollectionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UINib *cellNib = [UINib nibWithNibName:@"DMAMediaCollectionViewCell" bundle:nil];
    [self.featuredCollection registerNib:cellNib forCellWithReuseIdentifier:MediaCollectionCellID];
    
    self.categoryButton.title = NSLocalizedString(@"Category", @"Category");
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self gatherMoviesForCategory:self.selectedCategory];
    [self gatherFeaturedMovies];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"SelectedCategoriesSegue"]) {
        if ([self isTablet]) {
            UIStoryboardPopoverSegue *popoverSegue = (UIStoryboardPopoverSegue *)segue;
            self.popover = popoverSegue.popoverController;
            [self.popover setDelegate:self];
            [self displayCategoriesPopover];
        } else {
            DMACategoriesViewController *viewController = [segue destinationViewController];
            [viewController setSelectedCategory:self.selectedCategory];
            [[viewController tableView] reloadData];
            [viewController addObserver:self forKeyPath:@"selectedCategory" options:NSKeyValueObservingOptionNew context:@selector(feedChanged:)];
        }
    } else if ([[segue identifier] contains_esp:@"SelectedMovieSegue"]) {
        DMAMediaDetailViewController *viewController = [segue destinationViewController];
        viewController.movie = self.selectedMedia;
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[[self proxy] catalog] cancelAllHTTPOperations];
    
    [super viewDidDisappear:animated];
}

#pragma mark - Convenience
- (NSString *)topTitle
{
    if ([self isTablet]) {
        NSString *category = [self.selectedCategory userTitle];
        if ([category hasSuffix:@"ies"]) {
            category = [category stringByReplacingOccurrencesOfString:@"ies" withString:@"y" options:NSCaseInsensitiveSearch range:NSMakeRange(category.length-3, 3)];
        } else if ([category hasSuffix:@"s"] && (! [category hasSuffix:@"ss"])) {
            category = [category stringByReplacingOccurrencesOfString:@"s" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(category.length-1, 1)];
        }
        return [NSString stringWithFormat:@"%@ %@", category, NSLocalizedString(@"Movies", @"Movies")];
    } else {
        return [self.selectedCategory userTitle];
    }
}

#pragma mark - Movies fetching
- (void)gatherMoviesForCategory:(ESPCatalogCategory *)category
{
    if (IsEmpty([category urlTitle])) {
        if ([(ESPMediaTableProxy *)self.tablePager category]) {
            self.selectedCategory = [(ESPMediaTableProxy *)self.tablePager category];
        } else {
            ESPCatalogCategory *allCategory = [[ESPCatalogCategory alloc] init];
            allCategory.userTitle = @"All";
            allCategory.urlTitle = @"All";
            self.selectedCategory = allCategory;
        }
        [[self.topBar topItem] setTitle:[self topTitle]];
    }
    [(ESPMediaTableProxy *)self.tablePager setCategory:self.selectedCategory];
    [self.tablePager fetchNextPage];
}

- (void)gatherFeaturedMovies
{
    ESPCatalogCategory *featuredCategory = [[ESPCatalogCategory alloc] init];
    featuredCategory.urlTitle = @"Marvel";
    featuredCategory.userTitle = @"Marvel";
    [[[self proxy] catalog] obtainCategory:featuredCategory updatedSince:nil filterFields:nil ratings:[APP_DELEGATE ratingsFilter] success:^(NSArray *categoryResults, NSNumber *categoryCount) {
        self.featuredCollectionSet = [NSOrderedSet orderedSetWithArray:categoryResults];
        [self.featuredCollection reloadData];
    } failure:^(NSError *error) {
        ESPLog(@"ERROR gathering %@ category: %@", [featuredCategory userTitle], error);
    }];
}

- (UICollectionReusableView *)featuredSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        reusableview = [self.featuredCollection dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"FeaturedHeaderID" forIndexPath:indexPath];
    }
    return reusableview;
}

#pragma mark - UICollectionView override
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    if (view == self.collectionView) {
        return [self.collection count];
    } else {
        return [self.featuredCollectionSet count];
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)view cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    DMAMediaCollectionViewCell *cell = nil;
    NSOrderedSet *movies = nil;
    BOOL isCategoryCollection = (view == self.collectionView);
    if (isCategoryCollection) {
        cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:MediaCollectionCellID forIndexPath:indexPath];
        movies = self.collection;
        [self gatherMoreItemsFromPagerForIndexPath:indexPath];
    } else {
        cell = [self.featuredCollection dequeueReusableCellWithReuseIdentifier:MediaCollectionCellID forIndexPath:indexPath];
        movies = self.featuredCollectionSet;
    }
    
    cell.shouldShowBanner = NO;
    id object = [movies objectAtIndex:indexPath.row];
    BOOL isMovie = [object isKindOfClass:[ESPMedia class]];
    NSString *text = nil;
    if (isMovie) {
        BOOL hasEntitlement = [[self proxy] hasEntitlementToMedia:object];
        [cell configureForMedia:object hasEntitlement:hasEntitlement];
    } else {
        text = [object description];
        [cell.titleLabel setText:text];
        [cell.posterImageView setImage:[UIImage imageNamed:@"placeholder.png"]];
        [cell bringSubviewToFront:cell.titleLabel];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)view didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL isBaseCollection = (view == self.collectionView);
    NSOrderedSet *collectionSet = (isBaseCollection) ? self.collection : self.featuredCollectionSet;
    id movie = [collectionSet objectAtIndex:indexPath.row];
    self.selectedMedia = movie;
    
    NSString *segueIdentifier = [NSString stringWithFormat:@"SelectedMovieSegue%@", (isBaseCollection) ? @"Category" : @"Featured"];
    [self performSegueWithIdentifier:segueIdentifier sender:self];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)view viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (view == self.collectionView) {
        return [super collectionView:view viewForSupplementaryElementOfKind:kind atIndexPath:indexPath];
    } else {
        return [self featuredSupplementaryElementOfKind:kind atIndexPath:indexPath];
    }
}

#pragma mark - UIPopover/Category selector
- (void)displayCategoriesPopover
{    
    id viewController = [self.popover contentViewController];
    if ([viewController isKindOfClass:[DMACategoriesViewController class]]) {
        [viewController setSelectedCategory:self.selectedCategory];
        [[viewController tableView] reloadData];
        [viewController addObserver:self forKeyPath:@"selectedCategory" options:NSKeyValueObservingOptionNew context:@selector(feedChanged:)];
    }
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    id viewController = [popoverController contentViewController];
    if ([viewController isKindOfClass:[DMACategoriesViewController class]]) {
        [viewController removeObserver:self forKeyPath:@"selectedCategory"];
    }
}

#pragma mark - KVO
- (void)categoryChanged:(id)change
{
    if ([change isKindOfClass:[DMACategoriesViewController class]]) {
        self.selectedCategory = [change selectedCategory];
        [[self.topBar topItem] setTitle:[self topTitle]];
        
        [self.tablePager reset];

        self.isLoading = YES;
        [self gatherMoviesForCategory:self.selectedCategory];
        if (self.popover) {
            [self.popover dismissPopoverAnimated:YES];
        }
    }
    
    if (! [self isTablet]) {
        [self dismissViewControllerAnimated:YES completion:^{
            [change removeObserver:self forKeyPath:@"selectedCategory"];
        }];
    }
}

- (void)ownedMoviesChaged:(id)change
{
    if ([change isKindOfClass:[DMACommonProxy class]]) {
        [super ownedMoviesChaged:change];
        
        [self.collectionView reloadData];
        [self.featuredCollection reloadData];
    }
}

@end
