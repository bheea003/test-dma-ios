//
//  DMAOwnedMoviesViewController.h
//  DMA
//
//  Created by JP on 5/23/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DMACollectionViewController.h"

@class ESPMedia;
@interface DMAOwnedMoviesViewController : DMACollectionViewController

@property (strong, nonatomic) IBOutlet UINavigationBar *topBar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *signInOutButton;
@property (strong, nonatomic) IBOutlet UILabel *noMoviesBanner;
@property (strong, nonatomic) ESPMedia *selectedMedia;

- (IBAction)signInOutAction:(id)sender;
- (IBAction)searchAction:(id)sender;

@end
