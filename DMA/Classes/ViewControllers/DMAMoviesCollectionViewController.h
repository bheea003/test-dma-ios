//
//  DMAFirstViewController.h
//  DMA
//
//  Created by JP on 5/23/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DMACollectionViewController.h"

@class ESPCatalogCategory;
@interface DMAMoviesCollectionViewController : DMACollectionViewController <UIPopoverControllerDelegate>

@property (strong, nonatomic) IBOutlet UINavigationBar *topBar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *categoryButton;
@property (strong, nonatomic) UIPopoverController *popover;
@property (strong, nonatomic) ESPCatalogCategory *selectedCategory;
@property (strong, nonatomic) ESPMedia *selectedMedia;

@property (strong, nonatomic) IBOutlet UICollectionView *featuredCollection;
@property (strong, nonatomic) NSOrderedSet *featuredCollectionSet;

@end
