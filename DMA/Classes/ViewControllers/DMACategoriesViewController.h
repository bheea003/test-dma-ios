//
//  DMACategoriesViewController.h
//  DMA
//
//  Created by JP on 5/23/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMATableViewController.h"

@class ESPCatalogCategory;
@interface DMACategoriesViewController : DMATableViewController

@property (strong, nonatomic) ESPCatalogCategory *selectedCategory;

@end
