//
//  DMATableViewController.m
//  DMA
//
//  Created by JP on 5/23/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMATableViewController.h"
#import "DMACommonProxy.h"
#import "ESPCatalog.h"
#import "ESPConsumer.h"

@interface DMATableViewController ()

@end

@implementation DMATableViewController

#pragma mark - API Proxy
- (DMACommonProxy *)proxy
{
    return [DMACommonProxy sharedProxy];
}

- (ESPCatalog *)catalog
{
    return [[self proxy] catalog];
}

- (ESPConsumer *)consumer
{
    return [[self proxy] consumer];
}

@end
