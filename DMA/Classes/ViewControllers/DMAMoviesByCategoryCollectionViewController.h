//
//  DMAMoviesByCategoryCollectionViewController.h
//  DMA
//
//  Created by JP on 7/2/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMAViewController.h"

#define HEADER_HEIGHT 40

@interface DMAMoviesByCategoryCollectionViewController : DMAViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *catalogMediaSwitch;
@property (strong, nonatomic) ESPMedia *selectedMedia;
@property (assign, nonatomic) BOOL isLoadingDetails;

- (IBAction)didSwitchCatalog:(id)sender;

@end
