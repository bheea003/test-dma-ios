//
//  DMAMoviesByCategoryCollectionViewController.m
//  DMA
//
//  Created by JP on 7/2/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMAMoviesByCategoryCollectionViewController.h"
#import "DMAAppDelegate.h"
#import "DMACommonProxy.h"
#import "DMAMediaDetailViewController.h"
#import "DMAMediaCollectionViewCell.h"
#import "ESPCatalog.h"
#import "ESPCatalogCategory.h"
#import "ESPCatalogType.h"
#import "ESPConsumer.h"
#import "ESPEmbeddedIndexedCollectionViewTableViewCell.h"
#import "ESPIndexedCollectionView.h"
#import "MBProgressHUD.h"


@interface DMAMoviesByCategoryCollectionViewController ()
@property (strong, nonatomic) __block NSArray *mediaCollection;
@property (strong, nonatomic) __block NSMutableDictionary *mediaCollectionDictionary;
@property (strong, nonatomic) NSMutableDictionary *contentOffsetDictionary;
@end

@implementation DMAMoviesByCategoryCollectionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];    
    self.contentOffsetDictionary = [NSMutableDictionary dictionary];
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 66)]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (! self.mediaCollection) {
        [self setupView];
    }
    [self.tableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"MovieDetailSelectedSegue"]) {
        DMAMediaDetailViewController *viewController = [segue destinationViewController];
        viewController.movie = self.selectedMedia;
        viewController.hidesBottomBarWhenPushed = YES;
    }
}

#pragma mark - Override
- (void)setIsLoading:(BOOL)isLoading
{
    if (self.isLoadingDetails != isLoading) {
        self.isLoadingDetails = isLoading;
        if (self.isLoadingDetails) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.labelText = NSLocalizedString(@"Loading...", @"Loading...");
        } else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
    }
}

#pragma mark - IBActions
- (IBAction)didSwitchCatalog:(id)sender
{
    self.mediaCollectionDictionary = nil;
    self.mediaCollection = nil;
    [self.tableView reloadData];
    self.isLoading = YES;
    [self setupView];
}

#pragma mark - API convenience
- (DMACommonProxy *)proxy
{
    return [DMACommonProxy sharedProxy];
}

- (ESPCatalog *)catalog
{
    return [[self proxy] catalog];
}

#pragma mark - Category fetching
- (BOOL)isCatalog
{
    return [self.catalogMediaSwitch selectedSegmentIndex] == 0;
}

- (void)setupView
{
    if ([self isCatalog]) {
        [self setupCategories];
    } else {
        [self setupTypes];
    }
}

- (void)setupCategories
{
    NSArray *categoryNames = @[@"All"];
    [[self proxy] obtainCategoriesMinus:categoryNames success:^(NSArray *categories) {
        self.mediaCollection = categories;
        self.mediaCollectionDictionary = [NSMutableDictionary dictionary];
        for (ESPCatalogCategory *category in self.mediaCollection) {
            [self obtainMoviesForCategory:category];
        }
        self.isLoading = NO;
    } failure:^(NSError *error) {
        self.isLoading = NO;
        ESPLog(@"Categories error: %@", error);
    }];
}

- (void)obtainMoviesForCategory:(ESPCatalogCategory *)category
{
    NSString *categoryKey = [category urlTitle];
    if ([self.mediaCollectionDictionary valueForKey:categoryKey]) {
        return;
    }
    
    [[self catalog] obtainCategory:category updatedSince:nil filterFields:[ESPCatalog guidTitleAndThumbnailsOnlyFilter] ratings:[APP_DELEGATE ratingsFilter] success:^(NSArray *categoryResults, NSNumber *categoryCount) {
        [self.mediaCollectionDictionary setValue:categoryResults forKey:categoryKey];
        [self.tableView reloadData]; // NOT IDEAL, but it's a demo!
    } failure:^(NSError *error) {
        ESPLog(@"Catalog (%@) error: %@", [category userTitle], error);
    }];
}

- (void)setupTypes
{
    [[self proxy] obtainTypes:^(NSArray *types) {
        self.mediaCollection = types;
        self.mediaCollectionDictionary = [NSMutableDictionary dictionary];
        for (ESPCatalogType *type in self.mediaCollection) {
            [self obtainMoviesForType:type];
        }
        self.isLoading = NO;
    } failure:^(NSError *error) {
        self.isLoading = NO;
        ESPLog(@"Types error: %@", error);
    }];
}

- (void)obtainMoviesForType:(ESPCatalogType *)type
{
    NSString *typeKey = [type title];
    if ([self.mediaCollectionDictionary valueForKey:typeKey]) {
        return;
    }
    
    [[self catalog] obtainAllMediaForType:type updatedSince:nil filterFields:[ESPCatalog guidTitleAndThumbnailsOnlyFilter] ratings:[APP_DELEGATE ratingsFilter] success:^(NSArray *results) {
        [self.mediaCollectionDictionary setValue:results forKey:typeKey];
        [self.tableView reloadData]; // NOT IDEAL, but it's a demo!
    } failure:^(NSError *error) {
        ESPLog(@"Catalog (%@) error: %@", [type title], error);
    }];
}

- (NSString *)titleForSection:(NSInteger)section
{
    if ([self isCatalog]) {
        ESPCatalogCategory *category = [self.mediaCollection objectAtIndex:section];
        return [category userTitle];
    } else {
        ESPCatalogType *type = [self.mediaCollection objectAtIndex:section];
        return [type descriptionText];
    }
}
#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.mediaCollection count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self titleForSection:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HEADER_HEIGHT;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    int inset = 20;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, HEADER_HEIGHT)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(inset, 0, tableView.frame.size.width-inset, HEADER_HEIGHT)];
    [label setFont:[UIFont fontWithName:@"MatterhornSerif-Bold" size:20]];
    NSString *string =[self titleForSection:section];
    [label setText:string];
    [label setBackgroundColor:[UIColor whiteColor]];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor whiteColor]];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CategoryMoviesCell";
    ESPEmbeddedIndexedCollectionViewTableViewCell *cell = (ESPEmbeddedIndexedCollectionViewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [cell setCollectionViewDataSourceAndDelegate:self index:indexPath.section];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(ESPEmbeddedIndexedCollectionViewTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = cell.indexedCollectionView.index;
    CGFloat horizontalOffset = [self.contentOffsetDictionary[[@(index) stringValue]] floatValue];
    [cell.indexedCollectionView setContentOffset:CGPointMake(horizontalOffset, 0)];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    UINib *cellNib = [UINib nibWithNibName:@"DMAMediaCollectionViewCell" bundle:nil];
    [collectionView registerNib:cellNib forCellWithReuseIdentifier:@"MediaCollectionCellID"];
    
    return 1;
}

- (NSInteger)collectionView:(ESPIndexedCollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([self isCatalog]) {
        ESPCatalogCategory *category = [self.mediaCollection objectAtIndex:[collectionView index]];
        NSArray *categoryResults = [self.mediaCollectionDictionary valueForKey:[category urlTitle]];
        return [categoryResults count];
    } else {
        ESPCatalogType *type = [self.mediaCollection objectAtIndex:[collectionView index]];
        NSArray *typeResults = [self.mediaCollectionDictionary valueForKey:[type title]];
        return [typeResults count];
    }
}

- (UICollectionViewCell *)collectionView:(ESPIndexedCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    DMAMediaCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MediaCollectionCellID" forIndexPath:indexPath];
    cell.shouldShowBanner = NO;
    
    NSArray *movies = nil;
    if ([self isCatalog]) {
        ESPCatalogCategory *category = [self.mediaCollection objectAtIndex:[collectionView index]];
        movies = [self.mediaCollectionDictionary valueForKey:[category urlTitle]];
    } else {
        ESPCatalogType *type = [self.mediaCollection objectAtIndex:[collectionView index]];
        movies = [self.mediaCollectionDictionary valueForKey:[type title]];
    }
    
    id movie = [movies objectAtIndex:indexPath.row];
    BOOL hasEntitlement = [[self proxy] hasEntitlementToMedia:movie];
    [cell configureForMedia:movie hasEntitlement:hasEntitlement];
    return cell;
}

- (void)collectionView:(ESPIndexedCollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.isLoading = YES;

    NSArray *movies = nil;
    if ([self isCatalog]) {
        ESPCatalogCategory *category = [self.mediaCollection objectAtIndex:[collectionView index]];
        movies = [self.mediaCollectionDictionary valueForKey:[category urlTitle]];
        id movie = [movies objectAtIndex:indexPath.row];
        [[self catalog] obtainMetadataForMovieGUID:[movie guid] category:category shouldIncludeAncillaryContent:YES success:^(ESPMedia *media) {
            self.selectedMedia = media;
            [self performSegueWithIdentifier:@"MovieDetailSelectedSegue" sender:self];
            self.isLoading = NO;
        } failure:^(NSError *error) {
            self.isLoading = NO;
        }];
    } else {
        ESPCatalogType *type = [self.mediaCollection objectAtIndex:[collectionView index]];
        movies = [self.mediaCollectionDictionary valueForKey:[type title]];
        id movie = [movies objectAtIndex:indexPath.row];
        [[self catalog] obtainMetadataForMovieGUID:[movie guid] type:type success:^(ESPMedia *media) {
            [self playMedia:media];
        } failure:^(NSError *error) {
            self.isLoading = NO;
        }];
    }
}

#pragma mark - UIScrollViewDelegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([scrollView isKindOfClass:[ESPIndexedCollectionView class]]) {
        CGFloat horizontalOffset = scrollView.contentOffset.x;
        ESPIndexedCollectionView *collectionView = (ESPIndexedCollectionView *)scrollView;
        NSInteger index = collectionView.index;
        self.contentOffsetDictionary[[@(index) stringValue]] = @(horizontalOffset);
    }
}

@end
