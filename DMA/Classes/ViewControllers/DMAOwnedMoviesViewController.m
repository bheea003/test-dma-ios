//
//  DMAOwnedMoviesViewController.m
//  DMA
//
//  Created by JP on 5/23/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMAOwnedMoviesViewController.h"
#import "DMAAppDelegate.h"
#import "DMAMediaDetailViewController.h"

#import "ESPMedia.h"

@interface DMAOwnedMoviesViewController ()
@property (strong, nonatomic) NSDate *refreshedAt;
@end

@implementation DMAOwnedMoviesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self updateTopBar];
    self.noMoviesBanner.hidden = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    if (! [[self proxy] authorization]) {
        [[self proxy] displayLoginForm:nil];
    } else if ([self shouldRefresh]) {
        [self refreshMovies];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"SelectedMovieSegueOwned"]) {
        DMAMediaDetailViewController *viewController = [segue destinationViewController];
        viewController.movie = self.selectedMedia;
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[[self proxy] catalog] cancelAllHTTPOperations];
    [[[self proxy] consumer] cancelAllHTTPOperations];
    
    [super viewDidDisappear:animated];
}

#pragma mark - Logout
- (IBAction)signInOutAction:(id)sender
{
    if ([[self proxy] authorization]) {
        [[self proxy] addObserver:self forKeyPath:@"isLoading" options:NSKeyValueObservingOptionNew context:@selector(isLoadingChanged:)];
        [[self proxy] addObserver:self forKeyPath:@"authorization" options:NSKeyValueObservingOptionNew context:@selector(authorizationChanged:)];
        [[self proxy] logout];
    } else {
        [[self proxy] displayLoginForm:nil];
        self.noMoviesBanner.hidden = YES;
    }
}

- (IBAction)searchAction:(id)sender
{
    
}

#pragma mark - Convenience
- (void)updateTopBar
{
    [[self.topBar topItem] setTitle:NSLocalizedString(@"My Movies", @"My Movies")];
    [self.signInOutButton setTitle:([[self proxy] authorization]) ? NSLocalizedString(@"Sign Out", @"Sign Out") : NSLocalizedString(@"Sign In", @"Sign In")];
}

- (void)updateMovies
{
    self.collection = [[self proxy] ownedMedia];
    [self.collectionView reloadData];
}

- (void)updateNoMoviesBanner
{
    BOOL hasMovies = ([self.collection count] > 0);
    if (self.isLoading) {
        [self.noMoviesBanner setText:nil];
    }
    else if ([self.proxy authorization]) {
        [self.noMoviesBanner setText:(hasMovies) ? nil : NSLocalizedString(@"It appears you don't have any movies.", @"It appears you don't have any movies.")];
    } else {
        [self.noMoviesBanner setText:NSLocalizedString(@"It appears you're not logged in.", @"It appears you're not logged in.")];
    }
    self.noMoviesBanner.hidden = hasMovies;
}

- (void)refreshMovies
{
    if (self.isLoading) {
        return;
    }
    
    [[self proxy] obtainOwnedMovies];
    self.refreshedAt = [NSDate date];
}

- (NSArray *)favorites
{
    return [[self proxy] favorites];
}

- (ESPMedia *)mediaForIndexPath:(NSIndexPath *)indexPath
{
    return (indexPath.section == 0) ? [self.collection objectAtIndex:indexPath.row] : [[self favorites] objectAtIndex:indexPath.row];
}

#pragma mark - UICollectionView override


#define HeaderImage 11
#define HeaderLabel 12
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"OwnedCollectionHeaderID" forIndexPath:indexPath];
        UIImageView *imageView = (UIImageView *)[reusableview viewWithTag:HeaderImage];
        UILabel *label = (UILabel *)[reusableview viewWithTag:HeaderLabel];
        label.font = [UIFont fontWithName:@"MatterhornSerif-Bold" size:18.0];

        if (indexPath.section == 0) {
            imageView.image = [UIImage imageNamed:@"OwnedMickeyHeader"];
            label.text = NSLocalizedString(@"My Collection", @"My Collection");
        } else if (indexPath.section == 1) {
            imageView.image = [UIImage imageNamed:@"OwnedFavoriteHeader"];
            label.text = NSLocalizedString(@"My Favorites", @"My Favorites");
        }
    }
    return reusableview;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    if (section == 0) {
        return [self.collection count];
    } else {
        return [[self favorites] count];
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    DMAMediaCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MediaCollectionCellID forIndexPath:indexPath];
    cell.shouldShowBanner = NO;
    
    id object = [self mediaForIndexPath:indexPath];
    BOOL isMovie = [object isKindOfClass:[ESPMedia class]];
    NSString *text = nil;
    if (isMovie) {
        BOOL hasEntitlement = [[self proxy] hasEntitlementToMedia:object];
        [cell configureForMedia:object hasEntitlement:hasEntitlement];
    } else {
        text = [object description];
        [cell.titleLabel setText:text];
        [cell.posterImageView setImage:[UIImage imageNamed:@"placeholder.png"]];
        [cell bringSubviewToFront:cell.titleLabel];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)view didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedMedia = [self mediaForIndexPath:indexPath];
    [self performSegueWithIdentifier:@"SelectedMovieSegueOwned" sender:self];
}

#pragma mark - KVO
- (void)isLoadingChanged:(id)change
{
    if ([change isKindOfClass:[DMACommonProxy class]]) {
        self.isLoading = [change isLoading];
    }
}

- (void)authorizationChanged:(id)change
{
    if ([change isKindOfClass:[DMACommonProxy class]]) {
        [self updateMovies];
        [self updateTopBar];
        self.isLoading = NO;
    }
}

- (void)ownedMoviesChaged:(id)change
{
    if ([change isKindOfClass:[DMACommonProxy class]]) {
        [self updateMovies];
        [self updateNoMoviesBanner];
        self.isLoading = NO;
    }
}



@end
