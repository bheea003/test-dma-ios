//
//  DMACollectionViewController.m
//  DMA
//
//  Created by JP on 5/23/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMACollectionViewController.h"
#import "DMAAppDelegate.h"

#import "ESPMedia.h"
#import "ESPMediaThumbnail.h"
#import "ESPMediaTableProxy.h"
#import "UIImageView+AFNetworking.h"

#define COLLECTION_PAGE_SIZE 36

#define FooterLabelTag 3668 // FOOT
#define ActivityTag 3669 //

NSString *const CollectionFooterID = @"CollectionFooterID"; // UICollectionView footer id

@interface DMACollectionViewController ()
@property (assign, nonatomic) BOOL hasSetupObservers;

- (void)setupObservers;
- (void)teardownObservers;
@end

@implementation DMACollectionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UINib *cellNib = [UINib nibWithNibName:@"DMAMediaCollectionViewCell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:MediaCollectionCellID];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupObservers];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self teardownObservers];
    
    [super viewDidDisappear:animated];
}

#pragma mark - API Proxy
- (DMACommonProxy *)proxy
{
    return [DMACommonProxy sharedProxy];
}

#pragma mark - Caching Help
- (BOOL)shouldRefresh
{
    NSDate *lastCheck = self.refreshedAt;
    if (lastCheck)
    {
        int delay = DELAY_REFRESH_FOR;
        NSDate *delta = [NSDate dateWithTimeInterval:delay sinceDate:lastCheck];
        if ([delta compare:[NSDate date]] == NSOrderedDescending)
        {
            return NO;
        }
    }
    return YES;
}

#pragma mark - Movie Convenience
- (ESPTableProxy *)tablePager
{
    if (! [super tablePager]) {
        ESPMediaTableProxy *mediaTablePager = [[ESPMediaTableProxy alloc] initWithPageSize:COLLECTION_PAGE_SIZE delegate:self];
        mediaTablePager.catalog = [[self proxy] catalog];
        mediaTablePager.ratingsLimit = [APP_DELEGATE ratingsFilter];
        [super setTablePager:mediaTablePager];
    }
    return [super tablePager];
}

#pragma mark - UICollectionView
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    return [self.collection count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{    
    DMAMediaCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MediaCollectionCellID forIndexPath:indexPath];
    cell.shouldShowBanner = NO;
    
    id object = [self.collection objectAtIndex:indexPath.row];
    BOOL isMovie = [object isKindOfClass:[ESPMedia class]];
    NSString *text = nil;
    if (isMovie) {        
        BOOL hasEntitlement = [[self proxy] hasEntitlementToMedia:object];
        [cell configureForMedia:object hasEntitlement:hasEntitlement];
    } else {
        text = [object description];
        [cell.titleLabel setText:text];
        [cell.posterImageView setImage:[UIImage imageNamed:@"placeholder.png"]];
        [cell bringSubviewToFront:cell.titleLabel];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    id movie = [self.collection objectAtIndex:indexPath.row];
    [[self proxy] showMedia:movie viewController:self];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionFooter) {
        reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:CollectionFooterID forIndexPath:indexPath];
        int resultCount = [self.tablePager.results count];
        int totalCount = self.tablePager.totalResults;
        if (resultCount < totalCount) {
            NSString *resultString = [NSString stringWithFormat:@"%d %@ %d", resultCount, NSLocalizedString(@"results out of", @"results out of"), totalCount];
            [(UILabel *)[reusableview viewWithTag:FooterLabelTag] setText:resultString];
            if ([self.tablePager requestStatus] == RequestStatusInProgress) {
                [(UIActivityIndicatorView *)[reusableview viewWithTag:ActivityTag] startAnimating];
            } else {
                [(UIActivityIndicatorView *)[reusableview viewWithTag:ActivityTag] stopAnimating];
            }
        } else {
            [(UILabel *)[reusableview viewWithTag:FooterLabelTag] setText:@""];
            [(UIActivityIndicatorView *)[reusableview viewWithTag:ActivityTag] stopAnimating];
        }
    }
    return reusableview;
}

#pragma mark - KVO
- (void)setupObservers
{
    if (! self.hasSetupObservers) {
        self.hasSetupObservers = YES;
        [[self proxy] addObserver:self forKeyPath:@"authorization" options:NSKeyValueObservingOptionNew context:@selector(authorizationChanged:)];
        [[self proxy] addObserver:self forKeyPath:@"ownedMedia" options:NSKeyValueObservingOptionNew context:@selector(ownedMoviesChaged:)];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(defaultsChanged:) name:NSUserDefaultsDidChangeNotification object:nil];
    }
}

- (void)teardownObservers
{
    if (self.hasSetupObservers) {
        self.hasSetupObservers = NO;
        [[self proxy] removeObserver:self forKeyPath:@"authorization"];
        [[self proxy] removeObserver:self forKeyPath:@"ownedMedia"];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NSUserDefaultsDidChangeNotification object:nil];
    }
}

- (void)authorizationChanged:(id)change
{
    if ([change isKindOfClass:[DMACommonProxy class]]) {
        ESPLog(@"authorization changed");
    }
}

- (void)ownedMoviesChaged:(id)change
{
    if ([change isKindOfClass:[DMACommonProxy class]]) {
        ESPLog(@"ownedMovies changed");
    }
}

- (BOOL)defaultsChanged:(NSNotification *)notification
{
    NSString *ratingsFilter = [[APP_DELEGATE ratingsFilter] componentsJoinedByString:@":"];
    NSString *pagerFilter = [[(ESPMediaTableProxy *)[self tablePager] ratingsLimit] componentsJoinedByString:@":"];
    if ([ratingsFilter caseInsensitiveCompare:pagerFilter] != NSOrderedSame) {
        [(ESPMediaTableProxy *)[self tablePager] setRatingsLimit:[APP_DELEGATE ratingsFilter]];
        [[self tablePager] fetchFirstPage];
    }
    self.refreshedAt = [NSDate distantPast]; // defaults changed; refresh cached data
    
    return YES;
}

@end
