//
//  DMAAppleAtomProxy.m
//  DMA
//
//  Created by JP on 6/6/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMAAppleAtomProxy.h"

@implementation DMAAppleAtomProxy

+ (id)sharedProxy
{
    static dispatch_once_t onceQueue;
    static DMAAppleAtomProxy *proxy = nil;
    
    dispatch_once(&onceQueue, ^{
        proxy = [[self alloc] init];        
    });
    return proxy;
}

#pragma mark - Title to Apple Atom conversion
- (int)appleAtomForTitle:(NSString *)title
{
    int identifier = -1;
    
    if ([title caseInsensitiveCompare:@"PLACEHOLDER"] == NSOrderedSame) { identifier = 1234567890; }
    else if ([title caseInsensitiveCompare:@"A Bug's Life"] == NSOrderedSame) { identifier = 188837736; }
    else if ([title caseInsensitiveCompare:@"Air Buddies"] == NSOrderedSame) { identifier = 207105018; }
    else if ([title caseInsensitiveCompare:@"Alice in Wonderland (1951)"] == NSOrderedSame) { identifier = 412102121; }
    else if ([title caseInsensitiveCompare:@"An Extremely Goofy Movie"] == NSOrderedSame) { identifier = 492074024; }
    else if ([title caseInsensitiveCompare:@"Babes in Toyland"] == NSOrderedSame) { identifier = 298594584; }
    else if ([title caseInsensitiveCompare:@"Bedknobs and Broomsticks"] == NSOrderedSame) { identifier = 324854399; }
    else if ([title caseInsensitiveCompare:@"Bedtime Stories"] == NSOrderedSame) { identifier = 301177697; }
    else if ([title caseInsensitiveCompare:@"Blackbeard's Ghost"] == NSOrderedSame) { identifier = 297544955; }
    else if ([title caseInsensitiveCompare:@"Blank Check"] == NSOrderedSame) { identifier = 492397076; }
    else if ([title caseInsensitiveCompare:@"Bolt"] == NSOrderedSame) { identifier = 299891336; }
    else if ([title caseInsensitiveCompare:@"Boundin'"] == NSOrderedSame) { identifier = 81758680; }
    else if ([title caseInsensitiveCompare:@"Brave"] == NSOrderedSame) { identifier = 544101708; }
    else if ([title caseInsensitiveCompare:@"Brother Bear 2"] == NSOrderedSame) { identifier = 187696289; }
    else if ([title caseInsensitiveCompare:@"Cadet Kelly"] == NSOrderedSame) { identifier = 219560691; }
    else if ([title caseInsensitiveCompare:@"Candleshoe"] == NSOrderedSame) { identifier = 299438789; }
    else if ([title caseInsensitiveCompare:@"Cars"] == NSOrderedSame) { identifier = 188764984; }
    else if ([title caseInsensitiveCompare:@"Cars 2"] == NSOrderedSame) { identifier = 560482532; }
    else if ([title caseInsensitiveCompare:@"Cars Toons Air Mater"] == NSOrderedSame) { identifier = 550680230; }
    else if ([title caseInsensitiveCompare:@"Cars Toons Mater's Tall Tales"] == NSOrderedSame) { identifier = 404403405; }
    else if ([title caseInsensitiveCompare:@"Cars Toons Moon Mater"] == NSOrderedSame) { identifier = 550680701; }
    else if ([title caseInsensitiveCompare:@"Cars Toons Time Travel Mater"] == NSOrderedSame) { identifier = 529431217; }
    else if ([title caseInsensitiveCompare:@"Finding Nemo"] == NSOrderedSame) { identifier = 255295077; }
    else if ([title caseInsensitiveCompare:@"Monsters, Inc."] == NSOrderedSame) { identifier = 265727985; }
    else if ([title caseInsensitiveCompare:@"Ratatouille"] == NSOrderedSame) { identifier = 265250067; }
    else if ([title caseInsensitiveCompare:@"The Hunchback of Notre Dame"] == NSOrderedSame) { identifier = 292004872; }
    else if ([title caseInsensitiveCompare:@"The Incredibles"] == NSOrderedSame) { identifier = 188702939; }
    else if ([title caseInsensitiveCompare:@"Toy Story"] == NSOrderedSame) { identifier = 188703840; }
    else if ([title caseInsensitiveCompare:@"Toy Story 2"] == NSOrderedSame) { identifier = 268793231; }
    else if ([title caseInsensitiveCompare:@"Toy Story 3"] == NSOrderedSame) { identifier = 381438139; }
    else if ([title caseInsensitiveCompare:@"Up"] == NSOrderedSame) { identifier = 322447599; }
    else if ([title caseInsensitiveCompare:@"Wall•E"] == NSOrderedSame) { identifier = 86533539; }    
    
    return identifier;
}

@end
