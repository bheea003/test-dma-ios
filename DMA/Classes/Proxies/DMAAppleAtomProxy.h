//
//  DMAAppleAtomProxy.h
//  DMA
//
//  Created by JP on 6/6/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMAAppleAtomProxy : NSObject

+ (id)sharedProxy;

- (int)appleAtomForTitle:(NSString *)title;

@end
