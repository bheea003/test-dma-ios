//
//  DMACommonProxy.h
//  DMA
//
//  Created by JP on 6/20/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPCommonProxy.h"
#import "ESPNetworkActivityProtocol.h"

@class ESPMedia;
@interface DMACommonProxy : ESPCommonProxy

- (void)showMedia:(ESPMedia *)media viewController:(UIViewController<ESPNetworkActivityProtocol> *)presentingViewController;

- (NSArray *)favorites;
- (BOOL)isFavorite:(ESPMedia *)media;
- (void)addToFavorites:(ESPMedia *)media;
- (void)removeFromFavorites:(ESPMedia *)media;

@end
