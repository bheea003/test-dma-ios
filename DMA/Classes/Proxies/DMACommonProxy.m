//
//  DMACommonProxy.m
//  DMA
//
//  Created by JP on 6/20/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMACommonProxy.h"
#import "ESPMedia.h"
#import "DMASettingsConstants.h"
#import "DMAMediaDetailViewController.h"

@interface DMACommonProxy()
@property (strong, nonatomic) NSMutableSet *favoritesSet;
@end

@implementation DMACommonProxy

+ (id)sharedProxy
{
    static dispatch_once_t onceQueue;
    static DMACommonProxy *proxy = nil;
    
    dispatch_once(&onceQueue, ^{
        proxy = [[self alloc] init];
        [proxy restore];
        NSLog(@"%@", [proxy licenseArray]);
        [[NSNotificationCenter defaultCenter] addObserver:proxy selector:@selector(defaultsChanged:) name:NSUserDefaultsDidChangeNotification object:nil];
    });
    return proxy;
}

#pragma mark - Override
- (void)setOwnedMedia:(NSMutableOrderedSet *)ownedMedia
{
    [super setOwnedMedia:ownedMedia];
    [self readFavorites];
}

- (void)logout
{
    [self writeFavorites];
    [super logout];
}

#pragma mark - Simple Media details display
- (void)showMedia:(ESPMedia *)media viewController:(UIViewController<ESPNetworkActivityProtocol> *)presentingViewController
{
    presentingViewController.isLoading = YES;
    
    [[self catalog] obtainMetadataForMovie:media shouldIncludeAncillaryContent:YES success:^(ESPMedia *detailedMedia) {
        NSString *nibName = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? @"DMAMediaDetailViewController_iPad" : @"DMAMediaDetailViewController_iPhone";
        DMAMediaDetailViewController *movieDetailViewController = [[DMAMediaDetailViewController alloc] initWithNibName:nibName bundle:nil];
        movieDetailViewController.modalPresentationStyle = UIModalPresentationFormSheet;
        [media setAncillaryContent:[detailedMedia ancillaryContent]];
        movieDetailViewController.movie = media;
        presentingViewController.isLoading = NO;
        [presentingViewController presentViewController:movieDetailViewController animated:YES completion:NULL];
    } failure:^(NSError *error) {
        presentingViewController.isLoading = NO;
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                    message:[error description]
                                   delegate:nil
                          cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                          otherButtonTitles:nil] show];
    }];
}

#pragma mark - KVO
- (void)defaultsChanged:(NSNotification *)notification
{
    NSDictionary *defaults = [[notification object] dictionaryRepresentation];
    NSString *appName = [defaults valueForKey:ApplicationNameKey];
    if (! IsEmpty(appName)) {
        self.applicationName = appName;
    }
    NSString *appCountry = [defaults valueForKey:ApplicationCountryKey];
    if (! IsEmpty(appCountry)) {
        self.applicationCountry = appCountry;
    }
    NSString *appLanguage = [defaults valueForKey:ApplicationLanguageKey];
    if (! IsEmpty(appLanguage)) {
        self.applicationLanguage = appLanguage;
    }
}

#pragma mark - Favorites
- (NSString *)favoritesKey
{
    NSString *swid = [[self authorization] swid];
    NSString *favoritesKey = [NSString stringWithFormat:@"%@_favorites", swid];
    return favoritesKey;
}

- (void)readFavorites
{
    if ([self authorization]) {
        NSData *archive = [[NSUserDefaults standardUserDefaults] valueForKey:[self favoritesKey]];
        NSArray *archivedArray =  [NSKeyedUnarchiver unarchiveObjectWithData:archive];
        self.favoritesSet = [NSMutableSet setWithArray:archivedArray];
    }    
}

- (void)writeFavorites
{
    NSData *archive = [NSKeyedArchiver archivedDataWithRootObject:[self favorites]];
    [[NSUserDefaults standardUserDefaults] setValue:archive forKey:[self favoritesKey]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}    

- (NSArray *)favorites
{
    return [self.favoritesSet allObjects];
}

- (BOOL)isFavorite:(ESPMedia *)media
{
    return [self.favoritesSet containsObject:media];
}

- (void)addToFavorites:(ESPMedia *)media
{
    [self.favoritesSet addObject:media];
    [self writeFavorites];
}

- (void)removeFromFavorites:(ESPMedia *)media
{
    [self.favoritesSet removeObject:media];
    [self writeFavorites];
}

@end
