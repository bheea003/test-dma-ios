//
//  DMAVideoControlView.m
//  DMA
//
//  Created by JP on 7/26/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMAVideoControlView.h"

@implementation DMAVideoControlView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (IBAction)jumpBackAction:(id)sender
{
    [self.delegate control:self wantsJumpBack:sender];
}

- (IBAction)beginRewindAction:(id)sender
{
    [self.delegate control:self beginRewind:sender];
}

- (IBAction)endRewindAction:(id)sender
{
    [self.delegate control:self endRewind:sender];
}

- (IBAction)togglePlayAction:(id)sender
{
    [self.delegate control:self wantsTogglePlay:sender];
}

- (IBAction)beginFastForwardAction:(id)sender
{
    [self.delegate control:self beginFastForward:sender];
}

- (IBAction)endFastForwardAction:(id)sender
{
    [self.delegate control:self endFastForward:sender];
}

- (IBAction)bookmarkAction:(id)sender
{
    [self.delegate control:self wantsBookmark:sender];
}

@end
