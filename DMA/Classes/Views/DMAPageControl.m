//
//  DMAPageControl.m
//  DMA
//
//  Created by JP on 7/11/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMAPageControl.h"

@implementation DMAPageControl

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.activeImage = [UIImage imageNamed:@"btn_pagination_up"];
        self.inactiveImage = [UIImage imageNamed:@"btn_pagination_down"];
    }
    return self;
}

- (void)setCurrentPage:(NSInteger)page
{
    [super setCurrentPage:page];

    for (int i = 0; i < [self.subviews count]; i++)
   {
       UIView *dotView = [self.subviews objectAtIndex:i];
        if ([dotView isKindOfClass:[UIImageView class]])
        {
            UIImageView *imageView = (UIImageView *)dotView;
            imageView.frame = CGRectMake(dotView.frame.origin.x, dotView.frame.origin.y, 8, 8);
            imageView.image = (i == self.currentPage) ? self.activeImage : self.inactiveImage;
        } else {
            // iOS7 - let Apple draw its own page pagination until we figure out how to use our own images instead of Apple's in iOS7
        }
    }
}

@end
