//
//  DMAVideoControlView.h
//  DMA
//
//  Created by JP on 7/26/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMAVideoControlView;
@protocol DMAVideoControlDelegate <NSObject>
- (void)control:(DMAVideoControlView *)control wantsJumpBack:(id)sender;
- (void)control:(DMAVideoControlView *)control beginRewind:(id)sender;
- (void)control:(DMAVideoControlView *)control endRewind:(id)sender;
- (void)control:(DMAVideoControlView *)control wantsTogglePlay:(id)sender;
- (void)control:(DMAVideoControlView *)control beginFastForward:(id)sender;
- (void)control:(DMAVideoControlView *)control endFastForward:(id)sender;
- (void)control:(DMAVideoControlView *)control wantsBookmark:(id)sender;
@end


@interface DMAVideoControlView : UIView

@property (weak, nonatomic) id<DMAVideoControlDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIButton *jumpBackButton;
@property (strong, nonatomic) IBOutlet UIButton *rewindButton;
@property (strong, nonatomic) IBOutlet UIButton *togglePlayButton;
@property (strong, nonatomic) IBOutlet UIButton *fastForwardButton;
@property (strong, nonatomic) IBOutlet UIButton *bookmarkButton;

- (IBAction)jumpBackAction:(id)sender;
- (IBAction)beginRewindAction:(id)sender;
- (IBAction)endRewindAction:(id)sender;
- (IBAction)togglePlayAction:(id)sender;
- (IBAction)beginFastForwardAction:(id)sender;
- (IBAction)endFastForwardAction:(id)sender;
- (IBAction)bookmarkAction:(id)sender;

@end
