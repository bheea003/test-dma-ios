//
//  DMAPageControl.h
//  DMA
//
//  Created by JP on 7/11/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DMAPageControl : UIPageControl

@property (strong, nonatomic) UIImage *activeImage;
@property (strong, nonatomic) UIImage *inactiveImage;

@end
