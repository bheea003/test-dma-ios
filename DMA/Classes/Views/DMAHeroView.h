//
//  DMAHeroView.h
//  DMA
//
//  Created by JP on 7/10/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

typedef enum {
    DMAHeroShare,
    DMAHeroFavorite,
    DMAHeroDetails,
    DMAHeroDone,
    DMAHeroPlayMovie,
    DMAHeroPlayTrailer,
    DMAHeroPurchase,
    DMAHeroShowtimes
} DMAHeroAction;


@class DMAHeroView;
@protocol DMAHeroViewDelegate <NSObject>
- (void)heroView:(DMAHeroView *)heroView didSelectAction:(DMAHeroAction)heroState button:(UIButton *)button;
@end

@class ESPMedia;
@interface DMAHeroView : UIView

@property (assign, nonatomic) id<DMAHeroViewDelegate> delegate;
@property (assign, nonatomic) NSUInteger pageIndex;
@property (strong, nonatomic) IBOutlet UIImageView *posterView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *smallLeftButton;
@property (strong, nonatomic) IBOutlet UIButton *smallRightButton;
@property (strong, nonatomic) IBOutlet UIButton *bigOverlayButton;
@property (strong, nonatomic) IBOutlet UIButton *shareButton;
@property (strong, nonatomic) IBOutlet UIButton *favoriteButton;

- (void)configureViewForMovie:(ESPMedia *)movie isPreview:(BOOL)isPreview hasEntitlement:(BOOL)hasEntitlement;

- (IBAction)watchAction:(id)sender;
- (IBAction)detailsOrPurchaseAction:(id)sender;
- (IBAction)shareAction:(id)sender;
- (IBAction)favoriteAction:(id)sender;

@end
