//
//  DMAMediaCollectionViewCell.h
//  DMA
//
//  Created by JP on 5/21/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    BannerTypeNone,
    BannerTypeOwned,
    BannerTypeFavorite
} DMAMediaBannerType;

#define MediaCollectionCellID @"MediaCollectionCellID"

@class ESPMedia;
@interface DMAMediaCollectionViewCell : UICollectionViewCell

@property (assign, nonatomic) BOOL shouldShowBanner;
@property (strong, nonatomic) IBOutlet UIImageView *posterImageView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIView *disabledOverlay;
@property (strong, nonatomic) IBOutlet UIImageView *bannerImageView;

- (void)configureForMedia:(ESPMedia *)media hasEntitlement:(BOOL)hasEntitlement;
- (void)setBannerType:(DMAMediaBannerType)bannerType;

@end
