//
//  DMAMediaCreditCollectionViewCell.h
//  DMA
//
//  Created by JP on 6/5/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <UIKit/UIKit.h>

#define MediaCreditCollectionCellID @"MediaCreditCollectionCellID"

@interface DMAMediaCreditCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *mediaRole;
@property (strong, nonatomic) IBOutlet UILabel *mediaFullName;

@end
