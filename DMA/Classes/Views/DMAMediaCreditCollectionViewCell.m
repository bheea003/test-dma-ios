//
//  DMAMediaCreditCollectionViewCell.m
//  DMA
//
//  Created by JP on 6/5/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMAMediaCreditCollectionViewCell.h"

@implementation DMAMediaCreditCollectionViewCell

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    [self.mediaRole setText:@""];
    [self.mediaFullName setText:@""];
}

@end
