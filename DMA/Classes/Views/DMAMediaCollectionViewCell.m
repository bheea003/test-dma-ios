//
//  DMAMediaCollectionViewCell.m
//  DMA
//
//  Created by JP on 5/21/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMAMediaCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "ESPMedia.h"
#import "ESPMediaThumbnail.h"

@implementation DMAMediaCollectionViewCell

- (void)configureForMedia:(id)media hasEntitlement:(BOOL)hasEntitlement
{
    NSString *text = [media title];
    [self.titleLabel setText:text];
    [self sendSubviewToBack:self.titleLabel];
    
    ESPMediaThumbnail *thumbnail = [media posterThumbnail2x];
    if (thumbnail) {
        NSURL *imageURL = [NSURL URLWithString:[thumbnail fileUrl]];
        [self.posterImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    }
    
    self.shouldShowBanner = hasEntitlement;
    [self setBannerType: (hasEntitlement) ? BannerTypeOwned : BannerTypeNone];
}

- (void)setBannerType:(DMAMediaBannerType)bannerType
{
    self.bannerImageView.hidden = (! self.shouldShowBanner);

    if (self.shouldShowBanner) {
        UIImage *bannerImage = nil;
        switch (bannerType) {
            case BannerTypeFavorite:
                bannerImage = [UIImage imageNamed:@"icn_fav"];
                break;
            case BannerTypeOwned:
                bannerImage = [UIImage imageNamed:@"icn_owned"];
                break;
            default:
                bannerImage = nil;
                break;
        }
        
        [self bringSubviewToFront:self.bannerImageView];
        [UIView transitionWithView:self
                          duration:0.2f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            [self.bannerImageView setImage:bannerImage];
                            [self.bannerImageView setNeedsDisplay];
                        } completion:nil];
    }
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    [self.bannerImageView setImage:nil];
    [self.posterImageView setImage:nil];
    [self.titleLabel setText:@""];
    
    self.disabledOverlay.hidden = YES;
}

@end
