//
//  DMAHeroView.m
//  DMA
//
//  Created by JP on 7/10/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMAHeroView.h"
#import "UIImageView+AFNetworking.h"
#import "ESPMedia.h"
#import "ESPMediaThumbnail.h"

#import <QuartzCore/QuartzCore.h>

typedef enum {
    DMAHeroPreviewOwned, // isPreview && entitled
    DMAHeroPreviewUnowned, // isPreview && not entitled
    DMAHeroDetailOwned, // isNotPreview && entitled
    DMAHeroDetailUnowned, // isNotPreview && not entitled
    DMAHeroPreviewInTheaters, // isNotPreview && not released
    DMAHeroPreviewPresale // isNotPreview && ???
} DMAHeroState;

@interface DMAHeroView ()
@property (assign, nonatomic) BOOL isPreview;
@property (assign, nonatomic) BOOL hasEntitlement;
@property (assign, nonatomic) BOOL hasBeenReleased;
@end

@implementation DMAHeroView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DMAHeroView" owner:self options:nil];
        self = [nib lastObject];
    }
    return self;
}

#pragma mark - View Configuration
- (void)configureViewForMovie:(ESPMedia *)movie isPreview:(BOOL)isPreview hasEntitlement:(BOOL)hasEntitlement
{
    self.titleLabel.font = [UIFont fontWithName:@"MatterhornSerif-Bold" size:36.0];
    self.isPreview = isPreview;
    self.hasEntitlement = hasEntitlement;
    self.hasBeenReleased = [movie hasBeenReleased];
    
    [self configureButtons];
    [self configureViewForMovie:movie];
}

- (void)configureViewForMovie:(ESPMedia *)movie
{
    NSString *bigBackgroundImageName = [NSString stringWithFormat:@"%@-bg", [movie guid]];
    UIImage *bigBackgroundImage = [UIImage imageNamed:bigBackgroundImageName];
    if (bigBackgroundImage) {
        [self.posterView setImage:bigBackgroundImage];
    } else {
        NSURL *posterURL = [NSURL URLWithString:[[movie largeThumbnail] fileUrl]];
        UIImage *placeholder = [UIImage imageNamed:@"placeholder"];
        [self.posterView setImageWithURL:posterURL placeholderImage:placeholder];
    }
    
    [self.titleLabel setText:[movie title]];
}

- (void)configureButtons
{
    self.smallRightButton.hidden = YES;
    self.smallLeftButton.hidden = YES;
    self.bigOverlayButton.hidden = YES;
    
    if (self.hasBeenReleased) {
        if (! self.isPreview && self.hasEntitlement) {
            [self configureButtonsForPlay];
        } else {
            [self configureButtonsForSale];
        }
    } else {
        [self configureButtonsForTicketing];
    }
}

- (DMAHeroState)heroState
{
    if (self.hasBeenReleased) {
        if (self.isPreview && self.hasEntitlement) {
            return DMAHeroPreviewOwned;
        } else if (self.isPreview && !self.hasEntitlement) {
            return DMAHeroPreviewUnowned;
        } else if (! self.isPreview && self.hasEntitlement) {
            return DMAHeroDetailOwned;
        } else if (! self.isPreview && !self.hasEntitlement) {
            return DMAHeroDetailUnowned;
        }
    } else {
        return DMAHeroPreviewInTheaters;
    }
    return DMAHeroPreviewPresale;
}

#pragma mark - IBActions
- (IBAction)watchAction:(id)sender
{
    switch ([self heroState]) {
        case DMAHeroPreviewOwned:
            [self.delegate heroView:self didSelectAction:DMAHeroPlayTrailer button:sender];
            break;
        case DMAHeroPreviewUnowned:
            [self.delegate heroView:self didSelectAction:DMAHeroPlayTrailer button:sender];
            break;
        case DMAHeroDetailOwned:
            [self.delegate heroView:self didSelectAction:DMAHeroPlayMovie button:sender];
            break;
        case DMAHeroDetailUnowned:
            [self.delegate heroView:self didSelectAction:DMAHeroPlayTrailer button:sender];
            break;
        default:
            [self.delegate heroView:self didSelectAction:DMAHeroPlayTrailer button:sender];
            break;
    }
}

- (IBAction)detailsOrPurchaseAction:(id)sender
{
    switch ([self heroState]) {
        case DMAHeroPreviewOwned:
            [self.delegate heroView:self didSelectAction:DMAHeroDetails button:sender];
            break;
        case DMAHeroPreviewUnowned:
            [self.delegate heroView:self didSelectAction:DMAHeroDetails button:sender];
            break;
        case DMAHeroDetailOwned:
            [self.delegate heroView:self didSelectAction:DMAHeroPlayMovie button:sender];
            break;
        case DMAHeroDetailUnowned:
            [self.delegate heroView:self didSelectAction:DMAHeroPurchase button:sender];
            break;
        default:
            [self.delegate heroView:self didSelectAction:DMAHeroPurchase button:sender];
            break;
    }
}

- (IBAction)shareAction:(id)sender
{
    [self.delegate heroView:self didSelectAction:DMAHeroShare button:sender];
}

- (IBAction)favoriteAction:(id)sender
{
    [self.delegate heroView:self didSelectAction:DMAHeroFavorite button:sender];
}

#pragma mark - Button Madness
- (UIImage *)watchButtonDown
{
    switch ([self heroState]) {
        case DMAHeroPreviewOwned:
            return [self previewImage:NO];
            break;
        case DMAHeroPreviewUnowned:
            return [self previewImage:NO];
            break;
        case DMAHeroDetailOwned:
            return [self playMovieImage:NO];
            break;
        case DMAHeroDetailUnowned:
            return [self previewImage:NO];
            break;
        default:
            return [self previewImage:NO];
            break;
    }
}

- (UIImage *)watchButtonUp
{
    switch ([self heroState]) {
        case DMAHeroPreviewOwned:
            return [self previewImage:YES];
            break;
        case DMAHeroPreviewUnowned:
            return [self previewImage:YES];
            break;
        case DMAHeroDetailOwned:
            return [self playMovieImage:YES];
            break;
        case DMAHeroDetailUnowned:
            return [self previewImage:YES];
            break;
        default:
            return [self previewImage:YES];
            break;
    }
}

- (UIImage *)detailsImageDown
{
    switch ([self heroState]) {
        case DMAHeroPreviewOwned:
            return [self detailsImage:NO];
            break;
        case DMAHeroPreviewUnowned:
            return [self detailsImage:NO];
            break;
        case DMAHeroDetailOwned:
            return [self playMovieImage:NO];
            break;
        case DMAHeroDetailUnowned:
            return [self purchaseImage:NO];
            break;
        default:
            return [self detailsImage:NO];
            break;
    }
}

- (UIImage *)detailsImageUp
{
    switch ([self heroState]) {
        case DMAHeroPreviewOwned:
            return [self detailsImage:YES];
            break;
        case DMAHeroPreviewUnowned:
            return [self detailsImage:YES];
            break;
        case DMAHeroDetailOwned:
            return [self playMovieImage:YES];
            break;
        case DMAHeroDetailUnowned:
            return [self purchaseImage:YES];
            break;
        default:
            return [self detailsImage:YES];
            break;
    }
}

- (UIImage *)detailsImage:(BOOL)isHighlighted
{
    return isHighlighted ? [UIImage imageNamed:@"btn_details_down"] : [UIImage imageNamed:@"btn_details_up"];
}

- (UIImage *)doneImage:(BOOL)isHighlighted
{
    return isHighlighted ? [UIImage imageNamed:@"btn_done_DOWN"] : [UIImage imageNamed:@"btn_done_UP"];
}

- (UIImage *)playMovieImage:(BOOL)isHighlighted
{
    return isHighlighted ? [UIImage imageNamed:@"btn_play_DOWN"] : [UIImage imageNamed:@"btn_play_UP"];
}

- (UIImage *)previewImage:(BOOL)isHighlighted
{
    return isHighlighted ? [UIImage imageNamed:@"btn_preview_down"] : [UIImage imageNamed:@"btn_preview_up"];
}

- (UIImage *)purchaseImage:(BOOL)isHighlighted
{
    return isHighlighted ? [UIImage imageNamed:@"btn_purchase_DOWN"] : [UIImage imageNamed:@"btn_purchase_UP"];
}

- (UIImage *)showtimesAndTicketsImage:(BOOL)isHighlighted
{
    return isHighlighted ? [UIImage imageNamed:@"btn_tickets_DOWN"] : [UIImage imageNamed:@"btn_tickets_UP"];
}

- (UIImage *)trailerImage:(BOOL)isHighlighted
{
    return isHighlighted ? [UIImage imageNamed:@"btn_trailer_DOWN"] : [UIImage imageNamed:@"btn_trailer_UP"];
    
}

#pragma mark - Button Configuration
- (void)configureButtonsForPlay
{
    self.bigOverlayButton.hidden = NO;
    [self.bigOverlayButton setBackgroundImage:[UIImage imageNamed:@"btn_play_UP"] forState:UIControlStateNormal];
    [self.bigOverlayButton setBackgroundImage:[UIImage imageNamed:@"btn_play_DOWN"] forState:UIControlStateHighlighted];
}

- (void)configureButtonsForSale
{
    self.smallLeftButton.hidden = NO;
    self.smallRightButton.hidden = NO;
    [self.smallLeftButton setBackgroundImage:[self watchButtonDown] forState:UIControlStateNormal];
    [self.smallLeftButton setBackgroundImage:[self watchButtonUp] forState:UIControlStateHighlighted];
    [self.smallRightButton setBackgroundImage:[self detailsImageDown] forState:UIControlStateNormal];
    [self.smallRightButton setBackgroundImage:[self detailsImageUp] forState:UIControlStateHighlighted];
}

- (void)configureButtonsForTicketing
{
    self.bigOverlayButton.hidden = NO;
    [self.bigOverlayButton setBackgroundImage:[UIImage imageNamed:@"btn_tickets_UP"] forState:UIControlStateNormal];
    [self.bigOverlayButton setBackgroundImage:[UIImage imageNamed:@"btn_tickets_DOWN"] forState:UIControlStateHighlighted];
}

@end
