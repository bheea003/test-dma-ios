//
//  DMASettingsConstants.m
//  DMA
//
//  Created by JP on 6/25/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "DMASettingsConstants.h"

NSString *const StreamingEnabledKey = @"streamingEnabled";
NSString *const EmailEnabledKey = @"emailEnabled";
NSString *const RatingsFilterKey = @"ratingsFilter";

NSString *const ApplicationNameKey = @"applicationName";
NSString *const ApplicationCountryKey = @"applicationCountry";
NSString *const ApplicationLanguageKey = @"applicationLanguage";