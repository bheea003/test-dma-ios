//
//  DMASettingsConstants.h
//  DMA
//
//  Created by JP on 6/25/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const StreamingEnabledKey;
FOUNDATION_EXPORT NSString *const EmailEnabledKey;
FOUNDATION_EXPORT NSString *const RatingsFilterKey;

FOUNDATION_EXPORT NSString *const ApplicationNameKey;
FOUNDATION_EXPORT NSString *const ApplicationCountryKey;
FOUNDATION_EXPORT NSString *const ApplicationLanguageKey;