//
//  DMAAppDelegate.h
//  DMA
//
//  Created by JP on 5/23/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESPUIAppDelegate.h"

#define APP_DELEGATE (DMAAppDelegate *)[[UIApplication sharedApplication] delegate]

typedef enum {
    RatingsG,
    RatingsG_PG,
    RatingsG_PG_PG13,
} DMARatingsLimits;

@interface DMAAppDelegate : ESPUIAppDelegate <UIApplicationDelegate>

- (NSArray *)ratingsFilter;
- (BOOL)isStreamingEnabled;
- (BOOL)isEmailOptIn;

@end
