//
//  main.m
//  Disney Movies
//
//  Created by JP on 5/24/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DMAAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DMAAppDelegate class]));
    }
}
