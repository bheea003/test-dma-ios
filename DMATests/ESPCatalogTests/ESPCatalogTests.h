//
//  ESPCatalogTests.h
//  ESPCatalogTests
//
//  Created by JP on 4/24/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "ESPCoreTests.h"
#import "ESPCatalog.h"
#import "ESPMedia.h"

@interface ESPCatalogTests : ESPCoreTests

@end
