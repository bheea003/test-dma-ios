//
//  ESPCatalogTests.m
//  ESPCatalogTests
//
//  Created by JP on 4/24/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPCatalogTests.h"
#import "ESPConvenience.h"
#import "ESPCatalogCategory.h"
#import "ESPSearchResult.h"
#import "ESPSearchResultItem.h"

@implementation ESPCatalogTests

#pragma mark - Convenience
- (ESPCatalog *)catalog
{
    ESPCatalog *catalog = [ESPCatalog catalogForApplication:@"coolMovies" language:@"EN" country:@"US"];
    return catalog;
}

- (ESPCatalogCategory *)marvel
{
    ESPCatalogCategory *category = [[ESPCatalogCategory alloc] init];
    category.userTitle = @"Marvel";
    category.urlTitle = @"Marvel";
    return category;
}

- (ESPCatalogCategory *)animation
{
    ESPCatalogCategory *category = [[ESPCatalogCategory alloc] init];
    category.userTitle = @"Animation";
    category.urlTitle = @"Animation";
    return category;
}

- (void)testStatus
{
    self.isValid = NO;
    self.isRunning = YES;
    
    [[self catalog] obtainStatus:^(NSDictionary *result) {
        self.isValid = (result != nil);
        self.isRunning = NO;
    } failure:^(NSError *error) {
        self.isRunning = NO;
    }];
    while (self.isRunning) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    STAssertTrue(self.isValid, @"Unable to obtain status.");
}

- (void)testObtainCategoryNames
{
    self.isValid = NO;
    self.isRunning = YES;
    
    [self.catalog obtainAllCategories:^(NSArray *categories) {
        self.isValid = (categories != nil) && ([categories count] > 0);
        self.isRunning = NO;
    } failure:^(NSError *error) {
        self.isRunning = NO;
    }];
    while (self.isRunning) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    STAssertTrue (self.isValid, @"Categories acquisition failed");
}

- (void)testObtainCategory
{
    self.isValid = NO;
    self.isRunning = YES;

    [self.catalog obtainCategory:[self marvel] updatedSince:nil success:^(NSArray *categories, NSNumber *categoryCount) {
        self.isValid = ([categories count] > 0);
        self.isRunning = NO;
    } failure:^(NSError *error) {
        self.isRunning = NO;
    }];
    while (self.isRunning) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    STAssertTrue (self.isValid, @"Category acquisition for Marvel failed.");
}

- (void)testCategoryWithCount
{
    self.isValid = NO;
    self.isRunning = YES;
    
    __block NSString *feed = nil;
    [self.catalog obtainAllCategories:^(NSArray *categories) {
        NSUInteger randomIndex = arc4random() % [categories count];
        ESPCatalogCategory *category = [categories objectAtIndex:randomIndex];
        [self.catalog obtainCategory:category updatedSince:nil includeCount:YES filterFields:[NSArray arrayWithObject:FieldFilterGUID] success:^(NSArray *categories, NSNumber *categoryCount) {
            self.isValid = ([categories count] == [categoryCount intValue]);
            self.isRunning = NO;
        } failure:^(NSError *error) {
            self.isRunning = NO;
        }];
    } failure:^(NSError *error) {
        self.isRunning = NO;
    }];
    while (self.isRunning) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    STAssertTrue(self.isValid, @"Category acquisition with count for %@ failed", feed);
}

- (void)testMovieAcquisition
{
    self.isValid = NO;
    self.isRunning = YES;
    
    NSString *guid = @"116641";
    [self.catalog obtainMetadataForMovieGUID:guid category:[self animation] shouldIncludeAncillaryContent:YES success:^(ESPMedia *media) {
        id ancillaryContent = [[media ancillaryContent] lastObject];
        BOOL isMedia = [ancillaryContent isKindOfClass:[ESPMedia class]];
        STAssertTrue(isMedia, @"Ancillary content has gone wonky! It's not a Media object.");

        BOOL hasTitle = (! IsEmpty([media title]));
        BOOL isCorrectTitle = ([[media title] caseInsensitiveCompare:@"Cars 2"] == NSOrderedSame);
        STAssertTrue(hasTitle && isCorrectTitle, @"Cars 2 wasn't acquired for GUID:", guid);
        
        BOOL hasAncillary = ([[media ancillaryContent] count] > 0);
        STAssertTrue(hasAncillary, @"Cars 2 is missing its Ancillary content.");
        
        self.isValid = (isMedia && hasTitle && isCorrectTitle && hasAncillary);
        self.isRunning = NO;
    } failure:^(NSError *error) {
        self.isRunning = NO;
    }];
    while (self.isRunning) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    STAssertTrue(self.isValid, @"Cars 2 metadata acquisition failed");
}

- (void)testArchiving
{
    self.isValid = NO;
    self.isRunning = YES;
    
    NSString *guid = @"116641";
    [self.catalog obtainMetadataForMovieGUID:guid category:[self animation] shouldIncludeAncillaryContent:YES success:^(ESPMedia *media) {
        NSData *archive = [NSKeyedArchiver archivedDataWithRootObject:media];
        BOOL didArchive = (archive != nil);
        STAssertTrue(didArchive, @"Unable to encode media.");
        
        ESPMedia *unarchived = [NSKeyedUnarchiver unarchiveObjectWithData:archive];
        BOOL didUnarchive = [media isEqual:unarchived];
        STAssertTrue(didUnarchive, @"Archive doesn't result in correct object.");
        
        self.isValid = didArchive && didUnarchive;
        self.isRunning = NO;
    } failure:^(NSError *error) {
        self.isRunning = NO;
    }];
    while (self.isRunning) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    STAssertTrue(self.isValid, @"Cars 2 metadata acquisition failed.");
}

- (void)testSearch
{
    self.isValid = NO;
    self.isRunning = YES;
    
    NSString *query = @"monsters inc";
    [self.catalog query:query ratings:nil success:^(ESPSearchResult *searchResult) {
        NSArray *results = [searchResult searchResultItems];
        ESPSearchResultItem *lastItem = [results lastObject];
        
        self.isValid = ([[lastItem title] caseInsensitiveCompare:@"monsters, inc."] == NSOrderedSame);;
        self.isRunning = NO;
    } failure:^(NSError *error) {
        self.isRunning = NO;
    }];
    while (self.isRunning) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    STAssertTrue(self.isValid, @"Query Catalog failed.");
}

@end
