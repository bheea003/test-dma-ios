//
//  ESPAPIBridgeTests.m
//  ESPFoundation
//
//  Created by JP on 4/15/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPAPIClientTests.h"

@implementation ESPAPIClientTests

- (void)testHeaders
{
    ESPAPIClient *bridge = [ESPAPIClient clientForApplication:@"coolMovies" language:@"EN" country:@"US"];
    STAssertTrue([bridge hasCorrectHeaders], @"headers aren't being set correctly");
}

- (void)testStatus
{
    self.isValid = NO;
    self.isRunning = YES;
    
    ESPAPIClient *bridge = [ESPAPIClient clientForApplication:@"coolMovies" language:@"EN" country:@"US"];
    [bridge obtainStatus:^(NSDictionary *result) {
        self.isRunning = NO;
    } failure:^(NSError *error) {
        // 'core/status' should fail, as it doesn't exist
        self.isValid = YES;
        self.isRunning = NO;
    }];
    while (self.isRunning) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    STAssertTrue(self.isValid, @"Status obtained for base APIClient class.");
}

@end
