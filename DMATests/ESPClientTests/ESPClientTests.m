//
//  ESPClientTests.m
//  ESPClientTests
//
//  Created by JP on 4/4/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPClientTests.h"
#import "ESPClientUpgrade.h"

@implementation ESPClientTests

- (ESPClient *)client
{
    ESPClient *client = [ESPClient clientForApplication:@"coolMovies" language:@"EN" country:@"US"];
    return client;
}

- (void)testStatus
{
    self.isValid = NO;
    self.isRunning = YES;
    
    [[self client] obtainStatus:^(NSDictionary *result) {
        self.isValid = (result != nil);
        self.isRunning = NO;
    } failure:^(NSError *error) {
        self.isRunning = NO;
    }];
    while (self.isRunning) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    STAssertTrue(self.isValid, @"Unable to obtain status.");
}

- (void)testValidateAccessUS
{    
    self.isValid = NO;
    self.isRunning = YES;
    
    [self.client validateAccess:@"216.253.0.0" success:^(NSString *country) {
        self.isValid = [country caseInsensitiveCompare:@"usa"] == NSOrderedSame;
        self.isRunning = NO;
    } failure:^(NSError *error) {
        self.isValid = NO;
        self.isRunning = NO;
    }];
    while (self.isRunning) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    STAssertTrue (self.isValid, @"US Access Validation failed");
}

- (void)testValidateAccess
{
    self.isValid = NO;
    self.isRunning = YES;
    
    [self.client validateAccess:@"223.223.224.0" success:^(NSString *country) {
        self.isValid = NO; // access should fail
        self.isRunning = NO;
    } failure:^(NSError *error) {
        self.isValid = (error.code == AccessDenied);
        self.isRunning = NO;
    }];
    while (self.isRunning) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    STAssertTrue (self.isValid, @"Japan Access Validation failed");
}

- (void)testUpgradeNotApplicable
{
    self.isValid = NO;
    self.isRunning = YES;
    
    [self.client validateApplication:@"iOS" systemVersion:@"5.1" appversion:@"1.0" success:^(ESPClientUpgrade *upgrade) {
        self.isValid = upgrade.upgradeStatus == UpgradeNotApplicable;
        self.isRunning = NO;
    } failure:^(NSError *error) {
        self.isValid = NO;
        self.isRunning = NO;
    }];
    while (self.isRunning) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    STAssertTrue (self.isValid, @"UpgradeNotApplicable Validation failed");
}

@end
