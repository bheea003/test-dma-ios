//
//  ESPClientTests.h
//  ESPClientTests
//
//  Created by JP on 4/4/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "ESPCoreTests.h"
#import "ESPClient.h"

@interface ESPClientTests : ESPCoreTests

@end
