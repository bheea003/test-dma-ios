//
//  ESPStringTests.m
//  ESPFoundation
//
//  Created by JP on 4/5/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPStringTests.h"
#import "NSString+ESP.h"

@implementation ESPStringTests

- (void)testContains
{
    NSString *string = @"thisisatestofthecontainsmethod";
    STAssertTrue([string contains_esp:@"test"], @"String contains: isn't working");
}

- (void)testStringByReplacingPhoneNumberMarkdown
{
    NSString *string = @"[TEL:9876543210|Call now]";
    STAssertTrue([[string stringByReplacingPhoneNumberMarkdown_esp] caseInsensitiveCompare:@"<a href=\"tel:9876543210\">Call now</a>"] == NSOrderedSame, @"Telephone Link parsing is borked");
}

- (void)testStringByReplacingCMSLink
{
    NSString *string = @"[LINK:FAQ|FAQ Link]";
    STAssertTrue([[string stringByReplacingCMSLink_esp] caseInsensitiveCompare:@"<a href=\"dma://FAQ\">FAQ Link</a>"] == NSOrderedSame, @"CMS Link parsing is borked");
}

- (void)testStringByStrippingUnicodeCharacters
{
    NSString *string = @"Registered®Trademark™";
    STAssertTrue([[string stringByStrippingUnicodeCharacters_esp] caseInsensitiveCompare:@"RegisteredTrademark"] == NSOrderedSame, @"Unicode stripping is borked");
}

- (void)testURLEncoding
{
    NSString *string = @"This is a test";
    NSLog(@"encoded: %@", [string urlEncodedString_esp]);
    STAssertTrue([[string urlEncodedString_esp] caseInsensitiveCompare:@"This+is+a+test"] == NSOrderedSame, @"URLEncoding is borked");
}

@end