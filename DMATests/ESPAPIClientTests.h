//
//  ESPAPIBridgeTests.h
//  ESPFoundation
//
//  Created by JP on 4/15/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "ESPAPIClient.h"
#import "ESPCoreTests.h"

@interface ESPAPIClientTests : ESPCoreTests

@end
