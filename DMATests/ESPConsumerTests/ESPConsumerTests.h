//
//  ESPConsumerTests.h
//  ESPCore
//
//  Created by JP on 5/14/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "ESPCoreTests.h"
#import "ESPConsumer.h"
#import "ESPAuthorization.h"

@interface ESPConsumerTests : ESPCoreTests

@property (strong, nonatomic) ESPAuthorization *authorization;

@end
