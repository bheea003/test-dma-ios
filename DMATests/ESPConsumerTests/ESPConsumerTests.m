//
//  ESPConsumerTests.m
//  ESPCore
//
//  Created by JP on 5/14/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPConsumerTests.h"

@implementation ESPConsumerTests

#define LOGIN @"allmovies1@dsaa.com"
#define PASSWORD @"mickey1"

- (ESPConsumer *)consumer
{
    ESPConsumer *consumer = [ESPConsumer consumerForApplication:@"coolMovies" language:@"EN" country:@"US"];
    return consumer;
}

- (void)testStatus
{
    self.isValid = NO;
    self.isRunning = YES;
    
    [[self consumer] obtainStatus:^(NSDictionary *result) {
        self.isValid = (result != nil);
        self.isRunning = NO;
    } failure:^(NSError *error) {
        self.isRunning = NO;
    }];
    while (self.isRunning) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    STAssertTrue(self.isValid, @"Unable to obtain status.");
}

- (void)testLogin
{
    self.isValid = NO;
    self.isRunning = YES;
    
    [[self consumer] login:LOGIN password:PASSWORD success:^(ESPAuthorization *authorization) {
        self.isValid = (authorization != nil);
        self.isRunning = NO;
    } failure:^(NSError *error) {
        self.isRunning = NO;
    }];
    while (self.isRunning) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    STAssertTrue(self.isValid, @"Unable to login.");
}

- (void)testLogout
{
    self.isValid = NO;
    self.isRunning = YES;
    
    [[self consumer] login:LOGIN password:PASSWORD success:^(ESPAuthorization *authorization) {
        [[self consumer] beginPlaybackSession:authorization success:^(ESPConsumerPlaybackToken *token) {
            authorization.playbackToken = token;
            [[self consumer] logout:authorization success:^(NSString *result) {
                self.isValid = (result != nil);
                self.isRunning = NO;
            } failure:^(NSError *error) {
                self.isRunning = NO;
            }];
        } failure:^(NSError *error) {
            self.isRunning = NO;
        }];
    } failure:^(NSError *error) {
        self.isRunning = NO;
    }];
    while (self.isRunning) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    STAssertTrue(self.isValid, @"Unable to logout.");
}

- (void)testEntitlements
{
    self.isValid = NO;
    self.isRunning = YES;
    
    [[self consumer] login:LOGIN password:PASSWORD success:^(ESPAuthorization *authorization) {
        [[self consumer] obtainOwnedEntitlements:authorization success:^(NSArray *entitlements) {
            self.isValid = (entitlements.count > 0);
            self.isRunning = NO;
        } failure:^(NSError *error) {
            self.isRunning = NO;
        }];
    } failure:^(NSError *error) {
        self.isRunning = NO;
    }];
    while (self.isRunning) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    STAssertTrue(self.isValid, @"Unable to acquire entitlements.");
}

- (void)testMovies
{
    self.isValid = NO;
    self.isRunning = YES;
    
    [[self consumer] login:LOGIN password:PASSWORD success:^(ESPAuthorization *authorization) {
        [[self consumer] obtainOwnedMovies:authorization ratingsFilter:nil success:^(NSArray *movies) {
            self.isValid = ([movies count] > 0);
            self.isRunning = NO;
        } failure:^(NSError *error) {
            self.isRunning = NO;
        }];
    } failure:^(NSError *error) {
        self.isRunning = NO;
    }];
    while (self.isRunning) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    STAssertTrue(self.isValid, @"Unable to acquire Movies");
}

- (void)testSessionAcquisition
{
    self.isValid = NO;
    self.isRunning = YES;
    
    [[self consumer] login:LOGIN password:PASSWORD success:^(ESPAuthorization *authorization) {
        [[self consumer] beginPlaybackSession:authorization success:^(id object) {
            self.isValid = (nil != object);
            self.isRunning = NO;
        } failure:^(NSError *error) {
            self.isRunning = NO;
        }];
    } failure:^(NSError *error) {
        self.isRunning = NO;
    }];
    while (self.isRunning) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    STAssertTrue(self.isValid == YES, @"Unable to acquire session.");
}

- (void)testAuthorizationRefresh
{
    self.isValid = NO;
    self.isRunning = YES;
    
    [[self consumer] login:LOGIN password:PASSWORD success:^(ESPAuthorization *authorization) {
        [[self consumer] refresh:authorization success:^(ESPAuthorization *refreshedAuthorization) {
            self.isValid = (nil != refreshedAuthorization);
            self.isRunning = NO;
        } failure:^(NSError *error) {
            self.isRunning = NO;
        }];
    } failure:^(NSError *error) {
        self.isRunning = NO;
    }];
    while (self.isRunning) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    STAssertTrue(self.isValid == YES, @"Unable to refresh Authorization.");
}

- (void)testMetadata
{
    self.isValid = NO;
    self.isRunning = YES;
    
    [[self consumer] login:LOGIN password:PASSWORD success:^(ESPAuthorization *authorization) {
        [[self consumer] obtainMetadata:authorization success:^(NSDictionary *accountDictionary) {
            self.isValid = [[accountDictionary allKeys] count] > 0;
            self.isRunning = NO;
        } failure:^(NSError *error) {
            self.isRunning = NO;
        }];
    } failure:^(NSError *error) {
        self.isRunning = NO;
    }];
    while (self.isRunning) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
    STAssertTrue(self.isValid == YES, @"Unable to obtain Metadata.");
}

@end
