//
//  ESPCoreTests.h
//  ESPCoreTests
//
//  Created by JP on 4/30/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

@interface ESPCoreTests : SenTestCase

@property (nonatomic) BOOL isRunning;
@property (nonatomic) BOOL isValid;

@end
