//
//  AFNetworkingTests.m
//  ESPCore
//
//  Created by JP on 5/2/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "AFNetworkingTests.h"
#import "UIImageView+AFNetworking.h"

@implementation AFNetworkingTests

- (void)testImageURL
{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    NSURL *url = [NSURL URLWithString:@"http://img.webring.com/r/d/disneygraphix/logo"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [imageView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        STAssertTrue(image != nil, @"Unable to acquire image");
    }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        STFail(@"Unable to acquire image: %@", [error description]);
    }];
}

@end
