//
//  DMATests.h
//  DMATests
//
//  Created by JP on 5/28/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

@interface DMATests : SenTestCase

@property (nonatomic) BOOL isRunning;
@property (nonatomic) BOOL isValid;

@end
