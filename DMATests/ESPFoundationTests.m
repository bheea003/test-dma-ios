//
//  ESPFoundationTests.m
//  ESPFoundationTests
//
//  Created by JP on 4/5/13.
//  Copyright (c) 2013 Disney. All rights reserved.
//

#import "ESPFoundationTests.h"
#import "ESPConvenience.h"

@implementation ESPFoundationTests

- (void)testIsEmpty
{
    STAssertTrue(IsEmpty(@""), @"IsEmpty isn't working for strings");
    STAssertTrue(IsEmpty([NSArray array]), @"IsEmpty isn't working for arrays");
    STAssertTrue(IsEmpty([NSData data]), @"IsEmpty isn't working for data");
    STAssertTrue(! IsEmpty(@"TEST"), @"IsEmpty isn't working for strings");
    STAssertTrue(! IsEmpty([NSArray arrayWithObject:@"TEST"]), @"IsEmpty isn't working for arrays");
    STAssertTrue(! IsEmpty([NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://www.disney.com"]]), @"IsEmpty isn't working for data");
}

@end
